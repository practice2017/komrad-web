# -*- coding: utf-8 -*-

from flask import render_template, url_for, jsonify, request, abort
from flask import make_response

from siem.extensions import db
from siem.wrappers import access_required
from siem.models import Correspondences
from siem.correspondences import corresps
import siem.route_access as ra

import pdfkit


@corresps.route("", methods=["GET"])
@access_required(ra.PAGE_CORRESPONDENCIES)
def ajax_correspondencies():
    data = Correspondences.get_all_as_dict()
    # top lvl list of list definition =D
    data_for_graph = [[], [], [], []]
    sl = 0
    impl = 0
    not_impl = 0
    current_root = data[0]['ref']
    for row in data:
        if row['type'] == u'node':
            continue
        if row['type'] == u'root' and row['ref'] != current_root:
            data_for_graph[0].append(current_root)
            data_for_graph[1].append(sl)
            data_for_graph[2].append(impl)
            data_for_graph[3].append(not_impl)
            sl = 0
            impl = 0
            not_impl = 0
            current_root = row['ref']
            continue
        if row['type'] == u'leaf':
            if row['selected'] == 0:
                sl += 1
            # sl = sl + row['selected']
            # impl = impl + row['implemented']
            if row['implemented'] == 0:
                not_impl += 1
            else:
                impl += 1

    data_for_graph[0].append(current_root)
    data_for_graph[1].append(sl)
    data_for_graph[2].append(impl)
    data_for_graph[3].append(not_impl)

    return render_template("correspondences/correspondences.html",
                           data=data,
                           data_for_graph=data_for_graph)


@corresps.route("/download", methods=["GET"])
@access_required(ra.PAGE_CORRESPONDENCIES)
def pdf_correspondencies():
    data = Correspondences.get_all_as_dict()
    data_for_graph = [[], [], [], []]
    sl = 0
    impl = 0
    not_impl = 0
    current_root = data[0]['ref']
    for row in data:
        if row['type'] == u'node':
            continue
        if row['type'] == u'root' and row['ref'] != current_root:
            data_for_graph[0].append(current_root)
            data_for_graph[1].append(sl)
            data_for_graph[2].append(impl)
            data_for_graph[3].append(not_impl)
            sl = 0
            impl = 0
            not_impl = 0
            current_root = row['ref']
            continue
        if row['type'] == u'leaf':
            if row['selected'] == 0:
                sl += 1
            # sl = sl + row['selected']
            # impl = impl + row['implemented']
            if row['implemented'] == 0:
                not_impl += 1
            else:
                impl += 1

    data_for_graph[0].append(current_root)
    data_for_graph[1].append(sl)
    data_for_graph[2].append(impl)
    data_for_graph[3].append(not_impl)

    rendered_template = render_template("correspondences/corres_pdf.html",
                                        data=data,
                                        data_for_graph=data_for_graph)
    # endered_template = rendered_template.decode("utf-8").encode("cp1252")
    options = {
        'encoding': "utf8"
    }

    css = [
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for(
            'static',
            filename='bower_components/bootstrap/dist/css/bootstrap.min.css')
    ]

    pdf = pdfkit.from_string(
        rendered_template, False, options=options, css=css)
    response = make_response(pdf)
    response.headers['Content-Disposition'] = "attachment; \
        filename='iso27001.pdf"
    response.headers['Content-Type'] = "application/pdf; charset=utf-8"

    return response


@corresps.route("/switchimpl", methods=["POST"])
@access_required(ra.PAGE_CORRESPONDENCIES)
def switch_selected():
    _id = request.get_json().get('id', None)
    new_state = request.get_json().get('state', False)

    if _id:
        rec = Correspondences.get_by_id(_id)
        rec.implemented = new_state
        db.session.commit()
        return jsonify({'result': 'ok'}), 200

    return abort(403)


@corresps.route("/switchsl", methods=["POST"])
@access_required(ra.PAGE_CORRESPONDENCIES)
def switch_implemented():
    _id = request.get_json().get('id', None)
    new_state = request.get_json().get('state', False)

    if _id:
        rec = Correspondences.get_by_id(_id)
        rec.selected = new_state
        db.session.commit()
        return jsonify({'result': 'ok'}), 200

    return abort(403)


@corresps.route("/comment", methods=["POST"])
@access_required(ra.PAGE_CORRESPONDENCIES)
def set_comment():
    _id = request.get_json().get('id', None)
    comment = request.get_json().get('comment', None)

    if _id and comment:
        rec = Correspondences.get_by_id(_id)
        rec.comment = comment
        db.session.commit()
        return jsonify({'result': 'ok'}), 200
    return abort(403)
