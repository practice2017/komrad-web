# -*- coding: utf-8 -*-
import eventlet
eventlet.monkey_patch()
import os
import socketio

from flask import request, render_template, session, redirect, \
                  url_for, abort, Flask
from flask_bootstrap import Bootstrap
from flask_babel import Babel
# from werkzeug.contrib.profiler import ProfilerMiddleware
from datetime import timedelta, datetime


from extensions import (
    db,
    login_manager,
    sio,
    redis_store
)

from client_manager.hooked_manager import HookedManager
from configuration.models.license import License

# hooks for sio redis client
from event_listener.models.widget_hook import WidgetHook
from auth import authentification
from event_listener import event_listener
from fileserver.controller import fileserver
from utils import CustomJSONEncoder
from correlation import correlation
from configuration import configs
from correspondences import corresps
from facts_base import facts_base
from availability_monitoring import avail_monitor

__all__ = ['create_app']

DEFAULT_BLUEPRINTS = [
    event_listener,
    authentification,
    correlation,
    configs,
    corresps,
    fileserver,
    facts_base,
    avail_monitor
]
CLIENT_HOOKS = [
    WidgetHook
]


def create_app(config, engineio_logger=True, logger=True, hooks=CLIENT_HOOKS,
               blueprints=DEFAULT_BLUEPRINTS):
    """Create a Flask app.
    don`t use any blocking functions in app; 
    use for it eventlet or greenlet thread

    :param config: config object, see config.py
    :return: wsgi app
    """
    app = Flask(__name__)

    configure_app(app, config)
    #configure_hook(app)
    configure_extensions(app)
    configure_blueprints(app, blueprints)
    configure_template_filters(app)
    configure_error_handlers(app)
    client_manager = create_client_manager(app, hooks)
    sio.init_app(app, engineio_logger=engineio_logger, logger=logger,
                 async_mode='eventlet',
                 client_manager=client_manager
                 )

    # for rule in app.url_map.iter_rules():
    #     print rule
    return app


def configure_blueprints(app, blueprints):
    """Configure blueprints in views."""
    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def configure_app(app, config):
    """Different ways of configurations."""

    # http://flask.pocoo.org/docs/api/#configuration
    app.config.from_object(config)

    # Setup websocket

    # http://flask.pocoo.org/docs/config/#instance-folders
    # app.config.from_pyfile('production.cfg', silent=True)

    # Use instance folder instead of env variables to make deployment easier.
    # app.config.from_envvar('%s_APP_CONFIG' % DefaultConfig.PROJECT.upper(),
    #     silent=True)


def configure_extensions(app):
    """ setup extensions: sqlalchemy, babel, redis, login-manager
    
    :param app: flask app
    :return: None
    """
    # flask-sqlalchemy
    db.init_app(app)

    # client_manager
    db.init_app(app)

    # flask-babel
    babel = Babel(app)

    redis_store.init_app(app)

    # @app.teardown_appcontext
    # def shutdown_session(exception=None):
    #     db.remove()

    @babel.localeselector
    def get_locale():
        return 'ru'
        accept_languages = app.config.get('LANGUAGES')
        return request.accept_languages.best_match(accept_languages)

    Bootstrap(app)

    login_manager.init_app(app)
    login_manager.login_view = 'login.login'

    @login_manager.unauthorized_handler
    def unauthorized():
        abort(401)
        return

    # create pcap folder if not exists
    if not os.path.exists(app.config['PCAP_FILES_PATH']):
        os.makedirs(app.config['PCAP_FILES_PATH'])
        os.chmod(app.config['PCAP_FILES_PATH'], 0777)

    # app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])


def configure_template_filters(app):

    @app.template_filter()
    def pretty_date(value):
        return pretty_date(value)

    @app.template_filter()
    def datetimeformat(value, format='dd/MM/yyyy HH:mm:ss'):
        try:
            if type(value) != datetime:
                value = datetime.fromtimestamp(int(value))
        except:
            return value
        return value

    @app.template_filter()
    def tdelta(value):
        return str(timedelta(seconds=value))


def configure_logging(app):
    """Configure file(info) and email(error) logging."""

    if app.debug or app.testing:
        # Skip debug and test mode. Just check standard output.
        return

    import logging
    from logging.handlers import SMTPHandler

    # Set info level on logger, which might be overwritten by handers.
    # Suppress DEBUG messages.
    app.logger.setLevel(logging.INFO)

    info_log = os.path.join(app.config['LOG_FOLDER'], 'info.log')
    info_file_handler = logging.handlers.RotatingFileHandler(
        info_log, maxBytes=100000, backupCount=10)
    info_file_handler.setLevel(logging.INFO)
    info_file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]')
    )
    app.logger.addHandler(info_file_handler)

    # Testing
    app.logger.info("testing info.")
    app.logger.warn("testing warn.")
    app.logger.error("testing error.")

    # mail_handler = SMTPHandler(app.config['MAIL_SERVER'],
    #                            app.config['MAIL_USERNAME'],
    #                            app.config['ADMINS'],
    #                            'O_ops... %s failed!' % app.config['PROJECT'],
    #                            (app.config['MAIL_USERNAME'],
    #                             app.config['MAIL_PASSWORD']))
    # mail_handler.setLevel(logging.ERROR)
    # mail_handler.setFormatter(logging.Formatter(
    #     '%(asctime)s %(levelname)s: %(message)s '
    #     '[in %(pathname)s:%(lineno)d]')
    # )
    # app.logger.addHandler(mail_handler)


def configure_hook(app):
    """Настройка хуков до и после реквеста
    
    :param app: flask app
    :return: None
    """

    app.json_encoder = CustomJSONEncoder

    @app.before_request
    def check_license():
        """Проверка лицензии
        
        :return: none
        """
        if not request.endpoint == "static":
            lic = License()
            # the check method contain aborts if the license was expired
            lic.check()

    @app.before_request
    def before_request():
        """Проверка таймаута сессии
        
        :return: none
        """
        session.permanent = True

        app.permanent_session_lifetime = timedelta(
            minutes=app.config['SESSION_TIMEOUT'])



def create_client_manager(app, hooks):
    """Инициализация менеджера sub сообщений от очереди
    
    :param app: flask app
    :param hooks: функции обработки входящий сообщений
    :return: inited manager
    """
    queue_manager = HookedManager(app, 'redis://%s:%i' %
                                  (app.config['SIO_REDIS_HOST'],
                                   app.config['SIO_REDIS_PORT']),
                                  channel=app.config['SIO_CHANNEL'],
                                  write_only=False)
    for hook in hooks:
        queue_manager.addHook(hook)
    return queue_manager


def configure_error_handlers(app):

    @app.errorhandler(400)
    def bad_request_error_page(error):
        return render_template("errors/400.html"), 400

    @app.errorhandler(401)
    def unauthorized_goto(error):
        """
        
        :param error: str if error is regular
                      dict if error was declared by license manager
        :return: 401 rendered template
        """
        lic_info = None
        err_msg = None
        if not isinstance(error.description, str):
            if error.description.get('err_msg', None):
                err_msg = error.description.get('err_msg', None)
            if error.description.get('err_msg', None):
                lic_info = error.description.get('license_info', None)
        return render_template(
            "errors/401.html",
            lic_info=lic_info,
            err_msg=err_msg), 401

    @app.errorhandler(403)
    def forbidden_page(error):
        return render_template("errors/403.html"), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("errors/404.html"), 404
    
    @app.errorhandler(503)
    def page_not_found(error):
        return render_template("errors/503.html"), 503

    @app.errorhandler(500)
    def server_error_page(error):
        return render_template("errors/500.html"), 500

    @sio.on_error_default
    def def_err(data):
        pass
