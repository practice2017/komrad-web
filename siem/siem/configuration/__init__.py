# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""
from flask import Blueprint

configs = Blueprint('configs', __name__,
                    template_folder='templates', url_prefix='/config')

ACCESS_MODULE = 'access_config'

from controllers import *
from sio import *
