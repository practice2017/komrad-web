# -*- coding: utf-8 -*-
import requests
from flask import current_app as app
from werkzeug.exceptions import abort
import json


class License(object):
    def __init__(self):
        self.url = app.config['LICENSE_URL']
    
    @classmethod
    def getLicenseData(self):
        r = requests.get(self().url + 'license')
        if r.status_code != 200:
            return {'license_key': False}
        return json.loads(r.content)

    @classmethod		
    def getPerformanceData(self):
        r = requests.get(self().url + 'license/restrictions')
        if r.status_code != 200:
            return {'license_key': False}
        return json.loads(r.content)	

    def check(self):
        url = "%s%s" % (app.config['LICENSE_URL'], 'license')
        try:
            r = requests.get(url)
        except requests.ConnectionError:
            abort(503, {'err_msg': ""})
        if r.status_code == 404:
            abort(401, {'err_msg': r.content})
        if r.status_code == 402:
            abort(401, {'license_info': json.loads(r.content)})
        return True
