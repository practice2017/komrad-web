# -*- coding: utf-8 -*-
import requests
import json

from flask import current_app as app


class PluginsRequester(object):

    def __init__(self):
        self.url = app.config['FACTS_BASE_URL'] + 'plugins/'

    @classmethod
    def getAll(self):
        response = requests.get(self().url + 'all')
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        response = json.loads(response.content)
        for key, value in response.iteritems():
            value.update({'id': key})
        return response.values()

    @classmethod
    def getSidByPid(self, uuid):
        data = requests.get(self().url + 'sids_by_pid?pid=' + uuid)
        if data.status_code != 200:
            data.raise_for_status()
        return json.loads(data.content)

    @classmethod
    def getActiveList(self):
        response = requests.get(self().url + 'active_list')
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        return json.loads(response.content)

    @classmethod
    def get(self, is_active):
        pass

    @classmethod
    def update(self, ids):
        data = json.dumps({"new_state": ids})
        response = requests.put(self().url + 'update', data=data)
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        return {u"response": u"ok"}

