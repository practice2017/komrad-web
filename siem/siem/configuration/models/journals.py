# -*- coding: utf-8 -*-
import tailer

available_journals = [
    u'komrad-base-of-facts.log',
    u'komrad-correlation-engine.log',
    u'komrad-framework.log',
    u'komrad-gambolputty.log',
    u'komrad-gravedigger.log',
    u'komrad-indexer.log',
    u'komrad-reaction.log',
    u'komrad-searcher.log',
    u'komrad-storage.log',
    u'komrad-storage-rotate.log',
    u'komrad-widget-daemon.log',
    u'komrad-storage.err'
]


class Journals(object):

    def __init__(self, journals=available_journals):
        self.journals = journals

    def getLast100lines(self):
        res = {}
        for j in self.journals:
            #with open(j) as j_file:
            try:
                res[j] = tailer.tail(open(u'/var/log/' + j), 100)
                res[j] = reversed(res[j])
            except IOError:
                pass
        return res

