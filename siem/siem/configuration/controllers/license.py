# -*- coding: utf-8 -*-

from flask import jsonify, request, render_template
from siem.wrappers import access_required
from siem.configuration import configs
from ..models.license import License
import siem.route_access as ra


#@configs.route('/license', methods=["GET"])
#@access_required(ra.PAGE_LICENSE)
def ajax_license():
    """
    """
    return render_template("configuration/license.html")

@configs.route("/license/info", methods=["GET"])
def get_license_info():
    l = License()
    data = l.getLicenseData()
    return jsonify({'response': data})
	
@configs.route("/license/performance", methods=["GET"])
def get_license_performance():
    l = License()
    data = l.getPerformanceData()
    return jsonify({'response': data})	