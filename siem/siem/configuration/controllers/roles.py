# -*- coding: utf-8 -*-

from flask import jsonify, request, abort, Response, current_app, stream_with_context

from siem.wrappers import access_required
from siem.configuration import configs
from siem.models import Role
import siem.route_access as ra

from flask_babel import lazy_gettext as _

@configs.route("/role", methods=["POST", "PUT"])
@access_required(ra.PAGE_USERS)
def role_controller():
    if request.method == 'PUT':
        _id = request.get_json().get('id')
        name = request.get_json().get('name')
        access_map = request.get_json().get('access_map')
        Role.updateRecord(_id, name, access_map)
        return jsonify({'success': True}), 200

    name = request.get_json().get('name')
    access_map = request.get_json().get('access_map')
    Role.addRole(name, access_map)
    return jsonify({'success': True}), 200


@configs.route("/role/<int:_id>", methods=["DELETE"])
@access_required(ra.PAGE_USERS)
def delete_role_controller(_id):
    Role.removeRole(_id)
    return jsonify({'success': True}), 200


@configs.route('/roles/all', methods=["GET"])
@access_required(ra.PAGE_USERS)
def roles_get_list():
    """
    """
    return jsonify({'response': [i.serialize for i in Role.query.all()]}), 200


@configs.route('/role/export/<string:fmt>', methods=["GET"])
@access_required(ra.PAGE_USERS)
def roles_export_data(fmt='csv'):
    """
    """

    if fmt not in ['csv']:
        return abort(400)

    roles = Role.query.all()

    def generate(data):

        def getName(value):
            if value == 1:
                return 'R'
            if value == 2:
                return 'W'
            return 'N/A'

        table_headers = [_(u'#'), _(u'role name')] + \
                        ra.ACCESS_LABELS.values()
        print table_headers
        table_headers = [unicode(x) for x in table_headers]
        amap_keys = ra.ACCESS_LABELS.keys()

        spr = current_app.config['CSV_SEPARATOR']
        yield spr.join(table_headers) + '\n'
        for role in sorted(data, key=lambda k: k.id):
            access_map = role.access_map
            yield spr.join(
                [str(role.id), role.name] +
                [getName(access_map.get(k, 0)) for k in amap_keys]) + '\n'

    return Response(stream_with_context(
        generate(roles)), mimetype='text/csv')
