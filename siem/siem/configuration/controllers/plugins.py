# -*- coding: utf-8 -*-

from flask import jsonify, request, render_template

from siem.wrappers import access_required
from siem.configuration import configs
from ..models.plugins import PluginsRequester as req
import siem.route_access as ra


@configs.route("/plugins", methods=["GET"])
@access_required(ra.PAGE_PLUGINS)
def ajax_plugins():
    return render_template('configuration/plugins.html')


@configs.route("/plugins/sid", methods=["GET"])
@access_required(ra.PAGE_PLUGINS)
def ajax_plugin_sid():
    return render_template('configuration/plugin_sid.html')


@configs.route("/plugins/<string:pid>", methods=["GET"])
@access_required(ra.PAGE_PLUGINS)
def get_plugin_sid(pid):
    data = req.getSidByPid(pid)
    return jsonify({'response': data})


@configs.route("/plugins/all", methods=["GET"])
@access_required(ra.PAGE_PLUGINS)
def get_plugins():
    data = req.getAll()
    return jsonify({'response': data}), 200


@configs.route("/plugins", methods=["POST"])
@access_required(ra.PAGE_PLUGINS)
def update_plugins():
    ids = request.get_json().get('plugins')
    new_state = request.get_json().get('state')
    post_data = dict(zip(ids, [new_state] * len(ids)))
    data = req.update(post_data)
    return jsonify(data), 200
