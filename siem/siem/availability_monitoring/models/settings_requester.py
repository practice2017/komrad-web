# -*- coding: utf-8 -*-
import requests
import json

from flask import current_app as app


class settRequester(object):

    @classmethod
    def getAllSetts(self):
        data = requests.get(app.config['FACTS_BASE_URL'] + 'nagios')
        return json.loads(data.content)

    @classmethod
    def addSett(self, settings):
        data = json.dumps(settings)
        res = requests.post(app.config['FACTS_BASE_URL'] + 'nagios', data=data)
        if res.status_code != requests.codes.ok:
            pass
        return True
		
    @classmethod
    def configSett(self, settings):
        data = json.dumps(settings)
        res = requests.put(app.config['FACTS_BASE_URL'] + 'nagios', data=data)
        if res.status_code != requests.codes.ok:
            pass
        return True	

    @classmethod
    def deleteSett(self, id):
        res = requests.delete(
            app.config['FACTS_BASE_URL'] + 'nagios?id=' + str(id))
        if res.status_code != requests.codes.ok:
            pass
        return True
