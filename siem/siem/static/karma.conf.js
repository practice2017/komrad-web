// Karma configuration
// Generated on Wed Nov 30 2016 23:17:58 GMT-0800 (PST)

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['wiredep', 'jasmine', 'browserify'],


        wiredep: {
            dependencies: true,
            devDependencies: true
        },

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'test/**/*.js': [ 'browserify' ]
        },

        browserify: {
            debug: true//,
            //transform: [ 'agGrid']
        },

        // list of files / patterns to load in the browser
        files: [
            './js/app/app.komrad2.js',
            './js/app/configs/configs.module.js',
            './js/app/configs/*.js',
            './js/app/core/core.module.js',
            './js/app/core/*.js',
            './js/app/correl/correl.module.js',
            './js/app/correl/*.js',
            './js/app/corres/corres.module.js',
            './js/app/corres/*.js',
            './js/app/event-listener/el.module.js',
            './js/app/event-listener/*.js',
            './test/**/*.js'
        ],
        
        // list of files to exclude
        exclude: [],

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'html'],

        htmlReporter: {
            outputFile: 'tests/units.html',

            // Optional
            pageTitle: 'Unit Tests',
            subPageTitle: 'Komrad 2.0',
            groupSuites: true,
            useCompactStyle: true,
            useLegacyStyle: true
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome', 'Firefox', 'Opera'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity
    })
}
