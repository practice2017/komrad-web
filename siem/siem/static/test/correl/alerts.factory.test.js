"use strict";

describe("Alerts api service", function () {
    var AletrsContent,
        $httpBackend,
        drilldownList = {
            response: [{
                count: 13,
                id: "1c366de9-26b1-0d12-b788-3effd2444dda",
                ru_name: "Подозрительная активность"
            }]
        },
        incedentsStatistics = {
            total: {
                opened: 50,
                viewed: 5,
                closed: 10,
                total: 65
            }
        }

    beforeEach(module("KomradeApp.correl"));

    beforeEach(inject(function (_$httpBackend_, _AletrsContent_) {
        AletrsContent = _AletrsContent_;
        $httpBackend = _$httpBackend_;

        $httpBackend.when('GET', "/correl/alerts/drilldown").respond(drilldownList);
        $httpBackend.when('GET', "/correl/alerts/stat").respond(incedentsStatistics);
    }));

    it("should get drilldown list", function () {
        var data = new AletrsContent();
        data.pullDrilldown().then(function (data) {
            expect(data).toBe(drilldownList);
        });
    });

    it("should get incedents statistic", function () {
        var data = new AletrsContent();
        data.incedentsStat().then(function (data) {
            expect(data).toBe(incedentsStatistics);
        });
    });

});