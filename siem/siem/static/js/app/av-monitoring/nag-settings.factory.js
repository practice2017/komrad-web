(function () {
    'use strict';

    angular
        .module('KomradeApp.av_monitoring')
        .factory('NagSett', settGetter);


    settGetter.$inject = ['$http', '$q', 'Notification'];


    function settGetter($http, $q, Notification) {
        var settGetter = function () {
            this.busy = false;
            this.message = '';
            this.data = [];
            this.has_error = true;
        };

        settGetter.prototype.getAllSetts = function () {
            var defer = $q.defer();

            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            if (this.busy) return;

            $http.get('/settings/all')
                .success(function (data) {
                    console.log(data)
                    this.data = data;
                    this.busy = false;
                    defer.resolve(data);
                }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))
            return defer.promise;
        }

        settGetter.prototype.addSett = function (settings) {
            var defer = $q.defer();

            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            if (this.busy) return;

            $http.post('/settings/add', angular.toJson(settings))
                .success(function (data) {
                    console.log(data)
                    this.busy = false;
                    defer.resolve(data);
                }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))
            return defer.promise;
        }

        settGetter.prototype.deleteSett = function (id) {
            var defer = $q.defer();

            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            if (this.busy) return;

            $http.post('/settings/delete', angular.toJson(id))
                .success(function (data) {
                    this.busy = false;
                    defer.resolve(data);
                }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))
            return defer.promise;
        }

        settGetter.prototype.configSett = function (settings) {
            var defer = $q.defer();

            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            if (this.busy) return;

            $http.post('/settings/configure', angular.toJson(settings))
                .success(function (data) {
                    this.busy = false;
                    defer.resolve(data);
                }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))
            return defer.promise;
        }

        return settGetter;
    }


})();
