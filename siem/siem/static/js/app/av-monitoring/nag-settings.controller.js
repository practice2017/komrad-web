(function () {
    'use strict';

    angular
        .module('KomradeApp.av_monitoring')
        .controller('nagiosSettings', nagiosSettingsCtrl)
        .controller('settModalCtrl', settModalCtrl);

    nagiosSettingsCtrl.$inject = ['$scope', '$uibModal', 'NagSett'];

    settModalCtrl.$inject = ['$scope', '$filter', '$uibModalInstance', 'deleteItem', 'sett'];

    function nagiosSettingsCtrl($scope, $uibModal, NagSett) {
        $scope.searchSettings = '';
        $scope.nagiosSetting = {};
        $scope.nagiosSetting.empty = true;
        $scope.nagiosSettings = new NagSett();

        $scope.selectSetting = function (data) {
            if (data) {
                $scope.nagiosSetting = data
                $scope.nagiosSetting.empty = false;
            }
        }

        $scope.deleteSetting = function (sett) {
            var removeModalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'dialog.html',
                controller: 'settModalCtrl',
                windowClass: "app-modal-window",
                size: 'sm',
                resolve: {
                    deleteItem: function () {
                        return sett;
                    },
                    sett: function () {
                        return undefined
                    },
                    allSettings: function () {
                        return undefined
                    }
                }
            });

            removeModalInstance.result.then(function (sett) {
                $scope.nagiosSettings.deleteSett({id: sett.id}).then(function (data) {
                    var index = $scope.nagiosSettings.data.indexOf($scope.nagiosSetting);
                    $scope.nagiosSettings.data.splice(index, 1);
                    $scope.selectSetting($scope.nagiosSettings.data[index - 1]);
                    if (!$scope.nagiosSettings.data.length) {
                        $scope.nagiosSetting.empty = true;
                    }
                })
            }, function () {
            });
        }

        $scope.nagiosSettings.getAllSetts().then(function (data) {
            $scope.selectSetting(data[0]);
        });


        $scope.configure = function (sett) {
            var oldSettings = angular.copy(sett);
            var modalInstance = $uibModal.open({
                animation: true,
                scope: $scope,
                templateUrl: 'settModal.html',
                controller: 'settModalCtrl',
                size: 'lg',
                windowClass: "app-modal-window",
                resolve: {
                    deleteItem: function () {
                        return undefined;
                    },
                    sett: function () {
                        return sett
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $scope.$broadcast('deliver.allData', {
                    data: $scope.nagiosSettings.data,
                    configure: true
                })
            }, function () {
            });

            modalInstance.result.then(function (settings) {
                if (angular.equals(oldSettings, settings)) {
                    return;
                }
                $scope.nagiosSettings.configSett(settings).then(function (data) {
                    $scope.nagiosSettings.data.forEach(function (item) {
                        if (item.id === data.id) {
                            item.hostname = data.hostname;
                            item.address = data.address;
                            item.services = data.services;
                        }
                    });
                })
            }, function () {
            });
        }

        $scope.openCreator = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                scope: $scope,
                templateUrl: 'settModal.html',
                controller: 'settModalCtrl',
                size: size,
                windowClass: "app-modal-window",
                resolve: {
                    deleteItem: function () {
                        return undefined;
                    },
                    sett: function () {
                        return undefined
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $scope.$broadcast('deliver.allData', {data: $scope.nagiosSettings.data})
            }, function () {
            });

            modalInstance.result.then(function (settings) {
                $scope.nagiosSettings.addSett(settings).then(function (data) {
                    var services = [];
                    data.services.forEach(function (item) {
                        services.push(item)
                    });

                    function idSetter(arr) {
                        if (!arr.length) {
                            return 0
                        }
                        return arr[arr.length - 1].id + 1
                    }

                    $scope.nagiosSettings.data.push({
                        hostname: data.hostname,
                        address: data.address,
                        services: services,
                        id: idSetter($scope.nagiosSettings.data)
                    });
                    $scope.selectSetting($scope.nagiosSettings.data[0]);
                })
            }, function (error) {

            });
        }

    }

    function settModalCtrl($scope, $filter, $uibModalInstance, deleteItem, sett) {
        $scope.nagios_settings = {};
        $scope.nagios_settings.hostname = '';
        $scope.nagios_settings.address = '';
        $scope.nagios_settings.services = [];
        var options = [{
            id: 1,
            name: 'check_tcp',
        }, {
            id: 2,
            name: 'check_udp',
        }, {
            id: 3,
            name: 'check_ssh',
        }, {
            id: 4,
            name: 'check_http',
        }, {
            id: 5,
            name: 'check_https',
        }, {
            id: 6,
            name: 'check_smtp',
        }, {
            id: 7,
            name: 'ping',
        }];
        $scope.services = [{
            id: 1,
            port: undefined,
            options: options,
            selectedPlugin: {},
            description: undefined,
            portValidationErr: false,
            descriptionValidationErr: false,
            pluginValidationErr: false
        }]

        $scope.portSetter = function (service) {
            if (!service.selectedPlugin.name) {
                return;
            }
            switch (service.selectedPlugin.name) {
                case 'check_http':
                    service.port = 80;
                    break;
                case 'check_https':
                    service.port = 443;
                    break;
                case 'check_ssh':
                    service.port = 22;
                    break;
                case 'check_smtp':
                    service.port = 25;
                    break;
            }
        }


        $scope.$on('deliver.allData', function (e, data) {
            $scope.allData = data;
        })

        function selectSetter(item, options) {
            for (var i = 0; i < options.length; i++) {
                if (item.plugin == options[i].name) {
                    return options[i];
                }
            }
        }

        if (sett) {
            $scope.services.length = 0;
            $scope.nagios_settings.hostname = sett.hostname;
            $scope.nagios_settings.address = sett.address;
            $scope.nagios_settings.id = sett.id;
            sett.services.forEach(function (item, i) {
                var selected = selectSetter(item, options);
                $scope.services.push({
                    id: i,
                    port: item.port,
                    selectedPlugin: selected,
                    options: options,
                    description: item.description
                });
            });
        }

        if (deleteItem) {
            $scope.nagios_settings.hostname = deleteItem.hostname;
        }

        function validateAddress(value) {
            var address = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/
            return address.test(value);
        }


        $scope.addNewChoice = function () {
            var newItemNo = $scope.services.length + 1;
            $scope.services.push({
                id: 1 + newItemNo,
                port: undefined,
                options: options,
                selectedPlugin: {},
                description: undefined,
                portValidationErr: false,
                descriptionValidationErr: false,
                pluginValidationErr: false
            });
        };

        $scope.removeChoice = function () {
            var lastItem = $scope.services.length - 1;
            $scope.services.splice(lastItem);
        };


        $scope.ok = function (settings, services) {
            $scope.hostnameAllreadyExistError = false;
            $scope.validationError = false;
            var validAddress = validateAddress(settings.address);
            if (settings.hostname === '') {
                this.form.hostname.$dirty = true;
                return;
            }

            if (!$scope.allData.configure) {
                for (var i = 0; i < $scope.allData.data.length; i++) {
                    if (settings.hostname === $scope.allData.data[i].hostname) {
                        $scope.hostnameAllreadyExistError = true;
                        return;
                    }
                }
            }

            if (settings.address === '' || !validAddress) {
                this.form.address.$dirty = true;
                $scope.validationError = true;
                return;
            }


            for (var i = 0; i < services.length; i++) {
                if ($filter('isEmpty')(services[i].selectedPlugin) || !services[i].description
                    || !services[i].port) {
                    services[i].pluginValidationErr = true;
                    services[i].descriptionValidationErr = true;
                    services[i].portValidationErr = true;
                    return;
                }
            }

            services.forEach(function (item) {
                item.pluginValidationErr = false;
                item.descriptionValidationErr = false;
                item.portValidationErr = false;
                settings.services.push({
                    port: item.port,
                    plugin: item.selectedPlugin.name,
                    description: item.description
                });
            });
            $uibModalInstance.close(settings);
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.remove = function () {
            $uibModalInstance.close(deleteItem);
        }

    }
})();

