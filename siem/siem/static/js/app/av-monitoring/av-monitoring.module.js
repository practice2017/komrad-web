(function() {
    'use strict';

    angular.module('KomradeApp.av_monitoring', [
        'KomradeApp.core',
        'ngAnimate',
        'ui.layout',
        'ncy-angular-breadcrumb',
    ]);

})();