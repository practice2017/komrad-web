(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .controller('bofWizardCtrl', wizardCtrl);

    wizardCtrl.$inject = ['$scope', '$uibModalInstance', '$filter', '$timeout', 'settings', 'requester'];


    function wizardCtrl($scope, $uibModalInstance, $filter, $timeout, settings, requester) {
        var vm = this;
        var sqlSelect = 'SELECT ';
        vm.rawReqData = [];
        vm.colls = [];
        vm.runResult = [];
        vm.fileName = '';
        vm.temporaryTableData = [];
        vm.temporaryColsData = [];
        vm.listConfirmed = false;
        vm.requestLoading = false;
        vm.ListsRequester = requester;
        vm.showErrors = {
            refresh: false,
            request: false,
            requestData: false,
            rowsData: false
        }
        vm.isUpdate = false;

        vm.request_settings = function () {
            if (typeof settings === 'undefined') {
                return {
                    name: undefined,
                    type: 'auto',
                    update_period: 60,
                    timer_select: 's',
                    refresh: true,
                    sql: undefined,
                    filename: '',
                    values: undefined
                }
            }
            else {
                vm.isUpdate = true;
                var setts = angular.copy(settings);
                if (setts.sql)
                    setts.sql = setts.sql.slice(7);

                if (setts.update_period){
                    setts.refresh = true;
                }
                else {
                    setts.refresh = false;
                }

                if (setts.type === 'custom')
                    vm.runResult = setts.values;

                vm.listConfirmed = true;
                return setts;
            }
        }();

        //validation for first step
        vm.stepOne = function () {
            vm.name = angular.element(document.querySelector('.name-form'));
            if (vm.request_settings.name === undefined) {
                vm.name.$dirty = true;
                return false;
            }
            return true;
        };

        vm.wizardConfig = {
            name: 'bofWizard',
            currentStep: {}
        };
        //close wizard modal
        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        //run test request for auto list
        vm.runRequest = function () {
            vm.requestLoading = true;
            var request;
            request = sqlSelect + vm.request_settings.sql;
            vm.ListsRequester
                .rawSqlRequest(request)
                .then(function (result) {
                    vm.rawReqData = result.data;
                    vm.requestLoading = false;
                })
        };

        //perform csv data when choose file
        vm.handler = function (e, files) {
            vm.fileName = e.target.files['0'].name;
            var reader = new FileReader();

            reader.onload = function (event) {
                vm.runResult = CSVToArray(reader.result);
            };

            reader.readAsText(files[0]);
        };

        //write csv formated data to table
        function writeDataToTable(colsData, rowsData) {
            vm.cols = colsData;
            vm.runResult = rowsData;
        }

        //formate csv data
        function CSVToArray(strData, strDelimiter) {
            var strDelimiter = strDelimiter || ',';
            var tmp_str = strData;
            // the last nl symbol gets additional fake split element
            if (strData[strData.length - 1] === '\n')
                strData = strData.slice(0);
            var lines = strData.split('\n');
            if (lines.length <= 1)
                return [];

            var res = [];
            for (var i = 0; i < lines.length; i++){
                res.push(lines[i].split(strDelimiter));
            }
            return res;
        }

        function validateCustom(){
            var has_errors = false;
            vm.showErrors.rowsData = false;
            if (vm.runResult.length === 0){
                has_errors = true;
                vm.showErrors.rowsData = true;
            }
            return has_errors;
        }

        function validateAuto (){
            var has_errors = false;
            vm.showErrors.wrongFile = false;
            if (!vm.request_settings.update_period && vm.request_settings.refresh){
                has_errors = true;
                vm.showErrors.refresh = true;
            }
            if (!vm.request_settings.sql){
                has_errors = true;
                vm.showErrors.request = true;
            }
            return has_errors;
        }

        function makeSettingsAuto(){
            var rt = 0;
            if (vm.request_settings.refresh){
                rt = vm.request_settings.update_period;
            }
            return {
                sql: sqlSelect + vm.request_settings.sql,
                type: vm.request_settings.type,
                name: vm.request_settings.name,
                update_period: rt,
                timer_select: vm.request_settings.timer_select,
                type: vm.request_settings.type
            }
        }

        function makeSettingsCustom(){
            return {
                name: vm.request_settings.name,
                values: vm.runResult,
                filename: vm.filename,
                type: vm.request_settings.type
            }
        }
        //create custom list
        vm.create = function () {
            var makeValidate;
            var makeResult;
            if (vm.request_settings.type === 'custom') {
                makeValidate = validateCustom;
                makeResult = makeSettingsCustom;
            }
            if (vm.request_settings.type === 'auto') {
                makeValidate = validateAuto;
                makeResult = makeSettingsAuto;
            }
            if (makeValidate())
                return;

            vm.ListsRequester
                .createList(makeResult())
                .success(function (response) {
                    $uibModalInstance.close();
                })
                .error(function (response) {
                    vm.showErrors.requestData = true;
                });
        }

        vm.update = function () {
            var makeValidate;
            var makeResult;
            if (vm.request_settings.type === 'custom') {
                makeValidate = validateCustom;
                makeResult = makeSettingsCustom;
            }
            if (vm.request_settings.type === 'auto') {
                makeValidate = validateAuto;
                makeResult = makeSettingsAuto;
            }
            if (makeValidate())
                return;
            var result = makeResult();
            result.id = vm.request_settings.id;

            vm.ListsRequester
                .updateList(result)
                .success(function (response) {
                    $uibModalInstance.close();
                })
                .error(function (response) {
                    vm.showErrors.requestData = true;
                });
        }

        vm.final = vm.createTable;

    }

})();

