(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .directive('bofListFullData', bofList_single);

    function bofList_single() {
        return{
            restrict: 'E',
            templateUrl: 'bofListSingleFull.html',
            scope: {
                listId : '=',
            },
            controller: bofList_singleCtrl,
        };
    };

    bofList_singleCtrl.$inject = ['$scope', 'ListsRequester', 'sioBof', 'ChartService', '$uibModal'];

    function bofList_singleCtrl($scope, ListsRequester, sioBof, ChartService, $uibModal) {
        var event_name = "auto_new_values"

        $scope.ListsRequester = new ListsRequester();
        $scope.listInformation = undefined;
        $scope.graphConfig = undefined;
        $scope.selectedGraph = 'pie';
        var graphPusher = function () {};

        /**
         * watcher for list id, if it changed reload the directive data of list
         */
        $scope.$watch('listId', function(newValue, oldValue) {
            if (newValue){
                $scope.listInformation = newValue;
                loadList(newValue);
            }
        });

        $scope.$on('$destroy', function () {
            removeListeners();
        });


        $scope.setGraphType = function (type) {
            // can`t set type without data
            if (!$scope.graphConfig)
                return;
            var availGraphs = ['pie', 'pie3d', 'line', 'area', 'column', 'column3d'];
            if (availGraphs.indexOf(type) < 0)
                return;

            $scope.selectedGraph = type;

            $scope.graphConfig = ChartService.getConfigs(type, $scope.listInformation.values);
        }

        function removeMe(){
            removeListeners();
            $scope.ListsRequester.deleteList($scope.listInformation)
                .success(function (data, status) {
                    $scope.$emit('reloadLists')
                })
                .error(function () {
                });

        }

        // is data valid to graphs
        function checkDataForGraph(data){

            if (!data)
                return false

            for (var i = 0; i < data.length; i++) {
                // check is the values lengths has 2 values
                if (data[i].length < 2)
                    return false;

                var v = data[i][1];
                // is second value is numeric?
                if (!(!isNaN(parseFloat(v)) && isFinite(v)))
                    return false;
            }
            return true
        }

        // loading list data from server
        function loadList(item){
            removeListeners();
            $scope.ListsRequester.getList(item).then(function (data) {
                if (data.status !== 200){
                    return;
                }
                data = data.data;
                angular.extend($scope.listInformation, data);
                if (checkDataForGraph(data.values)) {
                    $scope.graphConfig = ChartService
                        .getConfigs($scope.selectedGraph,
                                    $scope.listInformation.values);
                }
                else {
                    $scope.graphConfig = undefined;
                    graphPusher = function () {};
                }

                // this lists don`t need to new info
                if ($scope.listInformation.type === 'custom' ||
                    $scope.listInformation.updatePeriod === 0)
                    return;
                forwardListeners();
            });
        }

        function updateValues(data){
            $scope.listInformation.values = data;

            // update graph by reinitting graph
            if ($scope.graphConfig)
                $scope.setGraphType($scope.selectedGraph);
        }

        // emtry function for avoiding an call error;
        var listener = function () {};

        function removeListeners() {
            listener();
            sioBof.removeListener(event_name);
        }

        function forwardListeners() {
            sioBof.forward(event_name);
            listener = $scope.$on(event_name, function (ev, data) {
                if (data.uuid === $scope.listInformation.id){
                    updateValues(data.values);
                }
            });
        }

        // open setting for selected widget
        $scope.openSettings = function () {
            var modalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'bofWizard.html',
                controller: 'bofWizardCtrl',
                controllerAs: 'wizard',
                windowClass: "app-modal-window",
                size: 'lg',
                resolve: {
                    settings: function () {
                        return $scope.listInformation;
                    },
                    requester: function () {
                        return $scope.ListsRequester;
                    },
                }
            });
            modalInstance.result.then(function (selectedItem) {
                loadList($scope.listInformation);
            }, function () {
            });
        }

                //delete list
        $scope.deleteListModal = function (list) {
            var removeModalInstance = $uibModal.open({
                scope: $scope,
                templateUrl: 'dialog.html',
                controller: 'modalRemoveCtrl as modal',
                windowClass: "app-modal-window",
                size: 'sm',
                resolve: {
                    settings: function () {
                        return list;
                    }
                }
            });
            removeModalInstance.result.then(function () {
                removeMe();
            }, function () {
            });
        };

    };

})();

