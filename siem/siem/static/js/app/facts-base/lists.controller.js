(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .controller('fbListsCtrl', listsCtrl)
        .controller('modalRemoveCtrl', modalRemoveCtrl);

    listsCtrl.$inject = ['$scope', '$http', '$filter', '$uibModal', '$log', 'BofLists'];


    modalRemoveCtrl.$inject = ['$scope', '$uibModalInstance', 'settings'];

    function listsCtrl($scope, $http, $filter, $uibModal, $log, BofLists) {

        $scope.animationsEnabled = true;
        $scope.selected_list = undefined;
        $scope.bofLists = new BofLists();
        $scope.cols = [];
        $scope.searchBofList = '';
        $scope.message = '';


        $scope.$on('reloadLists', function(event, args) {
            $scope.uploadAndSelect(0);
        })

        //open list create wizard
        $scope.openCreator = function (size) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'bofWizard.html',
                controller: 'bofWizardCtrl as wizard',
                size: size,
                windowClass: "app-modal-window",
                resolve: {
                    settings: function () {
                        return undefined;
                    },
                    requester: function (ListsRequester) {
                        return new ListsRequester();
                    },
                }

            });
            modalInstance.rendered.then(function () {
            }, function () {
            });

            modalInstance.result.then(function (added) {
                $scope.uploadAndSelect(added);
            }, function () {
            });
        };

        //gett lists
        $scope.uploadAndSelect = function (select_id){
            var sid = select_id;
            $scope.bofLists.getAllLists()
                .success(function (data, status) {
                    if (sid >= 0)
                        $scope.selectList($scope.bofLists.data[sid]);
                })
                .error(function () {
                });
        }

        $scope.uploadAndSelect(0);

        $scope.selectList = function (item) {
            $scope.selected_list = item;
        };

    }


    function modalRemoveCtrl($scope, $uibModalInstance, settings) {
        var self = this;

        self.list = settings;

        // remove list
        self.remove = function () {
            $uibModalInstance.close();
        };
        //close delete modal
        self.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }
})();

