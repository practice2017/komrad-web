(function () {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .config(uiRouterBOF);

    uiRouterBOF.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterBOF($stateProvider, $urlRouterProvider, $httpProvider) {

        var labels = {
            fblists: 'База фактов'
        }

        $stateProvider
            .state('fblists', {
                url:'/bof/lists',
                templateUrl: '/bof/lists',
                ncyBreadcrumb: {
                    label: labels.fblists
                },
                controller: 'fbListsCtrl'
            });
    }
})();

