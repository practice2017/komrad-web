(function() {
    'use strict';

    angular
        .module('KomradeApp.bof')
        .run(joinBofRoom);

    joinBofRoom.$inject = ['sioBof'];

    function joinBofRoom(sioBof) {
        sioBof.emit('join', {});
    };

})();
