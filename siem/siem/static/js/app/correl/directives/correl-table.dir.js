(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .directive('correlTable', correlTable);

    // widgetConfig directive duration and update period
    function correlTable() {
        var template = '<table-ui config="self.tableUiConfig" service-name="self.service"></table-ui>';
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается
            restrict: 'E',
            scope: {
                preloadedData: '=',
                service: '='
            },
            // контроллер для директивы

            controller: correlTableCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

            }

        };

    }

    correlTableCtrl.$inject = ['$scope', '$element', '$attrs', '$q', '$state', 'statuses'];

    function correlTableCtrl($scope, $element, $attrs, $q, $state, statuses) {
        var self = this;

        //ag-grid test -----------------------------------------------------------------------------
        self.tableLoading = true;

        var columnDefs = [
            {
                headerName: "",
                field: "select",
                width: 30,
                checkboxSelection: true,
                suppressSorting: true,
                suppressMenu: true,
                suppressMovable: true
            },
            {headerName: "№", field: "id", width: 100, suppressMenu: true},
            {
                headerName: "Имя директивы",
                field: "directive_id",
                suppressMenu: true
            },
            {headerName: "Дата фиксации", field: "alert_time", width: 150, suppressMenu: true},
            {headerName: "События", field: "events_number", width: 80, suppressMenu: true},
            {headerName: "Длительность", field: "duration", width: 120, suppressMenu: true},
            {headerName: "Риск", field: "risk", width: 70, suppressMenu: true},
            {
                headerName: "Статус",
                field: "status",
                width: 70,
                cellRenderer: statusIconRenderer,
                suppressMenu: true
            },
            {headerName: "Дата начала", field: "first_event_time", width: 150, suppressMenu: true},
            {
                headerName: "Действие",
                width: 80,
                field: "action",
                cellClass: "correl-action",
                cellRenderer: actionBtnRenderer,
                suppressMenu: true
            }
        ];

        var localeText = {
            page: 'Страница',
            to: 'до',
            of: 'из',
            last: 'Последняя',
            first: 'Первая',
            next: '<i class="fa fa-chevron-right"></i>',
            previous: '<i class="fa fa-chevron-left"></i>'
        };

        self.gridOptions = {
            columnDefs: columnDefs,
            suppressDragLeaveHidesColumns: true,
            enableColResize: true,
            virtualPaging: true,
            rowData: null,
            enableServerSideSorting: true,
            enableServerSideFilter: true,
            scrollbarWidth: 20,
            rowHeight: 37,
            rowSelection: "multiple",
            rowModelType: 'pagination',
            paginationPageSize: 20,
            localeText: localeText,
            icons: {
                sortAscending: '<i class="fa fa-sort-amount-asc"/>',
                sortDescending: '<i class="fa fa-sort-amount-desc"/>',
                checkboxChecked: '<i class="fa fa-check-square" style="color:#63A8EB; font-size: 20px"></i>',
                checkboxUnchecked: '<i class="fa fa-square-o" style="color:#E7EBEC; font-size: 22px"></i>'
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center" ' +
            'style="border: 1px solid #ddd"><img src="static/img/335.gif" ' +
            'style="width: 16px; height: 16px">  Загрузка...</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center" ' +
            'style="border: 1px solid #ddd">По данному запросу ничего не найдено</span>',
            onRowClicked: rowClick
        };

        var defered = $q.defer();
        defered.resolve(self.preloadedData);

        self.tableUiConfig = {
            gridOptions: self.gridOptions,
            preloadData: defered.promise,
            pagination: true,
            paginationSize: 5
        };


        //status icon formatter
        function statusIconRenderer(params) {
            var status = '<span class="' + statuses[params.value].props + '" title="' +
                statuses[params.value].name + '"><span class="status ' +
                statuses[params.value].icon + '"></span></span>';
            return status;
        }

        function rowClick(params) {
            if (!params.event.ctrlKey) {
                var url = $state.href('single_alert', {alertId: params.data.id});
                window.open(url, '_blank');
            }
        }

        /*function goToSingleAlertRenderer(params) {
         var link = '<a href="#/correl/alert/' + params.data.id + '" target="_blank">' + params.value + '</a>'
         return link;
         }*/

        function actionBtnRenderer(params) {
            params.eGridCell.onclick = function (e) {
                e.stopPropagation();
            };
            var btn = '<a href="#dirconstructor/' + params.data.uuid +
                '" class="btn btn-xs btn-default alert-edit" target="_blank"><span class="fa fa-pencil"></span></a>';
            return btn;
        }

        $scope.$on('correl.table.getSelectedRows', function (e, data) {
            self.gridOptions.api.showLoadingOverlay();
            var selectedRows = self.gridOptions.api.getSelectedRows(),
                resultData = {
                    selectedRows: selectedRows,
                    newStatus: data
                };

            $scope.$emit('correl.table.resolveSelectedRows', resultData);
        });
        
        $scope.$on('correl.table.hideOvelray', function (e, data) {
            self.gridOptions.api.hideOverlay();
        })


        // ag grid test end ------------------------------------------------------------------------

    }

})();