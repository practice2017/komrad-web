(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .controller('alertPage', alertSingle);

    alertSingle.$inject = ['$scope', 'alert_data', 'assign_list', '$uibModal',
        'DirTreeContent', 'statuses', 'risks', 'sioCorrel', 'me', '$sce', '$filter',
        'eventFields', '$http', 'Pagination', '$anchorScroll','$location', 'eventViewer'];

    function alertSingle($scope, alert_data, assign_list, $uibModal, DirTreeContent,
        statuses, risks, sioCorrel, me, $sce, $filter, eventFields, $http, Pagination,
        $anchorScroll, $location, eventViewer) {

        $scope.filter = undefined;
        $scope.loaded = false;
        $scope.statuses = statuses;
        $scope.risks = risks;
        $scope.eventsData = undefined;
        $scope.showGraph = false;
        $scope.assign_list = assign_list.data;
        me.get().then(function (data) {
            $scope.me = data;
        });

        $scope.report = {
            inc:alert_data.data.id,
            log: 0,
            events: 1,
            graph: 0,
            f: [6] // 6 is event_id //fields list
        }

        $scope.selectizeGroupsConfig = {
            plugins: ['remove_button'],
            valueField: 'id',
            labelField: 'name',
            delimiter: '|',
            placeholder: 'Pick something',
            onInitialize: function(selectize){
            // receives the selectize object as an argument 
            },
            // maxItems: 1 
        };

        $scope.selectizeReportConfig = {
            plugins: ['remove_button'],
            valueField: 'id',
            labelField: 'label',
            delimiter: '|',
            placeholder: 'Pick something',
            onInitialize: function(selectize){
            },
        };

        eventFields.getAll().then(function (data) {
            $scope.events_list = data;
        });

        $scope.summernoteConfig = {
              height: 150
        };

        function setupData (alert_data){
            var eventsData = alert_data.data;
            eventsData.history = fmtHistory(eventsData.history);
            var tmp_groups = [];
            var groups_lisr_str = [];
            for (var i = 0; i < eventsData.user_group_asigned.length; i++) {
                tmp_groups.push(
                    $filter('getByField')(assign_list.data, eventsData.user_group_asigned[i], 'id')
                );
                groups_lisr_str.push(eventsData.user_group_asigned[i].toString());
            }
            eventsData.user_group_asigned = tmp_groups;
            $scope.eventsData = eventsData;
            if ($scope.eventsData.graph)
                setupChart();

            $scope.to_update = {
                comment: "",
                risk: risks[eventsData.risk],
                status: statuses[eventsData.status],
                user_group_asigned: groups_lisr_str
            }
        };

        setupData(alert_data);


        function rsIsValid(risk_or_status) {
            if (risk_or_status === null || risk_or_status === undefined)
                return false;
            return true;
        }

        function validateGroupSubmit(list1, list2) {
            for(var i=0; i<list1.length; i++){
                if (list2.indexOf(list1[i]) === -1)
                    return true;
            }
            for(var i=0; i<list2.length; i++){
                if (list1.indexOf(list2[i]) === -1)
                    return true;
            }
            return false;
        }

        function fmtGroupMsg(groupsList) {
            var result = ""
            for (var i = 0; i < groupsList.length; i++) {
                var gr = $filter('getByField')(assign_list.data, groupsList[i], 'id');
                result += '<span class="label label-default">';
                if (gr.is_user){
                    result += '<i class="fa fa-user"></i>';
                }
                else {
                    result += '<i class="fa fa-group"></i>';
                }
                result += gr.name + '</span> ';
            }
            return result
        }

        function fmtRisk(risk) {
            return '<span class="' + risks[risk].props + '">' +
                       risks[risk].name + '</span> ';
        }

        function fmtStatus(status) {
            return '<span class="' + statuses[status].props + '">' +
                       statuses[status].name + '</span> ';
        }

        /**
         * [fmtMsg formate the massage of the new state]
         * @param  {[object]} prev [currant inc state]
         * @param  {[object]} curr [previous inc state]
         * @return {[object]}      [formatted object for ng-repeat]
         */
        function fmtMsg(prev, curr) {
            var msg = "";
            if (rsIsValid(curr.new_risk) && prev.new_risk != curr.new_risk){
                msg += 'Риск изменен на ' + fmtRisk(curr.new_risk);
            }

            if (rsIsValid(curr.new_status) && prev.new_status != curr.new_status){
                msg += 'Статус изменен на ' + fmtStatus(curr.new_status);
            }

            if (curr.removed_groups && curr.removed_groups.length > 0){
                msg += "Удаленные группы:";
                msg += fmtGroupMsg(curr.removed_groups)
            }

            if (curr.added_groups && curr.added_groups.length > 0){
                msg += "Добавленные группы:";
                msg += fmtGroupMsg(curr.added_groups)
            }

            return {
                dt: curr.datetime * 1000,
                changed_by: curr.changed_by,
                msg: $sce.trustAsHtml(msg),
                comment: $sce.trustAsHtml(curr.comment)
            }
        }

        /**
         * [fmtMsg format the init massage of the log]
         * @param  {[object]} _obj [init message]
         * @return {[object]}      [formatted object for ng-repeat]
         */
        function fmtInitMsg(_obj) {
            var msg = "";
            if (rsIsValid(_obj.new_risk)){
                msg += "Риск " + fmtRisk(_obj.new_risk);
            }

            if (rsIsValid(_obj.new_status)){
                msg += "Статус " + fmtStatus(_obj.new_status);
            }

            if (_obj.added_groups && _obj.added_groups.length > 0){
                msg += "Базовые группы:";
                msg += fmtGroupMsg(_obj.added_groups)
            }
            return {
                dt: _obj.datetime * 1000,
                changed_by: _obj.changed_by,
                msg: $sce.trustAsHtml(msg),
                comment :$sce.trustAsHtml(_obj.comment)
            }
        }

        /**
         * [fmtMsg format update massage of the sioCorrel]
         * @param  {[object]} _obj [init message]
         * @return {[object]}      [formatted object for ng-repeat]
         */
        function fmtUpdateMsg(_obj) {
            var msg = "";
            if (rsIsValid(_obj.new_risk)){
                msg += "Риск изменен на " + fmtRisk(_obj.new_risk);
            }

            if (rsIsValid(_obj.new_status)){
                msg += "Статус изменен на " + fmtStatus(_obj.new_status);
            }

            if (_obj.added_groups && _obj.added_groups.length > 0){
                msg += "Группы:";
                msg += fmtGroupMsg(_obj.added_groups)
            }
            return {
                dt: _obj.dt * 1000,
                changed_by: _obj.changed_by,
                msg: $sce.trustAsHtml(msg),
                comment: $sce.trustAsHtml(_obj.comm)
            }
        }

        /**
         * [fmtLogs Hande log list, add information about state changing.]
         * @param  {[array]} data [source data]
         * @return {[array]}      [handled data]
         */
        function fmtHistory(data) {
            var result = [];
            result.push(fmtInitMsg(data[0]));
            for (var i = 1; i < data.length; i++) {
                result.push(fmtMsg(data[i-1], data[i]));
            }
            return result;
        }

        $scope.cleanUpdateForm = function(){
            $scope.to_update = {
                comment: "",
                risk: risks[eventsData.risk],
                status: statuses[eventsData.status],
                user_group_asigned: groups_lisr_str
            }
        }

        /* socketing */

        sioCorrel.forward('update', $scope);
        $scope.$on('update', function (ev, data) {
            if (data.alert_id != $scope.eventsData.id)
                return;
            if (rsIsValid(data.new_status))
                $scope.eventsData.status = data.new_status;

            if (rsIsValid(data.new_risk))
                $scope.eventsData.risk = data.new_risk;

            if (rsIsValid(data.new_risk)){
                $scope.eventsData.risk = data.new_risk;
            }

            if (rsIsValid(data.groups.all)){
                $scope.eventsData.user_group_asigned = [];
                for (var i = 0; i < data.groups.all.length; i++) {
                    $scope.eventsData.user_group_asigned.push(
                        $filter('getByField')(assign_list.data, data.groups.all[i], 'id')
                    );
                }
            }

            $scope.eventsData.history.push(fmtUpdateMsg(data))

        });


        $scope.hcSelection = function(prop){
            return function(item){
                if ($scope.filter === undefined){
                    return true;
                }
                return (item[prop] >= $scope.filter.from && item[prop] <= $scope.filter.to);
            }
        }

        $scope.open = function (id) {
            eventViewer.open(id);
        };

        function setupChart(){
            var data = function(){
                var result = [];
                var series = [];
                var bands = [];
                for (var i = 0; i < $scope.eventsData.graph.events.length; i++) {
                    result.push({
                        x: $scope.eventsData.graph.events[i].ts * 1000,
                        y: 0,
                        z: $scope.eventsData.graph.events[i].count,
                    });
                }
                // push bubble
                series.push({
                    data:result,
                    showInLegend: false,
                    type: 'bubble',
                    events: {
                        selection: function(event) {
                            if (event.resetSelection){
                                $scope.filter = undefined;
                                $scope.$apply();
                                return;
                            };
                            $scope.filter = {
                                from: event.xAxis[0].min / 1000,
                                to: event.xAxis[0].max / 1000
                            };
                            $scope.$apply();
                        }
                    },
                })
                var first_is_absense = false;
                for(var i = 0; i < $scope.eventsData.graph.lvls.length; i++) {
                    if ($scope.eventsData.graph.lvls[i].absense && i === 0) {
                        first_is_absense = true;
                    }

                    var x = $scope.eventsData.graph.lvls[i].ts * 1000;

                    series.push({
                        color: 'black',
                        name: $scope.eventsData.graph.lvls[i].name,
                        type: 'line',
                        animation:false,
                        data: [[x,-1],[x,1]],
                        showInLegend: false,
                        dashStyle: 'Dash',
                        shadow: false,
                        marker:{
                            states:{
                                hover:{
                                    enabled: false
                                }
                            }
                        },
                    });
                    var time_to = undefined;
                    var time_from = undefined;
                    if ($scope.eventsData.graph.lvls[i].absense) {
                        if (first_is_absense) time_to = x + $scope.eventsData.graph.lvls[i].timer * 1000;
                        else time_to = x - $scope.eventsData.graph.lvls[i].timer * 1000;
                    }
                    else {
                        if (first_is_absense) first_is_absense = false;
                    }
                    if ($scope.eventsData.graph.lvls[i].absense) {
                        bands.push({
                            color: '#E8EAF6',
                            from: x, // Start of the plot band
                            to: time_to
                        })
                    }
                }
                return [series, bands];
            }();

            $scope.chartConfig = {
                options: {
                    exporting: { enabled: false },
                    chart: {
                        zoomType: 'x',
                        height: 200,
                    },
                    tooltip: {
                        formatter: function() {
                            if (this.series.type === 'bubble')
                                return 'Событий на ' +
                                       Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +
                                       ': <b>' + this.point.z + '</b>';

                            // its the line
                            return '<b>Правило:</b> ' + this.series.name + '<br>Дата фиксации: ' + 
                                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x);
                        }
                    }
                },
                series: data[0],
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    type: 'datetime',
                    plotBands: data[1],
                },
                yAxis: {
                    title: false,
                    labels: {
                        enabled: false
                    },
                    max:1,
                    min:-1
                },

            }
            $scope.showGraph = true;
        }

        $scope.disableRowClick = function($event) {
            $event.stopPropagation();
        }

        $scope.saveRisk = function(data){
            $scope.to_update.risk.id = data;
            $scope.submitChanges();
        }

        $scope.saveStatus = function(data){
            $scope.to_update.status.id = data;
            $scope.submitChanges();
        }

        $scope.submitChanges = function(){
            // prepearing sending data:
            var to_send = {}
            if ($scope.to_update.risk.id != $scope.eventsData.risk)
                to_send.r = $scope.to_update.risk.id;

            if ($scope.to_update.status.id != $scope.eventsData.status)
                to_send.s = $scope.to_update.status.id;

            if ($scope.to_update.comment)
                to_send.c = $scope.to_update.comment;

            var old_groups = [];
            for (var i = 0; i < $scope.eventsData.user_group_asigned.length; i++) {
                old_groups.push($scope.eventsData.user_group_asigned[i].id);
            }

            var new_groups = [];
            for (var i = 0; i < $scope.to_update.user_group_asigned.length; i++) {
                new_groups.push(parseInt($scope.to_update.user_group_asigned[i]));
            }

            if (validateGroupSubmit(new_groups, old_groups))
                to_send.gr = new_groups;

            if (!angular.equals({}, to_send)){
                to_send.id = $scope.eventsData.id;
                sioCorrel.emit('modify', to_send);
            }
        }


        $scope.generate = function (){
            return $http.get('/incidents/report', {params:$scope.report, responseType:'arraybuffer'})
                .success(function (data) {
                    var file = new Blob([data], { type: 'application/pdf; charset=utf-8' });
                    saveAs(file, 'alert' + $scope.eventsData.id +'.pdf');
                });
        }


        $scope.generateCSV = function (){
            var report_params = {
                'fmt': 'csv',
                'id': $scope.report.inc
            }
            return $http.get('/incidents/export',
                             {params:report_params, responseType:'arraybuffer'})
                .success(function (data) {
                    var file = new Blob([data], { type: 'data:attachment/csv; charset=utf-8' });
                    saveAs(file, 'alert' + $scope.eventsData.id +'.csv');
                });
        }

        $scope.gotoEditor = function() {
            $location.hash('editor');
            $anchorScroll();
        };

        //pagination

        $scope.pager = {};
        $scope.setPage = setPage;

        //initController();

        function initController() {
            // initialize to page 1
            $scope.setPage(1);
        }

        function setPage(page) {
            // get pager object from service
            $scope.pager = Pagination.GetPager($scope.eventsData.history.length, page);
            if (page < 1 || page > $scope.pager.totalPages) {
                return;
            }

            // get current page of items
            $scope.items = $scope.eventsData.history.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
        }

    }
})();

