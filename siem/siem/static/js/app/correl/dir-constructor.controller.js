(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .controller('dirsController', dirsCtrl)

    dirsCtrl.$inject = ['$scope', '$stateParams', '$document'];

    function dirsCtrl($scope, $stateParams, $document) {

        $scope.$on('$viewContentLoaded', function() {
            if (typeof $stateParams.dirId === 'undefined'){
                return;
            }
            var treeEl = angular.element($document.find('#directivesTree') );
            setTimeout(function() {
                var guid = setActiveByGuid(treeEl, $stateParams.dirId);
            }, 2000);

        });


        $scope.doNothing = function(id){
            return;
        }

        /*
            Реурсивный поиск директивы в дереве по её GUID.
        */
        function recursSearch(node, guid){
            if (typeof(node) === "object"){
                if ("uuid" in node){
                    if (node['uuid'] === guid)
                        return node['id'];
                }
                for (var i in node){
                    if (node[i]){
                        var res = recursSearch(node[i], guid);
                        if (res)
                            if (typeof(res) !== "string")
                                return node['id'] + ':' + res;
                            else
                                return res;
                    }
                }
            }
        }

        function setActiveByGuid(tree, guid){
            var jst = tree.jstree(true);

            var node = recursSearch(jst.get_json(), guid);
            if (node) {
                jst.select_node(node.split(':')[0]);
            } else {
                doREST('GET', '/correl/get_directive_by_guid/' + guid, null, 
                    function(directive){
                        hide_loader();
                        directive = JSON.parse(directive);
                        $("#dir-con").directiveConstructor("setDirective", JSON.parse(directive['data']));
                        bootbox.alert('Директива была удалена. Доступен только просмотр.');
                    },
                    function(x, e, s){
                        bootbox.alert('Ошибка загрузки директивы.');                    
                    }
                );
            }
        }


    }
})();

