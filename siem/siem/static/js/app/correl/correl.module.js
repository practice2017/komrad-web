(function () {
    'use strict';

    agGrid.initialiseAgGridWithAngular1(angular);

    angular.module('KomradeApp.correl', [
        'KomradeApp.core',
        'ngAnimate',
        'ncy-angular-breadcrumb',
        'infinite-scroll',
        'highcharts-ng',
        'ui.bootstrap',
        'ui-notification',
        'ngFileSaver',
        'angularMoment',
        'summernote',
        'restangular',
        'ui.layout',
        'ngPanels',
    ])
        .constant('risks', [{
            id: '0',
            name: 'риск 1',
            props: 'label label-danger'
        }, {
            id: '1',
            name: 'риск 2',
            props: 'label label-warning'
        }, {
            id: '2',
            name: 'риск 3',
            props: 'label label-success'
        }])
        .constant('statuses', [{
            id: '0',
            name: 'Новый',
            props: 'status-danger',
            icon: 'fa fa-exclamation-circle'
        }, {
            id: '1',
            name: 'Просмотрен',
            props: 'status-warning',
            icon: 'fa fa-clock-o'
        }, {
            id: '2',
            name: 'Закрыт',
            props: 'status-checked',
            icon: 'fa fa-check-circle'
        }]);
    ;
})();