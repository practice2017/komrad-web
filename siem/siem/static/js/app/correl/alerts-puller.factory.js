(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .factory('AlertsPuller', alertsGetterFactory);

    alertsGetterFactory.$inject = ['$http', '$filter', '$q', 'Notification', 'Restangular'];

    function alertsGetterFactory($http, $filter, $q, Notification, Restangular) {
        //main service constructor function
        var SearchContent = function () {
            this.message = '';
            this.busy = false;
            this.total = 0;
            this.page = 0;
            this.dir = '';
            this.order = 'desc';
            this.order_by = 'id';
            this.rule = {
                condition: 'AND',
                rules: [{
                    field: 'id',
                    id: 'id',
                    input: 'text',
                    operator: 'greater',
                    type: "integer",
                    value: 0
                }]
            };
        };


        var deffededPages;

        function dataTransformer(data) {
            var newData = [];
            data.forEach(function (item, index) {
                var duration = (item.alert_time - item.first_event_time) * 1000;
                newData.push({
                    id: item.id,
                    uuid: item.directive_id,
                    directive_id: item.ru_name,
                    alert_time: moment.unix(item.alert_time).format('DD/MM/YYYY HH:mm:ss'),
                    events_number: item.events_number,
                    duration: moment.utc(duration).format('HH:mm:ss'),
                    risk: item.risk,
                    status: item.status,
                    first_event_time: moment.unix(item.first_event_time).format('DD/MM/YYYY HH:mm:ss')
                });
            });

            return newData;
        }

        //get data to main alerts table
        SearchContent.prototype.getPage = function (number, page) {
            this.busy = true;
            var vm = this;
            
            if (this.message === '') {
                this.message = 'Загрузка...';
            }

            if (page || page === 0) {
                this.page = page;
            }

            var requestBody = {
                page: this.page,
                number: number,
                order: this.order,
                order_by: this.order_by,
                dir: this.dir,
                body: this.rule
            };

            function successGetPage(data) {
                var newData = [];
                if (data.total === 0) {
                    vm.busy = false;
                    vm.message = 'Всего найдено: ' + data.total;
                    deffededPages.resolve({
                        total: data.total
                    });
                    return;
                }

                vm.total = data.total;
                vm.message = 'Всего найдено: ' + vm.total;
                newData = dataTransformer(data.page);
                
                deffededPages.resolve({
                    data: newData,
                    numberOfPages: Math.ceil(data.total / number),
                    total: +data.total
                });
                vm.busy = false;

            }

            function errorGetPage(err) {
                if (err.status === -1) {
                    return;
                }
                Notification.error("Ошибка получения данных с сервера.");
            }

            if (deffededPages) deffededPages.resolve();

            deffededPages = $q.defer();

            Restangular.all('incidents/page').withHttpConfig({timeout: deffededPages.promise})
                .post(requestBody).then(successGetPage, errorGetPage);

            return deffededPages.promise;

        };


        SearchContent.prototype.setFilter = function (rules) {
            var number = 20,
                deferred = $q.defer();

            this.page = 0;

            if (!angular.equals(this.rule, rules) && rules !== undefined) {
                this.order_by = 'id';
                this.order = 'desc';
                this.rule = rules;
            }

            this.getPage(number).then(function (result) {
                if (!result) {
                    return;
                }
                deferred.resolve({
                    data: result.data,
                    numberOfPages: Math.ceil(result.total / number),
                    total: +result.total
                });
            });
            return deferred.promise;
        };

        SearchContent.prototype.setSort = function (number, orderBy, order, page) {
            var number = 20,
                deferred = $q.defer();

            this.page = page;
            this.order_by = orderBy;
            this.order = order;

            this.getPage(number).then(function (result) {
                if (!result) {
                    return;
                }
                deferred.resolve({
                    data: result.data,
                    numberOfPages: Math.ceil(result.total / number),
                    total: +result.total
                });
            });
            return deferred.promise;
        };

        SearchContent.prototype.exportPDF = function (){
            return $http.post('/incidents/report/all', {query:this.rule}, {responseType:'arraybuffer'})
                .success(function (data) {
                    var file = new Blob([data], { type: 'data:application/pdf; charset=utf-8' });
                    saveAs(file, 'incidents.pdf');
                });
        }


        return SearchContent;
    }

})();
