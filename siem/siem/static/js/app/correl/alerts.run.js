(function() {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .run(joinAlertsRoom);

    joinAlertsRoom.$inject = ['sioCorrel'];

    function joinAlertsRoom(sioCorrel) {
        sioCorrel.emit('join_corr', {});
    };

})();
