(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .factory('sioCorrel', SocketIOFactory);

    SocketIOFactory.$inject = ['socketFactory'];

    function SocketIOFactory(socketFactory) {
        var myIoSocket = io.connect('/correl');
        var  socketio = socketFactory({
            ioSocket: myIoSocket,
            prefix: ''
        });
        return socketio;
    }

})();

