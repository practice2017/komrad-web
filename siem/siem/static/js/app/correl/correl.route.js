(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .config(uiRouterCorrel);

    uiRouterCorrel.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function uiRouterCorrel($stateProvider, $urlRouterProvider, $httpProvider) {
        var labels = {
            dirconstructor: 'Конструктор директив',
            alerts: 'Инциденты',
            single_alert: 'инцидент № {[{eventsData.id}]}'
        }

        $stateProvider
            .state('dirconstructor', {
                url: '/dirconstructor',
                templateUrl: '/dirconstructor',
                ncyBreadcrumb: {
                    label: labels.dirconstructor,
                },
                controller: 'dirsController',
                data: {
                    docFile: "event_listener/dir_constructor.pdf"
                }
            })
            .state('loaded_dirconstructor', {
                url:'/dirconstructor/:dirId',
                templateUrl: function ($stateParams) {
                    var id = $stateParams.dirId || '';
                    return '/dirconstructor';
                },
                ncyBreadcrumb: {
                    label: labels.dirconstructor,
                },
                controller: 'dirsController'
            })
            .state('alerts', {
                url: '/correl/alerts/all',
                templateUrl: '/correl/alerts/all',
                ncyBreadcrumb: {
                    label: labels.alerts,
                },
                controller: 'alertsListController',
                resolve: {
                    drilldown: function (AletrsContent) {
                        var drilldown = new AletrsContent(),
                            drillAndFilters;
                        drillAndFilters = drilldown.pullDrilldown();
                        return drillAndFilters;
                    },
                    incStat: function (AletrsContent) {
                        var stats = new AletrsContent();
                        return stats.incedentsStat();
                    },
                    firstLoadData: function (AlertsPuller) {
                        var number = 20,
                            service,
                            data;
                        service = new AlertsPuller()
                        data = service.getPage(number);
                        return {
                            data: data,
                            service: service
                        }
                    }
                }
            })
            .state('single_alert', {
                url: '/correl/alert/:alertId',
                templateUrl: '/correl/alert_page',
                ncyBreadcrumb: {
                    label: labels.single_alert,
                    parent: 'alerts'
                },
                controller: 'alertPage',
                resolve: {
                    alert_data: function ($http, $stateParams) {
                        var id = $stateParams.alertId || '';
                        return $http({method: 'GET', url: '/correl/alert/' + id});
                    },
                    assign_list: function ($http) {
                        return $http({method: 'GET', url: '/correl/assign_list'});
                    },
                }
            });


    }


})();

