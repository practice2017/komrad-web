(function () {
    'use strict';

    angular
        .module('KomradeApp.correl')
        .factory('TableGridContent', alertsGetterFactory);

    alertsGetterFactory.$inject = ['$http', '$filter', '$q', 'Notification', 'Restangular'];

    function alertsGetterFactory($http, $filter, $q, Notification, Restangular) {
        //main service constructor function
        var SearchContent = function () {
            this.page = 0;
            this.dir = '';
            this.order = 'desc';
            this.order_by = 'id';
            this.rule = {
                condition: 'AND',
                rules: [{
                    field: 'id',
                    id: 'id',
                    input: 'text',
                    operator: 'greater',
                    type: "integer",
                    value: 0
                }]
            };
        };


        var deffededPages;

        function dataTransformer(data) {
            var newData = [];
            data.forEach(function (item, index) {
                newData.push({
                    id: item.id,
                    uuid: item.directive_id,
                    directive_id: item.ru_name,
                    alert_time: moment.unix(item.alert_time).format('DD/MM/YYYY HH:mm:ss'),
                    events_number: item.events_number,
                    duration: item.duration,
                    risk: item.risk,
                    status: item.status,
                    first_event_time: moment.unix(item.first_event_time).format('DD/MM/YYYY HH:mm:ss')
                });
            });

            return newData;
        }

        //get data to main alerts table
        SearchContent.prototype.getPage = function (number, page) {

            if (page || page === 0) {
                this.page = page;
            }

            var requestBody = {
                page: this.page,
                number: number,
                order: this.order,
                order_by: this.order_by,
                dir: this.dir,
                body: this.rule
            };
            
            function successGetPage(data) {
                var newData = [];
                if (data.total === 0) {
                    deffededPages.resolve({
                        total: data.total
                    });
                    return;
                }

                newData = dataTransformer(data.page);
                
                deffededPages.resolve({
                    data: newData,
                    numberOfPages: Math.ceil(data.total / number),
                    total: +data.total
                });

            }

            function errorGetPage(err) {
                if (err.status === -1) {
                    return;
                }
                Notification.error("Ошибка получения данных с сервера.");
            }

            if (deffededPages) deffededPages.resolve();

            deffededPages = $q.defer();

            Restangular.all('incidents/page').withHttpConfig({timeout: deffededPages.promise})
                .post(requestBody).then(successGetPage, errorGetPage);

            return deffededPages.promise;

        };


        SearchContent.prototype.setFilter = function (rules) {
            var number = 20,
                deferred = $q.defer();

            this.page = 0;

            if (!angular.equals(this.rule, rules) && rules !== undefined) {
                this.order_by = 'id';
                this.order = 'desc';
                this.rule = rules;
            }

            this.getPage(number).then(function (result) {
                if (!result) {
                    return;
                }
                deferred.resolve({
                    data: result.data,
                    numberOfPages: Math.ceil(result.total / number),
                    total: +result.total
                });
            });
            return deferred.promise;
        };

        SearchContent.prototype.setSort = function (orderBy, order) {
            var number = 20,
                deferred = $q.defer();

            this.page = 0;
            this.order_by = orderBy;
            this.order = order;

            this.getPage(number).then(function (result) {
                if (!result) {
                    return;
                }
                deferred.resolve({
                    data: result.data,
                    numberOfPages: Math.ceil(result.total / number),
                    total: +result.total
                });
            });
            return deferred.promise;
        };

        return SearchContent;
    }

})();
