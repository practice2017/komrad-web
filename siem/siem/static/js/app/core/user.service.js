(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .service('me', aboutMe);

    aboutMe.inject = ['$http', '$q'];

    function aboutMe($http, $q) {

        var info = undefined;

        var defer = $q.defer();


        function getCurrUserInfo() {
            if(info) {
                defer.resolve(info);
            }

            var httpRequest = $http.get('/about_me')
            .success(function(data) {
                info=data;
                defer.resolve(info);
            })
            return defer.promise;
        }

        return  {
            get: getCurrUserInfo,
        }
    }



})();
