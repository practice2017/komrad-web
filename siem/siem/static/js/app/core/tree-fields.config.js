(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .config(treeFieldsConfig);


    treeFieldsConfig.$inject = ['ivhTreeviewOptionsProvider'];

    function treeFieldsConfig(ivhTreeviewOptionsProvider) {

        ivhTreeviewOptionsProvider.set({
            idAttribute: 'id',
            labelAttribute: 'label',
            childrenAttribute: 'children',
            selectedAttribute: 'selected',
            useCheckboxes: true,
            expandToDepth: 0,
            indeterminateAttribute: '__ivhTreeviewIndeterminate',
            expandedAttribute: '__ivhTreeviewExpanded',
            defaultSelectedState: false,
            validate: true,
            twistieExpandedTpl: '<div class="twist tree-expand"></div>',
            twistieCollapsedTpl: '<div class="twist tree-collapse"></div>',
            twistieLeafTpl: '<div class="twist dotted-lines"></div>'
        });
        
    }



})();

