(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .service('treeFields', getTreeFields);

    getTreeFields.inject = ['$http', '$q'];

    function getTreeFields($http, $q) {

        var allFields = undefined;

        function getAllFileds() {
            var defer = $q.defer();
            if (allFields) {
                defer.resolve(allFields);
            }

            $http.get('/fields/tree')
                .success(function (data) {
                    console.log(data);
                    data.forEach(function (item) {
                        if(!item.label) {
                            item.label = item.name
                        }
                    });
                    allFields = data;
                    defer.resolve(allFields);
                });
            return defer.promise;
        }

        return {
            getAll: getAllFileds
        }
    }

})();
