(function () {
    'use strict';

    angular
        .module('KomradeApp.core')
        .directive('tableUi', tableUi);

    // widgetConfig directive duration and update period
    function tableUi() {
        var template = '<div ng-show="self.tableIsLoaded">' +
            '<div ag-grid="self.agGridOptions" class="ag-bootstrap" style="height:350px"></div>' +
            '<div class="row">' +
            '<div class="col-lg-4 col-md-4 col-sm-4" style="margin-top: 20px; padding-left: 0; padding-right: 0; display: -webkit-inline-box;" ' +
            'ng-show="self.config.quantityOfRecords">' +
            '<span style="line-height: 2.428571; padding-left: 20px; padding-right: 20px;">На странице</span>' +
            '<select class="form-control" ng-init="self.numberOfRows = self.displayedRowsNumber[0]"' +
            'ng-options="item.number for item in self.displayedRowsNumber track by item.id"' +
            'ng-model="self.numberOfRows" ng-change="self.quantitySelect()"' +
            'style="width:90px;">' +
            '</select>' +
            '</div>' +
            '<div class="text-center" ng-show="(self.totalPages > 1) && self.config.pagination" class="col-lg-8  col-md-8  col-sm-8" >' +
            '<!-- pager -->' +
            '<ul ng-if="self.allOfTheData.length" class="pagination">' +
            '<li ng-class="{disabled:self.currentPage === 0}">' +
            '<a class="waves waves-button waves-effect waves-float" ng-click="(self.currentPage === 0)?disabled:self.setPage(0)">Первая</a>' +
            '</li>' +
            '<li ng-class="{disabled:self.currentPage === 0}">' +
            '<a class="waves waves-button waves-effect waves-float" ng-click="(self.currentPage === 0)?disabled:self.setPage(self.currentPage - 1)">' +
            '<i class="fa fa-chevron-left"></i>' +
            '</a>' +
            '</li>' +
            '<li ng-repeat="page in self.pager track by page.id" ng-class="{active:self.currentPage === page.id}">' +
            '<a class="waves waves-button waves-effect waves-float" ng-click="self.setPage(page.id)">{[{page.pageNum}]}</a>' +
            '</li>' +
            '<li class="animate-show" ng-hide="((self.pager[1].id == (self.totalPages - 4)) || (self.totalPages <=5))?true:false">' +
            '<a style="cursor: default">...</a>' +
            '</li><li class="animate-show" ng-hide="((self.pager[1].id == (self.totalPages - 4)) || (self.totalPages <=5))?true:false">' +
            '<a class="waves waves-button waves-effect waves-float" ng-click="self.setPage(self.totalPages-1)">{[{self.totalPages}]}</a>' +
            '</li>' +
            '<li ng-class="{disabled:self.currentPage === (self.totalPages - 1)}">' +
            '<a class="waves waves-button waves-effect waves-float" ng-click="(self.currentPage === (self.totalPages - 1))?disabled:self.setPage(self.currentPage + 1)">' +
            '<i class="fa fa-chevron-right"></i>' +
            '</a></li>' +
            '<li ng-class="{disabled:self.currentPage === (self.totalPages - 1)}">' +
            '<a class="waves waves-button waves-effect waves-float" ng-click="(self.currentPage === (self.totalPages - 1))?disabled:self.setPage(self.totalPages-1)">Последняя</a>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>';
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается
            restrict: 'E',
            scope: {
                serviceName: '=',
                config: '='
            },
            // контроллер для директивы

            controller: tableUiCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

            }

        };

    }

    tableUiCtrl.$inject = ['$scope', '$element', '$attrs', '$injector',
        '$timeout', '$window'];

    function tableUiCtrl($scope, $element, $attrs, $injector, $timeout, $window) {
        var self = this;
        self.tableIsLoaded = false;
        self.numberOfRows = undefined;
        self.displayedRowsNumber = [];

        if (self.config.quantityOfRecords) {
            self.config.quantityOfRecordsOptions.forEach(function (item, index) {
                self.displayedRowsNumber.push({
                    id: index,
                    number: +item
                })
            })
            number = self.displayedRowsNumber[0].number;
        }
        var windowObject = angular.element($window),
            mainTable = angular.element(document.querySelector('.ag-bootstrap'));
        self.agGridOptions = self.config.gridOptions;
        // function serviceInjector(serviceName) {
        //     var service = $injector.get(serviceName);
        //     return service;
        // }

        //var service = serviceInjector(self.serviceName);

        self.service = self.serviceName;

        self.agGridData = [];

        self.allOfTheData = undefined;
        var number = self.agGridOptions.paginationPageSize, // Number of entries showed per page.
            order,
            orderBy;
        self.currentPage = 0;
        self.totalPages = undefined;
        self.pager = [];
        self.startRow = undefined;
        self.endRow = undefined;
        self.pages = undefined;
        self.firstLoad = false;

        function createNewDatasource() {
            if (!self.allOfTheData) {
                // in case user selected 'onPageSizeChanged()' before the json was loaded
                return;
            }

            var dataSource = {
                //rowCount: ???, - not setting the row count, infinite paging will be used
                getRows: function (params) {
                    if (self.endRow) {
                        params.endRow = self.endRow;
                        params.startRow = self.startRow;
                    }

                    //console.log('asking for ' + params.startRow + ' to ' + params.endRow);
                    self.currentPage = (params.endRow / number) - 1;

                    if (params.sortModel.length !== 0) {
                        orderBy = params.sortModel[0].colId;
                        order = params.sortModel[0].sort;

                        self.service.setSort(number, orderBy, order, self.currentPage).then(function (result) {
                            self.allOfTheData = result.data;
                            self.pages = result.total;
                            self.totalPages = result.numberOfPages;
                            self.firstLoad = true;
                            setTableHeight();

                            var lastRow = -1;
                            if (self.pages <= params.endRow) {
                                lastRow = self.pages - 1;
                            }
                            self.setPage(self.currentPage, true);
                            params.successCallback(self.allOfTheData, self.pages);


                        });
                        return;
                    }

                    //if (params)
                    //pagination content getter
                    if (params.startRow !== 0) {
                        self.service.getPage(number, self.currentPage).then(function (result) {
                            self.agGridData.length = 0;
                            self.allOfTheData = result.data;
                            self.pages = result.total;
                            self.totalPages = result.numberOfPages;
                            self.firstLoad = false;
                            setTableHeight();
                            // see if we have come to the last page. if we have, set lastRow to
                            // the very last row of the last page. if you are getting data from
                            // a server, lastRow could be returned separately if the lastRow
                            // is not in the current page.
                            var lastRow = -1;
                            if (self.pages <= params.endRow) {
                                lastRow = self.pages - 1;
                            }
                            params.successCallback(self.allOfTheData, self.pages);

                        });
                        return;
                    }
                    if (params.startRow === 0) {
                        //firts page content getter
                        if (!self.firstLoad) {
                            self.service.getPage(number, self.currentPage).then(function (result) {
                                self.agGridData.length = 0;
                                self.allOfTheData = result.data;
                                self.pages = result.total;
                                self.totalPages = result.numberOfPages;
                                setTableHeight();

                                // see if we have come to the last page. if we have, set lastRow to
                                // the very last row of the last page. if you are getting data from
                                // a server, lastRow could be returned separately if the lastRow
                                // is not in the current page.
                                var lastRow = -1;
                                if (self.pages <= params.endRow) {
                                    lastRow = self.pages - 1;
                                }
                                params.successCallback(self.allOfTheData, self.pages);
                                self.firstLoad = true;
                                self.setPage(0, true);
                                self.agGridOptions.api.sizeColumnsToFit();
                            });
                            return;
                        }
                        self.pages = self.pages;
                        self.totalPages = Math.ceil(self.pages / number);

                        // see if we have come to the last page. if we have, set lastRow to
                        // the very last row of the last page. if you are getting data from
                        // a server, lastRow could be returned separately if the lastRow
                        // is not in the current page.
                        var lastRow = -1;
                        if (self.pages <= params.endRow) {
                            lastRow = self.pages - 1;
                        }
                        //self.setPage(0, true);
                        params.successCallback(self.allOfTheData, self.pages);
                        $timeout(function () {
                            if (window.innerWidth < 1600 && self.agGridOptions.columnDefs.length > 6) {
                                self.agGridOptions.columnApi.autoSizeColumns(self.agGridOptions.columnDefs)
                                return;
                            }
                            self.agGridOptions.api.sizeColumnsToFit();
                            self.tableIsLoaded = true;
                        }, 500)
                    }
                }
            };

            self.agGridOptions.api.setDatasource(dataSource);
        }

        function setRowData(rowData, pages) {
            self.allOfTheData = rowData;
            self.pages = pages;
            setTableHeight();
            createNewDatasource();
        }

        self.config.preloadData.then(function (result) {
            setRowData(result.data, result.total);
            self.tableIsLoaded = true;
            self.firstLoad = true;
        });

        $scope.$on('table.setFilter', function (e, filter) {
            if (self.config.quantityOfRecords) {
                number = self.numberOfRows.number;
            }
            // show 'loading' overlay
            self.agGridOptions.api.showLoadingOverlay();
            self.service.setFilter(filter).then(function (result) {
                if (result.total) {
                    setRowData(result.data, result.total);
                    self.firstLoad = true;
                    return;
                }
                self.agGridData.length = 0;
                setRowData(self.agGridData, result.total);
                self.agGridOptions.api.refreshView();
                self.agGridOptions.api.showNoRowsOverlay();
            });

        });

        $scope.$on('table.update', function (e, data) {
            if(data.selectedRows) {
                self.allOfTheData.forEach(function (item) {
                    data.selectedRows.forEach(function (subItem) {
                        if(+item.id === +subItem.id) {
                            item.active = data.newStatus;
                        }
                    });
                    self.agGridOptions.api.refreshView();
                    self.agGridOptions.api.hideOverlay();
                });

                return;
            }
            //delete and update data in table
            if(data.remove) {
                self.allOfTheData.forEach(function (item) {
                    if (data.alert_id === item.id) {
                        self.allOfTheData.splice(self.allOfTheData.indexOf(item), 1);
                    }
                });
                setRowData(self.allOfTheData, self.pages);
                self.agGridOptions.api.hideOverlay();
                return;
            }

            self.allOfTheData.forEach(function (item) {
                if (data.alert_id === item.id && data.new_status !== item.status) {
                    item.status = data.new_status;
                }
                self.agGridOptions.api.refreshView();
                self.agGridOptions.api.hideOverlay();
            });
        });

        self.quantitySelect = function () {
            self.firstLoad = false;
            self.agGridOptions.paginationPageSize = self.numberOfRows.number;
            number = self.numberOfRows.number;
            self.setPage(0);
        }

        $scope.$on('table.export', function (e, data) {

            var params = {
                skipHeader: false,
                fileName: 'Report.csv'

            };
            self.agGridOptions.api.exportDataAsCsv(params);
        });

        windowObject.on("resize.doResize", function () {
            $timeout(function () {
                if (window.innerWidth > 1600) {
                    self.agGridOptions.api.sizeColumnsToFit();
                    return;
                }
            }, 1000);

            if (window.innerWidth < 1600) {
                self.agGridOptions.columnApi.autoSizeColumns(self.agGridOptions.columnDefs);
            }

        });

        $scope.$on("$destroy", function () {
            windowObject.off("resize.doResize"); //remove the handler added earlier
        });

        $scope.$on('table.selectAllRows', function (e, data) {
            self.agGridOptions.api.selectAll();
        });

        $scope.$on('table.deselectAllRows', function (e, data) {
            self.agGridOptions.api.deselectAll();
        });


        function setTableHeight() {
            var tableHeight;
            if (self.allOfTheData.length >= 20) {
                tableHeight = (self.allOfTheData.length * self.agGridOptions.rowHeight) + self.agGridOptions.rowHeight;
                mainTable.height(tableHeight);
                return;
            }
            if (self.allOfTheData.length === 0) {
                tableHeight = self.agGridOptions.rowHeight * 3;
                mainTable.height(tableHeight);

            } else {
                tableHeight = (self.allOfTheData.length * self.agGridOptions.rowHeight)
                    + self.agGridOptions.rowHeight;
                mainTable.height(tableHeight);
            }
        }

        //pagination starts here

        self.paginationFirstLoad = true;
        self.firstDisplayedPage = undefined;
        self.lastDisplayedPage = undefined;
        self.originPagerLength = self.config.paginationSize;

        self.setPage = function (page, load) {
            self.config.paginationSize = self.originPagerLength;
            if (self.totalPages < 5) {
                self.config.paginationSize = self.totalPages;
            }
            var paginationCount = self.currentPage,
                pagesDelta = page - self.currentPage,
                total = self.totalPages - 1;

            if (self.pager.length !== 0) {
                if (self.currentPage > page && page < self.firstDisplayedPage.id
                    && page !== 0) {
                    var leftSidePages;
                    if (pagesDelta === -(self.config.paginationSize - 1)) {
                        pagesDelta = pagesDelta + 2;
                    }

                    var leftSidePages = page;
                    for (var i = 0; i < -pagesDelta; i++) {
                        self.pager.pop();
                        self.pager.unshift({
                            id: leftSidePages - 1,
                            pageNum: leftSidePages
                        });
                        leftSidePages--
                    }

                }
                if (self.currentPage < page && page > self.lastDisplayedPage.id
                    && page !== total) {
                    if (pagesDelta === (self.config.paginationSize - 1)) {
                        pagesDelta = +pagesDelta - 2;
                    }

                    var rightSidePages = page;
                    for (var i = 0; i < +pagesDelta; i++) {
                        self.pager.push({
                            id: rightSidePages + 1,
                            pageNum: rightSidePages + 2
                        });
                        self.pager.shift();
                        rightSidePages++;
                    }
                }
            }


            if (self.paginationFirstLoad || self.pager.length === 0 || page === 0) {
                self.pager.length = 0;
                paginationCount = page;
                for (var i = 0; i < self.config.paginationSize; i++) {
                    self.pager.push({
                        id: paginationCount,
                        pageNum: paginationCount + 1
                    });
                    paginationCount++
                }
            }

            if (page === total) {
                self.pager.length = 0;
                paginationCount = self.totalPages - self.config.paginationSize;
                for (var i = paginationCount; i < self.totalPages; i++) {
                    self.pager.push({
                        id: paginationCount,
                        pageNum: paginationCount + 1
                    });
                    paginationCount++
                }
            }

            self.firstDisplayedPage = self.pager[1];
            self.lastDisplayedPage = self.pager[self.pager.length - 2];
            self.currentPage = page;

            self.endRow = (page * number) + number;
            self.startRow = page * number;
            if (!load) {
                createNewDatasource();
            }
            self.paginationFirstLoad = false;
        };

        self.setPage(0);

    }

})();