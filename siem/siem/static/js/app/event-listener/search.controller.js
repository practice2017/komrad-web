(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .controller('searchController', searchCtrl)
        .controller('saveQueryCtrl', saveQueryCtrl);

    searchCtrl.$inject = ['$rootScope', '$scope', '$interval', '$http', '$window', '$document', 'Notification',
        'FileSaver', 'Blob', '$uibModal', 'SearchContent', '$filter', 'moment', 'widget', 'duration',
        'query', 'QueriesContent', '$q', '$timeout', 'ngPanels', 'eventFields', 'SortedContent',
        'SearchQuery', 'FieldsTransform', 'fields', 'ivhTreeviewBfs', 'eventViewer'];

    saveQueryCtrl.$inject = ['$scope', '$http', '$filter', 'Notification', '$uibModalInstance', '$state'];

    function searchCtrl($rootScope, $scope, $interval, $http, $window, $document, Notification, FileSaver, Blob,
                        $uibModal, SearchContent, $filter, moment, widget, duration, queryObj,
                        QueriesContent, $q, $timeout, ngPanels, eventFields, SortedContent,
                        SearchQuery, FieldsTransform, fields, ivhTreeviewBfs, eventViewer) {

        $scope.closed = false;
        $scope.rangeMin = 0;
        $scope.rangeMax = 0;
        $scope.minDate = '';
        $scope.maxDate = '';
        $scope.query_name = '';
        $scope.query_description = '';
        $scope.rule = {};
        $scope.tableHeaders = [];
        $scope.loading_status = '';
        $scope.rangeFlag = false;
        $scope.sortFlag = false;
        $scope.fullReq = false;
        $scope.emptyTable = false;
        $scope.messageTypeHeader = undefined;
        $scope.queries = new QueriesContent();
        $scope.queries.loadData();
        $scope.dropdown = [];
        $scope.sortType = ''; // значение сортировки по умолчанию количество
        $scope.sortReverse = {};  // обратная сортировка количество

        $scope.testLeftFields = fields;
        if (!$rootScope.testLeftFields) {
            $rootScope.testLeftFields = $scope.testLeftFields;
        }


        $scope.$on('fieldsHeaders.empty', function (e, data) {
            if (data) {
                $scope.emptyTable = true;
                return;
            }
            $scope.emptyTable = false;
        });

        $scope.testLeftFields.forEach(function (item) {
            if (item.children) {
                item.children.forEach(function (subItem) {
                    if (subItem.name === 'event_id' ||
                        subItem.name === 'data') {
                        subItem.selected = true;
                    }
                    $scope.tableHeaders.push({
                        id: subItem.name,
                        label: subItem.label,
                        display: false
                    });
                });
            }
        });

        //pushing data_type to sort select
        $scope.data_items = [];
        eventFields.getFaceted().then(function (data) {
            for (var f in data) {
                $scope.data_items.push({
                    id: data[f].id,
                    name: data[f].name,
                    label: data[f].label
                })
            }
        });

        $scope.data_items.selectedOption = undefined;

        var filters = FieldsTransform.transform();

        //get filter fields for qb
        $scope.qbFilters = filters;
        $scope.qbPlugins = ['sortable', 'mapping2', 'datetimepicker', 'list_search'];
        $scope.qbLanguage = 'ru';

        //setrules func for qb
        function setRules(rules) {
            if (typeof rules === 'string') {
                rules = JSON.parse(rules);
            }
            $scope.$broadcast('qb.setRules', rules);
        }

        // directive callback function
        $scope.callbackQuery = function (item) {
            if (item.body) {
                $scope.$broadcast('qb.reset');
                setRules(item.body);
                $scope.query_name = item.readableName;
                $scope.query_description = item.description;
            } else {
                return;
            }
        };
        //input-dropdown data wait
        $scope.dataWaiting = function () {
            var query = $q.defer();
            var dropdown = [];
            $timeout(function () {
                $scope.queries.items.forEach(function (item) {
                    dropdown.push({
                        readableName: item.readableName,
                        id: item.id,
                        created: item.created,
                        body: item.body,
                        description: item.description,
                        role_access: item.role_access
                    });
                });

                query.resolve(dropdown); // через какое-то время (асинхронно) - исполняем промис
            }, 1000);

            return query.promise;
        };

        $scope.dataWaiting()
            .then(successGetData, errorGetData);

        function successGetData(value) {
            value.forEach(function (item) {
                $scope.dropdown.push({
                    readableName: item.readableName,
                    id: item.id,
                    created: item.created,
                    body: item.body,
                    description: item.description,
                    role_access: item.role_access
                });
            });
        }

        function errorGetData() {
            return;
        }

        $scope.objectMessage = '';

        $scope.queryObject = null;

        $scope.filterObjectList = function (userInput) {
            var filter = $q.defer();
            var normalisedInput = userInput.toLowerCase();

            var filteredArray = $scope.dropdown.filter(function (query) {
                var matchQueryName = query.readableName.toLowerCase().indexOf(normalisedInput) === 0;

                return matchQueryName;
            });

            filter.resolve(filteredArray);
            return filter.promise;
        };


        $scope.rangeReset = function () {
            $scope.rangeMin = 0;
            $scope.rangeMax = 0;
            $scope.setQuery();
        };

        $scope.$on('$viewContentLoaded', function () {
            if (queryObj) {
                $scope.rule = queryObj.body;
                $scope.query_name = queryObj.name;
                $scope.query_description = queryObj.description;
                $scope.$on('qb.inited', function () {
                    $scope.$broadcast('qb.setRules', $scope.rule);
                    $scope.setQuery();
                })
                return;
            }
            if (!widget) {
                return;
            }

            $scope.rule = widget.data.settings.query_body;
            // если график имеет какотето пределенное значение для фасета,
            // то добавляем еще одно правило

            var additional_rule = [];
            var duration_rule = [];
            if (duration.ef_value) {
                additional_rule = {
                    "id": widget.data.settings.facet[0].name,
                    "field": widget.data.settings.facet[0].name,
                    "type": widget.data.settings.facet[0].type,
                    "input": "text",
                    "operator": "equal",
                    "value": duration.ef_value
                }
            }

            if (duration.from && duration.to) {
                $scope.rule = {
                    condition: 'AND',
                    rules: [{
                        "id": "timestamp",
                        "field": "timestamp",
                        "type": "datetime",
                        "input": "text",
                        "operator": "between",
                        "value": [
                            +duration.from,
                            +duration.to
                        ]
                    }
                    ].concat(additional_rule)
                        .concat(widget.data.settings.query_body)
                }
            }
            else {
                $scope.rule = widget.data.settings.query_body;
            }
            $scope.$on('qb.inited', function () {
                $scope[widget.data.settings.data_type] = true;
                $scope.$broadcast('qb.setRules', $scope.rule);
                $scope.setQuery();
            })
        });

        $scope.event_id = true;
        $scope.data = true;

        // check: is object empty?
        function convertObjectDates(obj) {
            return true;
        }

        //call http services
        function requester(rules) {
            $scope.content = {};
            if ($filter('isEmpty')(rules)) {
                $scope.content.message = 'Ошибка при построении запроса';
                return;
            }

            $scope.iconDesc = false;

            if (!$scope.data_items.selectedOption) {
                $scope.content = new SearchContent(rules);
                $scope.content.nextPage($scope.rangeMin, $scope.rangeMax);
                $scope.fullReq = true;
                $scope.sortFlag = false;
                rangeChart(rules);
            } else {
                $scope.fullReq = false;
                $scope.rangeFlag = false;
                $scope.sortFlag = true
                $scope.sorted_content = new SortedContent(rules, $scope.data_items.selectedOption.name);
                $scope.sorted_content.getSorted().then(successSort, errorSort);
            }

            function successSort(data) {
                $scope.sortedData = data;
                $scope.sortedData.forEach(function (item) {
                    setTimeout(function () {
                        var chart = item.chart.getHighcharts();
                        chart.reflow();
                    }, 200);
                });
                $scope.sortFlag = true;
            }

            function errorSort(err) {
                return;
            }

            $scope.reverse = true;

            $scope.setOrderAndReload = function (name) {
                if ($scope.iconDesc === name) {
                    $scope.reverse = !$scope.reverse;
                }
                if ($scope.iconDesc !== name) {
                    $scope.reverse = true;
                }
                $scope.iconDesc = name;
                $scope.content.nextPage($scope.rangeMin, $scope.rangeMax, name, $scope.reverse);
            };

            function rangeChart(qb) {
                function reflow() {
                    var c = this;
                    setTimeout(function () {
                        c.reflow();
                    }, 0);
                }

                var reqBody = {
                    query: qb,
                    from: $scope.rangeMin,
                    to: $scope.rangeMax
                };

                $scope.rangeFlag = false;

                $scope.rangeChart = {};


                $http.post('/range_slider', angular.toJson(reqBody))
                    .success(requestComplete)
                    .error(requestFail);

                function requestComplete(response) {

                    $scope.rangeChart.options = {
                        chart: {
                            type: 'column',
                            zoomType: 'x',
                            events: {
                                selection: function (event) {
                                    var text,
                                        label;
                                    if (event.xAxis) {
                                        $scope.rangeMin = Math.round(event.xAxis[0].min / 1000);
                                        $scope.rangeMax = Math.round(event.xAxis[0].max / 1000);
                                        $scope.minDate = "От: " + moment.unix($scope.rangeMin).format('DD. MMM YYYY, HH:mm:ss');
                                        $scope.maxDate = "До: " + moment.unix($scope.rangeMax).format('DD. MMM YYYY, HH:mm:ss');
                                        $scope.setQuery();
                                    } else {
                                        text = 'Selection reset';
                                    }
                                },
                            },
                            load: reflow,
                            addSeries: reflow
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    };
                    $scope.rangeChart.title = {
                        text: ''
                    };
                    $scope.rangeChart.xAxis = {
                        type: 'datetime'
                    };
                    $scope.rangeChart.yAxis = {
                        min: 0,
                        title: {
                            text: 'События'
                        }
                    };
                    $scope.rangeChart.plotOptions = {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    };
                    $scope.rangeChart.credits = {
                        enabled: false
                    };
                    $scope.rangeChart.series = [{
                        name: "Количество",
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                }
                            }
                        },
                        pointPadding: 1,
                        groupPadding: (9900 / 10000),
                        color: '#63A8EB',
                        data: []
                    }];

                    $scope.rangeFlag = true;
                    var settings = response.result;

                    setTimeout(function () {
                        var chart = $scope.rangeChart.getHighcharts();

                        var newSeries = [];
                        settings.forEach(function (point) {
                            newSeries.push({
                                x: +point.x,
                                y: +point.y
                            });

                        });

                        chart.series[0].setData(newSeries, true);
                        chart.reflow();
                    }, 200)

                }

                function requestFail(err) {
                    return;
                }
            }

            $scope.getAllRowInfo = function (data) {
                $scope.rangeMin = 0;
                $scope.rangeMax = 0;
                var queryBuilderEl = angular.element(document.querySelector('#builder'));
                var rowRules = {
                    condition: "AND",
                    rules: [{
                        field: $scope.data_items.selectedOption.name,
                        id: $scope.data_items.selectedOption.name,
                        input: 'text',
                        operator: 'equal',
                        type: 'string',
                        value: data.message_type
                    }]
                };

                $scope.data_items.selectedOption = undefined;
                $scope.content = new SearchContent(rowRules);
                $scope.content.nextPage($scope.rangeMin, $scope.rangeMax);
                $scope.fullReq = true;
                $scope.sortFlag = false;
                rangeChart(rowRules);
                queryBuilderEl.queryBuilder('setRules', rowRules);

            };
        }


        //listen for resolveGetRules qb-directive event and launch requester
        $scope.$on('resolveGetRules', function (e, rules) {
            requester(rules);
        });

        $scope.fieldViewSwitcher = function (name) {
            $scope[name] = !$scope[name];
        }
        //set qury for queryBuilder
        $scope.setQuery = function () {
            if ($scope.data_items.selectedOption) {
                $scope.messageTypeHeader = $scope.data_items.selectedOption.label;
            }
            $scope.$broadcast('qb.getRules');
        };

        $scope.resetQuery = function () {
            $scope.rangeMin = 0;
            $scope.rangeMax = 0;
            $scope.setQuery();
        };

        $scope.modal = $uibModal;

        //open save request modal for queryBuilder
        $scope.saveQB = function () {
            $scope.modal.open({
                scope: $scope,
                templateUrl: 'saveQueryDialog.html',
                controller: 'saveQueryCtrl',
                windowClass: "app-modal-window",
                size: 'md'
            });


        };

        //listen for save.qbQuery saveQueryCtrl event and translate it to qb-directive
        $scope.$on('save.qbQuery', function () {
            $scope.saveQuery = true;
            $scope.$broadcast('qb.getRules', $scope.saveQuery);
        });

        //listen for save answer from qb-directive and translate it to saveQueryCtrl
        $scope.$on('qb.saveQuery', function (e, rules) {
            $scope.$broadcast('qb.querySave', rules);
        });

        $scope.open = function (id) {
            eventViewer.open(id);
        };

        //export result-table to csv
        $scope.exportData = function () {
            var table = angular.element(document.querySelector('.exportable-result'));
            table.TableCSVExport({
                delivery: 'download',
                filename: 'Report.csv'
            });
            //var table2 = $('.exportable-result');
            //console.log(table)
            //console.log(table2)
        };

        //delete query from queryBuilder
        $scope.deleteQuery = function () {
            $uibModal.open({
                scope: $scope,
                templateUrl: 'deleteQueryDialog.html',
                controller: 'saveQueryCtrl',
                windowClass: "app-modal-window",
                size: 'sm'
            });
        };

        $scope.panels = ngPanels.newGroup('demo', {
            one: {},
            two: {
                masks: ['one']
            }
        });

        $scope.openPanelTwo = function () {
            $scope.panels.open('two');
        };

        //listen for deleteQuery saveQueryCtrl event and drop params to default
        $scope.$on('deleteQuery', function () {
            $scope.$broadcast('qb.reset');
            $scope.rangeMin = 0;
            $scope.rangeMax = 0;
            $scope.rangeFlag = false;
            $scope.rangeChart.options = {};
            $scope.minDate = "";
            $scope.maxDate = "";
        });

    }

    function saveQueryCtrl($scope, $http, $filter, Notification, $uibModalInstance, $state) {
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.$on('qb.querySave', function (e, rules) {
            if ($filter('isEmpty')(rules)) {
                return;
            }

            $http.post('/query/body',
                JSON.stringify({
                    query_name: $scope.query_name,
                    query_body: rules,
                    query_description: $scope.query_description,
                    query_id: $state.params.queryId
                })
            ).success(function (data, status) {
                Notification.success('Успешно сохранено');
                $uibModalInstance.dismiss('cancel');
            }).error(function (data) {
                Notification.error('Ошибка при записи данных');
            })
        });

        $scope.saveQuery = function () {
            $scope.$emit('save.qbQuery');
        };

        $scope.restore = function () {
            $scope.$emit('deleteQuery', [1]);

            var input = angular.element(document.querySelector(
                ".input-dropdown input"));
            input.val('').trigger('change');
            $uibModalInstance.dismiss('cancel');
        };
    }

})();
