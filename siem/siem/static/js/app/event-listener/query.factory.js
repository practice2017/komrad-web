(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .factory('SearchQuery', queryFactory);

    queryFactory.$inject = ['$http', 'Notification', '$q'];

    function queryFactory($http, Notification, $q) {
        var Query = function () {
            this.id = undefined,
            this.body = undefined,
            this.created = undefined,
            this.name = '',
            this.description = ''
        };

        Query.prototype.get = function (id) {
            var deferred = $q.defer();
            $http.get('/query/body/' + id)
            .success(function (data) {
                this.id = data.id;
                this.body = data.body;
                this.created = data.created;
                this.name = data.name;
                this.description = data.description;
                deferred.resolve(this);
            }.bind(this))
            .error(function (data) {
            }.bind(this))
            return deferred.promise;
        }
        return Query;
    }


})();
