(function() {
    'use strict';

    angular.module('KomradeApp.event_listener', [
        'KomradeApp.core',
        'ngAnimate', 
        'ui.router',
        'ncy-angular-breadcrumb',
        'highcharts-ng',
        'angularSpinner', 
        'ui.bootstrap', 
        'gridster', 
        'ui-notification', 
        'ngFileSaver',
        'smart-table', 
        'angularMoment',
        'infinite-scroll',
        'emguo.poller',
        'inputDropdown',
        'mgo-angular-wizard',
        'ui.layout',
        'ngPanels',
        'doubleScrollBars',
        'treeView'
    ]);

})();
