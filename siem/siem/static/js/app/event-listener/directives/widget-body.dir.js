(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('widgetBody', widgetBodyDir)

    widgetBodyDir.$inject = ['$compile'];

    /**
     * [widgetBodyDir description]
     * @param  {[type]} $compile [description]
     * @return {[type]}          [description]
     */
    function widgetBodyDir($compile) {

        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                scope.getContentUrl = function() {
                    // setup template name by wtype field
                    return attrs.wtype + 'WBT.html';
                }
                // create a new angular element from the resource in the
                // inherited scope object so it can compile the element
                // the item element represents the custom widgets
                var newEl = angular.element(
                    '<div style="height:100%" ng-include="getContentUrl()"></div>');
                // using jQuery after new element creation, to append element
                element.append(newEl);
                // returns a function that is looking for scope
                // use angular compile service to instanitate a new widget element
                $compile(newEl)(scope);
            }
        }
    }
})();
