(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('inlineEdit', inlineEditDir);

    function inlineEditDir() {
        return{
            restrict: 'A',
            transclude: true,
            template: '<label class="editing" data-ng-transclude></label>',
            scope: {
                ngModel : '=',
            },
            require: 'ngModel',
            controller: ['$scope','$element','$transclude', inlineEditCtrl]
        };
    };

    function inlineEditCtrl($scope, $element, $transclude) {
        $transclude(function(clone) {
            $scope.transcluded_content = clone[0].textContent;
        });
        $element.bind('click', function(){
            $element.hide().after('<input type="text" ng-model="ngModel" value="' +
                $scope.transcluded_content+'" />');

            $element.next().focus().blur(function (){
                $scope.transcluded_content = $element.next().val();
                $element.html($scope.transcluded_content);
                $element.next().hide();
                $element.show();
            });
        });
    };

})();

