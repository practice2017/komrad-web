(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('filledDropdown', filledDropdownDir);

    filledDropdownDir.$inject = ['$filter'];

    function filledDropdownDir($filter) {
        return {
            restrict: 'E',
            require: '^ngModel',
            scope: {
                default: '=', // default value
                ngModel: '=', // selection
                items: '=',   // items to select from
                callback: '&' // callback
            },
            link: function(scope, element, attrs) {
                element.on('click', function(event) {
                    event.preventDefault();
                });
                scope.found = $filter('getByField')(scope.items, scope.default);
                if (!scope.found){
                    scope.found = {name:'поле из...'}
                }
                if (scope.default != scope.ngModel){
                    scope.ngModel = scope.found;
                }

                // console.log(found);
                // scope.callback({ item: found });
                // scope.default = found;
                // scope.isButton = 'isButton' in attrs;

                // selection changed handler
                scope.select = function(item) {
                    scope.ngModel = item;
                    scope.found = item;
                    if (scope.callback) {
                        scope.callback({ item: item });
                    }
                };
            },
            template:
                '<div class="btn-group" uib-dropdown >\
                  <!-- button style uib-dropdown -->\
                  <button ng-if="isButton" type="button"\
                          class="btn btn-primary"\
                          ng-bind="ngModel.name || default">\
                  </button>\
                  <button ng-if="isButton" type="button" class="btn btn-primary \
                          uib-dropdown-toggle" uib-dropdown-toggle>\
                    <span class="caret"></span>\
                  </button>\
                  <!-- no button, plz -->\
                  <a ng-if="!isButton" href\
                     class="btn btn-primary uib-dropdown-toggle"\
                     uib-dropdown-toggle>\
                    {{found.name}} <span class="caret"></span>\
                  </a>\
                  <!-- dropdown items -->\
                  <ul class="uib-dropdown-menu" role="menu">\
                    <li ng-repeat="item in items">\
                      <a href="#"\
                         ng-bind="item.name"\
                         ng-click="select(item)"></a>\
                    </li>\
                  </ul>\
                </div>'
        };
    }

})();
