(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('widgetConfigLeft', widgetConfigL)
        .directive('widgetConfigRight', widgetConfigR);


    // widgetConfig directive duration and update period
    function widgetConfigL() {
        var directiveDefinitionObject = {

            // шаблон задан как ссылка или выражение

            templateUrl: "widget_configLeft.html",

            // применение директивы ограничивается

            restrict: 'A',

            // контроллер для директивы

            controller: function ($scope, $element, $attrs, $transclude) {
                var graphSelect = $element.find('.graphSelect');
                var updatePeriod = $element.find('.period');
                var duration = $element.find('.duration');
                var durStripe = $element.find('.durStripe');
                graphSelect.on('change', function () {
                    if (graphSelect.val() === 'line') {
                        $scope.widget_settings.data_type = undefined;
                        $scope.lineGraphConfig = true;
                        $scope.areaConfigFlag = true;
                        $scope.graph3d = false;
                    } else if (graphSelect.val() === 'column' ||
                        graphSelect.val() === 'pie') {
                        $scope.widget_settings.plot_duration = $scope.widget_settings.update_period;
                        $scope.graph3d = true;
                        $scope.lineGraphConfig = false;
                        $scope.areaConfigFlag = false;
                    } else if (graphSelect.val() === 'polar') {
                        $scope.widget_settings.plot_duration = $scope.widget_settings.update_period;
                        $scope.lineGraphConfig = false;
                        $scope.areaConfigFlag = true;
                        $scope.graph3d = false;
                    } else {
                        $scope.lineGraphConfig = false;
                        $scope.graph3d = false;
                        $scope.lineGraphConfig = false;
                        $scope.areaConfigFlag = false;
                    }

                    if (graphSelect.val() === 'pie' ||
                        graphSelect.val() === 'polar') {
                        duration.attr('disabled', 'disabled');
                        duration.val(updatePeriod.val());
                        $scope.disableFlag = true;
                    } else {
                        duration.removeAttr('disabled');
                    }
                });

                $scope.checkDuration = function(){
                    return +duration.val() < +updatePeriod.val();
                }

                updatePeriod.on('input', function () {
                    if (graphSelect.val() === 'pie' ||
                        graphSelect.val() === 'polar' ||
                        $scope.chartFlag === true) {
                        duration.val(updatePeriod.val());
                        $scope.widget_settings.plot_duration = +duration.val();
                    }

                });

                $scope.getGraphOptions = function () {
                    if ($scope.widget_settings.graph_type == 'line' &&
                        $scope.areaChartFlag == true) {
                        $scope.widget_settings.graph_type = 'area';
                    }
                    if ($scope.widget_settings.graph_type == 'line' &&
                        $scope.facetedChartFlag == true) {
                        $scope.widget_settings.graph_type = 'facetedLine';
                    }
                    if ($scope.widget_settings.graph_type == 'column' &&
                        $scope.chartFlag == true) {
                        $scope.widget_settings.graph_type = 'column3d';
                    }
                    if ($scope.widget_settings.graph_type == 'pie' &&
                        $scope.chartFlag == true) {
                        $scope.widget_settings.graph_type = 'pie3d';
                    }
                    if ($scope.widget_settings.graph_type == 'polar' &&
                        $scope.areaConfigFlag == true){
                        $scope.$emit('changeToArea')
                    }
                }

                $scope.setGraphOptions = function () {
                    if ($scope.widget_settings.graph_type == 'area') {
                        $scope.widget_settings.graph_type = 'line';
                        $scope.areaChartFlag = true;
                        $scope.lineGraphConfig = true;
                        $scope.areaConfigFlag = true;
                        $scope.graph3d = false;
                    }
                    if ($scope.widget_settings.graph_type == 'facetedLine') {
                        $scope.widget_settings.graph_type = 'line';
                        $scope.facetedChartFlag = true;
                        $scope.lineGraphConfig = true;
                        $scope.areaConfigFlag = true;
                        $scope.graph3d = false;
                    }
                    if ($scope.widget_settings.graph_type == 'line') {
                        $scope.lineGraphConfig = true;
                        $scope.areaConfigFlag = true;
                        $scope.graph3d = false;
                    }
                    if ($scope.widget_settings.graph_type == 'column3d') {
                        $scope.widget_settings.graph_type = 'column';
                        $scope.chartFlag = true;
                        $scope.lineGraphConfig = false;
                        $scope.areaConfigFlag = false;
                        $scope.graph3d = true;
                    }
                    if ($scope.widget_settings.graph_type == 'pie3d') {
                        $scope.widget_settings.graph_type = 'pie';
                        $scope.chartFlag = true;
                        $scope.lineGraphConfig = false;
                        $scope.areaConfigFlag = false;
                        $scope.graph3d = true;
                    }
                    if ($scope.widget_settings.graph_type == 'pie' ||
                        $scope.widget_settings.graph_type == 'column') {
                        $scope.lineGraphConfig = false;
                        $scope.areaConfigFlag = false;
                        $scope.graph3d = true;
                    }
                    /*if(условие настроек чарта polar ){
                        $scope.areaConfigFlag = true;
                        $scope.chartFlag = true;
                    }*/
                }

                function filterGraph(items, graphType) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].id == graphType) {
                            graphType = items[i];
                            return graphType;
                        }
                    }
                }

                $scope.setGraphOptions();
                $scope.widget_settings.graph_type = filterGraph($scope.items,
                    $scope.widget_settings.graph_type);

            },

            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

            }

        }

        return directiveDefinitionObject;
    }

    // widgetConfig directive show delta and events count
    function widgetConfigR() {
        var directiveDefinitionObject = {

            // шаблон задан как ссылка или выражение

            templateUrl: 'widget_configRight.html',

            // применение директивы ограничивается

            restrict: 'A',

            // контроллер для директивы

            controller: function ($scope, $element, $attrs, $transclude) {

                function filterDataType(items, dataType) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].id == dataType) {
                            dataType = items[i];
                            return dataType;
                        }
                    }
                }

                setTimeout(function () {
                    $scope.widget_settings.data_type = filterDataType($scope.data_items,
                        $scope.widget_settings.data_type);
                }, 20);



                $scope.setDelta = function () {
                    if ($scope.widget_settings.show_delta == 1) {
                        $scope.widget_settings.show_delta = true
                        $scope.showDFlag = true;
                    }
                    if ($scope.widget_settings.show_delta == 0) {
                        $scope.showDFlag = false;
                        $scope.widget_settings.show_delta = false;
                    }
                }


                $scope.setDataSort = function () {
                    if ($scope.widget_settings.sort == 0) {
                        $scope.widget_settings.sort = false;
                    }
                    if ($scope.widget_settings.sort == 1) {
                        $scope.widget_settings.sort = true;
                    }
                }

                $scope.setDataSort();
                $scope.setDelta();
            },

            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

            }

        }

        return directiveDefinitionObject;
    }
})();