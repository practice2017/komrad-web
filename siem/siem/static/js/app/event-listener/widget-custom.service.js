(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .factory('WidgetPuller', widgetPuller);

    widgetPuller.$inject = ['$http', '$interval', 'moment', '$state', 'eventFields', 'PluginsPuller'];

    // factory widgetPuller #14250
    function widgetPuller($http, $interval, moment, $state, eventFields, PluginsPuller) {

        //var plugins = new PluginsPuller();
        //plugins.getAllPlugins();
        // create new chart
        /**
         * возвращает chart с пустыми настройками в зависимости от выбранного графика
         * @param graphType
         * @returns {x.Chart}
         */
        function getChartConfig(settings) {
            function reflow() {
                var c = this;
                setTimeout(function () {
                    c.reflow();
                }, 0);
            }

            function hasXaxisLables(setts) {
                if (setts.plot_duration == setts.update_period)
                    return false;
                return true;
            }

            function getFacetLabel(facet_id) {
                if (!facet_id)
                    return "";
                if (settings.query_body[0] != "{")
                    return "";
                return eventFields.byId(facet_id).label + ':';
            }

            var graphType = settings.graph_type;

            if (graphType === 'line') {
                // basic line
                return {
                    options: {
                        chart: {
                            type: 'line',
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            valueSuffix: '',
                            formatter: function () {
                                return "Количество: " + this.point.y + '<br/>' + Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x);
                            }
                        },
                        plotOptions: {
                            line: {
                                marker: {
                                    enabled: true,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        }
                                    }
                                }
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_from = Math.floor(this.prev_ts / 1000);
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: Math.floor(this.x / 1000),
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'События'
                        }
                    },
                    credits: {
                        enabled: false
                    }
                };
            }
            if (graphType === 'pie') {
                // pie chart
                return {
                    options: {
                        chart: {
                            type: 'pie',
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">' + getFacetLabel(settings.facet[0].id) + ' {point.key}</span><br/>',
                            pointFormat: 'Количество: <b>{point.y}</b>'
                        },
                        title: {
                            text: ''
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_to = Math.floor(Date.now() / 1000);
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_to = Math.floor(this.timestamp / 1000);
                                                ts_from = Math.floor(this.prev_ts / 1000);
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: ts_to,
                                                    ef_value: this.name
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Количество',
                        data: []
                    }],
                };
            }
            if (graphType === 'column') {

                // basic column
                return {
                    options: {
                        chart: {
                            type: 'column',
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px"> Время:{point.key}</span><br/>' +
                            '<span>' + getFacetLabel(settings.facet[0].id) +
                            ' {series.name}' + '</span><br/><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_from = Math.floor(this.prev_ts / 1000)
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: Math.floor(this.x / 1000),
                                                    ef_value: this.series.name
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        labels: {
                            enabled: hasXaxisLables(settings)
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'События'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: []
                };
            }
            if (graphType === 'polar') {

                // wind rose
                return {
                    options: {
                        chart: {
                            polar: true,
                            type: 'area',
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            shared: false,
                            headerFormat: '<span style="font-size:10px">' + getFacetLabel(settings.facet[0].id) + ' {point.key}</span><br/>',
                            pointFormat: '<span style="color:{series.color}"> Количество: <b>{point.y:,.0f}</b><br/>'
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_to = Math.floor(Date.now() / 1000);
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_to = Math.floor(this.series.chart.options.ts_to / 1000);
                                                ts_from = Math.floor(this.series.chart.options.ts_from / 1000);
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: ts_to,
                                                    ef_value: this.category
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                    },
                    title: {
                        text: ''
                    },
                    pane: {
                        size: '80%'
                    },
                    xAxis: {
                        min: 0,
                        categories: [],
                        tickmarkPlacement: 'on'
                    },
                    yAxis: {
                        lineWidth: 0
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'vertical'
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        type: 'area',
                        pointPlacement: 'on',
                        name: 'Количество',
                        data: []
                    }]
                };
            }
            if (graphType === 'solidgauge') {

                // solid gauge
                return {
                    options: {
                        chart: {
                            type: 'solidgauge',
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            enabled: true,
                            valueSuffix: ' %'
                        },
                        pane: {
                            center: ['50%', '85%'],
                            size: '100%',
                            startAngle: -90,
                            endAngle: 90,
                            background: {
                                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                                innerRadius: '60%',
                                outerRadius: '100%',
                                shape: 'arc'
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    // the value axis
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        stops: [
                            [0.3, '#55BF3B'], // green
                            [0.5, '#DDDF0D'], // yellow
                            [0.8, '#DF5353'] // red
                        ],
                        lineWidth: 0,
                        minorTickInterval: null,
                        tickPixelInterval: 400,
                        tickWidth: 0,
                        labels: {
                            y: 16
                        },
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: 'Загрузка',
                        data: [],
                        dataLabels: {
                            format: '<div style="text-align:center;margin-bottom:-2px"><span style="font-size:12px;color:' +
                            (Highcharts.theme && Highcharts.theme.contrastTextColor) + '">{y}</span>' +
                            '</div>'
                        }
                    }]
                };
            } //dummy chart
            if (graphType === 'area') {

                // basic area
                return {
                    options: {
                        chart: {
                            type: 'area',
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            valueSuffix: '',
                            formatter: function () {
                                return "Количество: " + this.point.y + '<br/>' + Highcharts.dateFormat('%A, %b %e, %Y %H:%M:%S', this.x);
                            }
                        },
                        plotOptions: {
                            area: {
                                marker: {
                                    enabled: true,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        }
                                    }
                                }
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_from = Math.floor(this.prev_ts / 1000);
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: Math.floor(this.x / 1000),
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Cобытия'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Количество',
                        data: []
                    }]
                };
            }
            if (graphType === 'gauge') {

                //angular gauge
                return {
                    options: {
                        chart: {
                            type: 'gauge',
                            plotBackgroundColor: null,
                            plotBackgroundImage: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        pane: {
                            startAngle: -150,
                            endAngle: 150,
                            background: [{
                                backgroundColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, '#FFF'],
                                        [1, '#333']
                                    ]
                                },
                                borderWidth: 0,
                                outerRadius: '109%'
                            }, {
                                backgroundColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, '#333'],
                                        [1, '#FFF']
                                    ]
                                },
                                borderWidth: 1,
                                outerRadius: '107%'
                            }, {
                                backgroundColor: '#DDD',
                                borderWidth: 0,
                                outerRadius: '105%',
                                innerRadius: '103%'
                            }]
                        },
                        yAxis: {
                            min: 0,
                            max: 100,

                            minorTickInterval: 'auto',
                            minorTickWidth: 1,
                            minorTickLength: 10,
                            minorTickPosition: 'inside',
                            minorTickColor: '#666',

                            tickPixelInterval: 30,
                            tickWidth: 2,
                            tickPosition: 'inside',
                            tickLength: 10,
                            tickColor: '#666',
                            labels: {
                                step: 2,
                                rotation: 'auto'
                            },
                            title: {
                                text: '%'
                            },
                            plotBands: [{
                                from: 0,
                                to: 60,
                                color: '#55BF3B' // green
                            }, {
                                from: 60,
                                to: 80,
                                color: '#DDDF0D' // yellow
                            }, {
                                from: 80,
                                to: 100,
                                color: '#DF5353' // red
                            }]
                        },
                        tooltip: {
                            enabled: true,
                            valueSuffix: ' %'
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    // the value axis
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Загрузка',
                        data: [],
                        dataLabels: {
                            format: '<div style="text-align:center;margin-bottom:-2px"><span style="font-size:12px;color:' +
                            (Highcharts.theme && Highcharts.theme.contrastTextColor) + '">{y}</span>' +
                            '</div>'
                        }
                    }]
                };
            } //dummy chart
            if (graphType === 'facetedLine') {
                // line chart based on facets
                return {
                    options: {
                        chart: {
                            type: 'line',
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px"> Время:{point.key}</span><br/>' +
                            getFacetLabel(settings.facet[0].id) + '{series.name}<table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        plotOptions: {
                            series: {
                                marker: {
                                    enabled: false
                                },
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_from = 1367040610;
                                            if (settings.show_delta) {
                                                ts_from = Math.floor(this.prev_ts / 1000)
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: Math.floor(this.x / 1000),
                                                    ef_value: this.series.name
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'События'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    credits: {
                        enabled: false
                    },
                    series: []
                };
            }
            /*if (graphType === 'bar') {

             // basic bar
             return {
             options: {
             chart: {
             type: 'bar',
             events: {
             load: reflow,
             addSeries: reflow
             }
             },
             tooltip: {
             valueSuffix: ' шт'
             },
             plotOptions: {
             bar: {
             dataLabels: {
             enabled: true
             }
             }
             },
             navigation: {
             buttonOptions: {
             enabled: false
             }
             }
             },
             title: {
             text: ''
             },
             xAxis: {
             categories: ['Rubicon', 'sshd', 'OSSEC', 'telnet', 'sudo']
             },
             yAxis: {
             min: 0,
             title: {
             text: 'События'
             },
             labels: {
             overflow: 'justify'
             }
             },
             legend: {
             layout: 'horizontal',
             align: 'center',
             verticalAlign: 'bottom',
             borderWidth: 0,
             backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
             shadow: false
             },
             credits: {
             enabled: false
             },
             series: [{
             name: '192.168.1.1',
             data: [107, 31, 635, 203, 2]
             }, {
             name: '192.168.1.100',
             data: [133, 156, 947, 408, 6]
             }, {
             name: '192.168.1.244',
             data: [1052, 954, 4250, 740, 38]
             }]
             };
             } //dummy chart*/
            if (graphType === 'pie3d') {
                return {
                    options: {
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 0
                            },
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        tooltip: {
                            shared: false,
                            headerFormat: '<span style="font-size:10px">' + getFacetLabel(settings.facet[0].id) + ' {point.key}</span><br/>',
                            pointFormat: '<span style="color:{series.color}"> Количество: <b>{point.y:,.0f}</b><br/>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                depth: 35,
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_to = Math.floor(Date.now() / 1000);
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_to = Math.floor(this.options.timestamp / 1000);
                                                ts_from = Math.floor(this.options.prev_ts / 1000);
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: ts_to,
                                                    ef_value: this.name
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        type: 'pie',
                        name: 'Количество',
                        data: []
                    }]
                };
            }
            /*if (graphType === 'stackedcolumn') {

             // stacked column
             return {
             options: {
             chart: {
             type: 'column',
             events: {
             load: reflow,
             addSeries: reflow
             }
             },
             tooltip: {
             headerFormat: '<b>{point.x}</b><br/>',
             pointFormat: '{series.name}<br/> Количество :{point.y}<br/>Общее: {point.stackTotal}'
             },
             plotOptions: {
             column: {
             stacking: 'normal',
             dataLabels: {
             enabled: true,
             color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
             style: {
             textShadow: '0 0 3px black'
             }
             }
             }
             },
             navigation: {
             buttonOptions: {
             enabled: false
             }
             }
             },
             title: {
             text: ''
             },
             xAxis: {
             categories: ['Rubicon', 'SShd', 'OSSEC', 'Sudo', 'Tail']
             },
             yAxis: {
             min: 0,
             title: {
             text: 'События'
             },
             stackLabels: {
             enabled: true,
             style: {
             fontWeight: 'bold',
             color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
             }
             }
             },
             legend: {
             align: 'center',
             verticalAlign: 'bottom',
             backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
             borderWidth: 0,
             shadow: false
             },
             credits: {
             enabled: false
             },
             series: [{
             name: '192.155.19.22',
             data: [5, 3, 4, 7, 2]
             }, {
             name: '192.155.199.11',
             data: [2, 2, 3, 2, 1]
             }, {
             name: '244.15.19.100',
             data: [3, 4, 4, 2, 5]
             }, {
             name: '244.150.1.100',
             data: [4, 4, 6, 2, 1]
             }]
             };
             }*/ //dummy chart
            if (graphType === 'column3d') {
                return {
                    options: {
                        chart: {
                            type: 'column',
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 5,
                                depth: 50,
                                viewDistance: 40
                            },
                            events: {
                                load: reflow,
                                addSeries: reflow
                            }
                        },
                        plotOptions: {
                            column: {
                                depth: 25
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (e) {
                                            var ts_to = Math.floor(Date.now() / 1000);
                                            var ts_from = 0;
                                            if (settings.show_delta) {
                                                ts_to = Math.floor(this.series.chart.options.ts_to / 1000);
                                                ts_from = Math.floor(this.series.chart.options.ts_from / 1000);
                                            }
                                            var url = $state.href('search_widget',
                                                {
                                                    id: settings.uuid,
                                                    from: ts_from,
                                                    to: ts_to,
                                                    ef_value: this.series.name
                                                }
                                            );
                                            window.open(url, '_blank');
                                        }
                                    }
                                },
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px"> Время:{point.key}</span><br/>' +
                            '{series.name}<table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">Количество: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            useHTML: true
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: [],
                        labels: {
                            enabled: hasXaxisLables(settings),
                            rotation: -45
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'События'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: []
                };
            }
        }

        // $http data request
        /**
         * получает данные  с сервера и применяет их на чартах
         */

        /*function pidTransform(plugs, dat) {
            if (dat.length) {
                dat.forEach(function (data) {
                    plugs.forEach(function (plugin) {
                        if (data.name === plugin.id) {
                            data.name = plugin.name;
                        }
                    });
                });
            }
            if (dat.categories && dat.categories.length !== 0) {
                for (var i = 0; i < dat.categories.length; i++) {
                    plugs.forEach(function (plugin) {
                        if (dat.categories[i] === plugin.id) {
                            dat.categories[i] = plugin.name;
                        }
                    });
                }
            }
        }*/

        function getPusher(graphType) {
            function pushWithSlice(length, duration, period) {
                if (length < (duration - period) / period) {
                    return false;
                }
                return true;
            }

            if (graphType === 'line' || graphType === 'area') {
                return function (chart, data, duration, period) {
                    var slice = pushWithSlice(chart.series[0].data.length, duration, period);
                    data.forEach(function (item, i, data) {
                        chart.series[0].addPoint(item, true, slice);
                    });
                    return data[data.length - 1].x;
                };
            }
            if (graphType === 'pie' || graphType === 'pie3d') {
                return function (chart, data) {
                    chart.series[0].setData(data, true, true, true);
                    return 0;
                }
            }
            if (graphType === 'solidgauge' || graphType === 'gauge') {
                return function (chart, data) {
                    chart.series[0].setData(data);
                }
            }
            if (graphType === 'polar') {
                return function (chart, data) {
                    chart.xAxis[0].setCategories(data.categories);
                    chart.series[0].setData(data.data, true, true, true);
                    chart.options.ts_from = data.prev_ts;
                    chart.options.ts_to = data.timestamp;
                    return 0;
                }
            }
            if (graphType === 'column3d') {
                return function (chart, data) {
                    chart.xAxis[0].setCategories(data.categories);
                    chart.series[0].setData(data.data, true, true, true);
                    chart.options.ts_from = data.prev_ts;
                    chart.options.ts_to = data.timestamp;
                    return 0;
                }
            }
            if (graphType === 'facetedLine' || graphType === 'column') {
                return function (chart, data, duration, period) {
                    var slice = pushWithSlice(chart.series[0].data.length, duration, period);
                    // Object.forEach(function (point) {
                    //     chart.series[0].addPoint(point, true, needSlice);
                    // });
                    data.forEach(function (item, i, data) {
                        chart.series[i].addPoint(item.data[0], true, slice);
                    });
                    return data[0].data[data[0].data.length - 1].x;
                };
            }

        }

        function getConfigs(settings, data) {
            /*settings.facet.forEach(function (item) {
                if (item.name === 'plugin_id') {
                    pidTransform(plugins.data, data);
                }
            });*/
            
            var chart = getChartConfig(settings);
            var lastTimestamp = 0;
            var type = settings.graph_type;

            if (type === 'line' || type === 'area') {

                chart.series = [{data: data}];
                lastTimestamp = data[data.length - 1].x;
            }
            if (type === 'pie' || type === 'pie3d') {
                chart.series = [{data: data}];
            }
            if (type === 'solidgauge' || type === 'gauge') {
                chart.series = [data];
            }
            if (type === 'polar') {
                chart.series = [{data: data.data}];
                chart.xAxis.categories = data.categories;
                chart.options.ts_from = data.prev_ts;
                chart.options.ts_to = data.timestamp;
            }
            if (type === 'column3d') {
                chart.series = data.series;
                chart.xAxis.categories = data.categories;
                chart.options.ts_from = data.prev_ts;
                chart.options.ts_to = data.timestamp;

            }
            if (type === 'facetedLine' || type === 'column') {
                chart.series = data;
                try {
                    lastTimestamp = data[0].data[data[0].data.length - 1].x;
                } catch (e) {
                    lastTimestamp = 0;
                }
            }
            return {
                chart: chart,
                l_ts: lastTimestamp
            };
        }

        return {
            getChartBaseConfig: getChartConfig,
            getChartDataConfig: getConfigs,
            getPusher: getPusher
        };

    }
})();
