(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .service('FieldsTransform', fieldsTransform)

    fieldsTransform.inject = ['$q', 'eventFields'];

    function fieldsTransform($q, eventFields) {

        var qbFields = undefined;

        function transformFields() {
            var qbFilters = $q.defer();
            if (qbFields) {
                qbFilters.resolve(qbFields);
            }
            eventFields.getAll().then(function (data) {
                data.forEach(function (item) {
                    item.id = item.name;
                    if (item.type === 'integer') {
                        item.input = 'text';
                    }
                });
                qbFilters.resolve(data);
            });

            return qbFilters.promise;
        }

        return {
            transform: transformFields
        };
    }

})();
