(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .controller('eventDetail', eventDetailCtrl);

    eventDetailCtrl.$inject = ['$scope', 'event_data'];

    function eventDetailCtrl($scope, event_data) {
        function wmiMessageTransformer(data) {
            if (data.data.base.children.plugin_id.value === 'COLLECTOR_WMI') {
                data.data.base.children.data.value = data.data.base.children.data.value.replace(/\\r/g, "");
                data.data.base.children.data.value = data.data.base.children.data.value.replace(/\\n/g, "\n");
                data.data.other.forEach(function (item) {
                    if (item.children) {
                        item.children.wmi_Message.value = item.children.wmi_Message.value.replace(/\\r/g, "");
                        item.children.wmi_Message.value = item.children.wmi_Message.value.replace(/\\n/g, "\n");
                    }
                });
            }
        }

        wmiMessageTransformer(event_data);

        $scope.base = event_data.data.base;
        $scope.net = event_data.data.net;
        $scope.other = event_data.data.other;

        $scope.base_bottom = angular.copy($scope.base)
        $scope.base_bottom.label = "Дополнительная информация";
        $scope.base_bottom.name = "base_additional_data";

        var to_del = ['event_id', 'event_type', 'timestamp',
            'plugin_id', 'plugin_sid', 'timestamp_event'];
        for (var i = 0; i < to_del.length; ++i) {
            delete $scope.base_bottom.children[to_del[i]];
        }
    }
})();
