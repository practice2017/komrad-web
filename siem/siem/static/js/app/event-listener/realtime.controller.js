(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .controller('realtimeController', realtimeCtrl)
        .controller('realtimeConfigSetter', realtimeConfigCtrl);

    realtimeCtrl.$inject = ['$rootScope', '$scope', 'Notification', '$uibModal', 'socketio', 'ngPanels', 'eventViewer'
        , 'fields'];


    function realtimeCtrl($rootScope, $scope, Notification, $uibModal, socketio, ngPanels,   eventViewer ,fields) {

        var hist_size = 60; // histogramm size
        $scope.max_list_size = 40; // table row count
        var events_list_size = $scope.max_list_size;
        $scope.paused = false; // pause unpause update events
        var column_color = '#33aa33'; //column color for histogramm
        var ioEvent = 'RealTime:Data';
        var sioStop = undefined;
        var timePoints = {
            id: [],
            time: []
        };
        $scope.main_button = "Стоп";
        $scope.received = 0;
        $scope.events_list = [];
        $scope.event_id = true;
        $scope.data = true;
        $scope.tableHeaders = [];

        $scope.panels = ngPanels.newGroup('demo', {
            one: {},
            two: {
                masks: ['one']
            }
        });

        $scope.openPanelTwo = function () {
            $scope.panels.open('two');
        };

        $scope.testLeftFields = fields;
        if (!$rootScope.testLeftFields) {
            $rootScope.testLeftFields = $scope.testLeftFields;
        }
        
        $scope.testLeftFields.forEach(function (item) {
            if (item.children) {
                item.children.forEach(function (subItem) {
                    if (subItem.name === 'event_id' ||
                        subItem.name === 'data') {
                        subItem.selected = true;
                    }
                    $scope.tableHeaders.push({
                        id: subItem.name,
                        label: subItem.label,
                        display: false
                    });
                });
            }
        });
        
        $scope.changeTableSize = function (new_size) {
            if (new_size < 0) {
                return;
            }
            events_list_size = new_size;
        };


        var array = [1];

        // add pair of data [iteration] and [timestamp] on timePoints variable
        function getTimePoint(timestamp) {
            if (timestamp === -1) {
                return '';
            }
            var date = new Date(timestamp * 1000);

            return date.toLocaleTimeString('ru-RU', {
                hour12: false,
                hour: "numeric",
                minute: "numeric",
                second: "numeric"
            });
        }


        $scope.fieldViewSwitcher = function (name) {
            $scope[name] = !$scope[name];
        }

        function removeListeners() {
            socketio.emit('leave_realtime', {});
            socketio.removeListener(ioEvent);
            if (sioStop)
                sioStop();
        }

        function forwardListeners() {
            socketio.forward(ioEvent);
            socketio.emit('join_realtime', {});
            sioStop = $scope.$on(ioEvent, function (ev, data) {
                wmiMessageTransformer(data.events)
                update(data);
            });

        }

        function wmiMessageTransformer(data) {
            data.forEach(function (item) {
                if (item.plugin_id === 'COLLECTOR_WMI') {
                    item.data = item.data.replace(/\\r/g, "");
                    item.data = item.data.replace(/\\n/g, "");
                    item.wmi_Message = item.wmi_Message.replace(/\\r/g, "");
                    item.wmi_Message = item.wmi_Message.replace(/\\n/g, "\n");
                }
            });
        }

        function update(data) {
            var len = parseInt(data.count)
            if (!$scope.paused || len == 0) {

                var curr_size = $scope.events_list.length;

                if (len === events_list_size) {
                    $scope.events_list = data.events;
                }

                if (len > events_list_size) {
                    $scope.events_list = data.events.slice(0, events_list_size);
                }

                if (len < events_list_size) {
                    var slice_right_board = events_list_size - len;
                    var new_list = [];
                    if (slice_right_board > 0) {
                        new_list = $scope.events_list.slice(0, events_list_size - len);
                    }
                    $scope.events_list = data.events.concat(new_list)
                }

            }
            $scope.addPoints(len, data.timestamp);
        };

        function clearPlots() {
            var data = [],
                start = -hist_size;
            for (-start; start <= 0; start += 1) {
                $scope.addPoints([
                    -1,
                    0
                ]);
            }
        };

        $scope.addPoints = function (count, timestamp) {
            var chconfig = $scope.chartConfig.getHighcharts();
            chconfig.series[0].addPoint(
                {
                    name: getTimePoint(timestamp),
                    y: count,
                    color: column_color
                }, true, true)
        };

        function reflow() {
            var c = this;
            setTimeout(function () {
                c.reflow();
            }, 0);
        }

        $scope.chartConfig = {

            options: {
                exporting: {enabled: false},
                chart: {
                    type: 'column',
                    animation: false, // don't animate in old IE
                    marginRight: 10,
                    height: 200,
                    events: {
                        load: reflow,
                        addSeries: reflow,
                        redraw: reflow
                    }
                },
                tooltip: {
                    shared: true,
                },
            },
            plotOptions: {
                series: {
                    animation: false,
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                }
            },
            series: [{
                name: 'Количество событий',
                grouping: false,
                //data: []
                data: (function () {
                    // generate an array of random data
                    var data = [];
                    var start = -hist_size;
                    for (-start; start < 0; start += 1) {
                        data.push([
                            '',
                            0
                        ]);
                    }
                    return data;
                }()),
                showInLegend: false,
            }],
            title: {
                text: '',
                style: {
                    display: 'none'
                }
            },
            subtitle: {
                text: '',
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                tickPixelInterval: 150,
                minTickInterval: 5,
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Количество'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                }]
            }

        }


        $scope.pauseUnpause = function () {
            if (!$scope.paused) {
                column_color = '#89E489';
                $scope.main_button = "Старт";
                $scope.paused = true;
                return;
            }
            $scope.paused = false;

            $scope.main_button = "Стоп";
            column_color = '#33aa33';
        };


        $scope.set_color = function (index) {
            if (index < $scope.received) {
                return 'notification notification-alert';
            }
        }

        $scope.set_id = function (index) {
            if (index < $scope.received) {
                return 'colorCycleSecond';
            }
        }

        $scope.hook = function () {
        };


        $scope.openSettings = function () {

            var modalInstance = $uibModal.open({
                scope: $scope,
                animation: true,
                templateUrl: 'realtimeSettings.html',
                controller: 'realtimeConfigSetter',
                size: 'md',
                windowClass: "app-modal-window",
            });

            modalInstance.result.then(function (max_list_size) {
                $scope.max_list_size = max_list_size;
                $scope.changeTableSize(max_list_size);
            }, function () {
            });
        };

        $scope.openEvent = function (id) {
            eventViewer.open(id);
        };

        $scope.$on('$destroy', function () {
            removeListeners();
        });

        $scope.$on('$viewContentLoaded', function () {
            forwardListeners();
        });
    }

    function realtimeConfigCtrl($scope, $uibModalInstance) {
        // adding new widget
        $scope.ok = function (new_table_size) {
            $uibModalInstance.close(new_table_size);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }

})();


