(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('rubiconDataFrame', rubiconDataDirective);

    function rubiconDataDirective() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'rubiconDataFrame.html',
            scope: { data: '=' },
            controller: rubiconDataDirCtrl
        };
    }

    rubiconDataDirCtrl.$inject = ['$scope', '$sce'];

    function rubiconDataDirCtrl($scope, $sce) {

        var node_template = '<div class="ivh-treeview-node-content" title="{[{trvw.label(node)}]}">' +
                '<span ivh-treeview-toggle>' +
                  '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>' +
                '</span>' +
                  '<span style=\'font-family: Menlo,Monaco,Consolas,"Courier New",monospace;\' ' +
                        'ng-bind-html="trvw.label(node)"></span>' +
                '<div ivh-treeview-children></div>' +
              '</div>';

        $scope.tree_options = {
            twistieCollapsedTpl: '<span class="fa fa-plus-square-o"></span>',
            twistieExpandedTpl: '<span class="fa fa-minus-square-o"></span>',
            twistieLeafTpl: ' ',
            nodeTpl: node_template,
            useCheckboxes:false,
        }

        function makeTree(source) {
            var result = [];
            for (var i = 0; i < source.length; ++i) {
                var label_value = source[i]['@value'] || '';
                var label_name = source[i]['@showname'] || source[i]['@show'] || '';
                if (label_value)
                    label_value = ': <b>' + label_value + '</b>';
                var to_append = {
                    label: $sce.trustAsHtml(label_name + label_value),
                };
                if (source[i].field)
                    to_append.children = makeTree(source[i].field);

                result.push(to_append)
            }
            return result;
        }

        $scope.tree = makeTree($scope.data.tree);
    }



})();
