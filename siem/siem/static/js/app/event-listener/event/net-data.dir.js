(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('netEventData', netDataDirective);

    function netDataDirective() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'netEventData.html',
            scope: { data: '=' },
            controller: function ($scope) {
            }
        };
    }

})();
