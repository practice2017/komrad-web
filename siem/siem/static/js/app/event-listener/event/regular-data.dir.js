(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .directive('regularDataFrame', regularDataDirective);

    function regularDataDirective() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'regularDataFrame.html',
            scope: { data: '=' },
            controller: function ($scope) {}
        };
    }




})();
