(function () {
    'use strict';

    angular
        .module('KomradeApp.event_listener')
        .factory('SearchContent', searchContentFactory)
        .factory('QueriesContent', getQueriestFactory)
        .factory('SortedContent', getSortedFactory);

    searchContentFactory.$inject = ['$http', 'Notification', 'moment'];
    getQueriestFactory.$inject = ['$http', 'Notification', '$q'];
    getSortedFactory.$inject = ['$http', 'Notification', '$q'];

    function searchContentFactory($http, Notification, moment) {
        var SearchContent = function (qb) {
            this.query_body = qb;
            this.items = [];
            this.busy = false;
            this.page = 1;
            this.message = '';
            this.has_error = false;
            this.total = undefined;
            this.name = 'event_id';
            this.reverse = true;
        };


        function wmiMessageTransformer(data) {
            data.forEach(function (item) {
                if (item.plugin_id === 'COLLECTOR_WMI') {
                    item.data = item.data.replace(/\\r/g, "");
                    item.data = item.data.replace(/\\n/g, "");
                    item.wmi_Message = item.wmi_Message.replace(/\\r/g, "");
                    item.wmi_Message = item.wmi_Message.replace(/\\n/g, "\n");
                }
            });
        }

        SearchContent.prototype.nextPage = function (from, to, name, reverse) {
            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            ;
            if (this.busy) return;
            if (this.items.length >= this.total) return;
            this.busy = true;

            var reqBody = {
                from: from,
                to: to,
                query_body: this.query_body,
                sort_by: this.name,
                sort_reverse: this.reverse
            }
            if (typeof name !== 'undefined') {
                this.items.length = 0;
                this.message = '';
                this.page = 1;
                this.total = undefined;
                this.name = name;
                this.reverse = reverse;
                reqBody.sort_by = this.name;
                reqBody.page = this.page;
                reqBody.sort_reverse = this.reverse;
            }
            if (reverse) {
                this.reverse = reverse
                reqBody.sort_reverse = this.reverse;
            } else {
                reqBody.sort_reverse = this.reverse;
            }
            reqBody.page = this.page;

            $http.post('/fullrequest', angular.toJson(reqBody)).success(function (data) {
                wmiMessageTransformer(data.results);
                if (typeof data.total === 'undefined') {
                    this.message = 'События по требуемому запросу не найдены';
                    this.busy = false;
                    return;
                }
                if (typeof this.total === 'undefined') {
                    this.total = data.total;
                    this.message = 'Всего найдено: ' + this.total;
                }
                ;
                this.items = this.items.concat(data.results);
                this.page = this.page + 1;
                this.has_error = false;
                this.busy = false;
            }.bind(this))
                .error(function (data) {
                    this.message = 'При выполнении запроса произошла ошибка';
                    Notification.error(this.message);
                    this.has_error = true;
                    this.busy = false;
                }.bind(this))
        };
        return SearchContent;
    }


    function getQueriestFactory($http, Notification, $q) {
        var searchContent = function (qb) {
            this.items = [];
            this.total = 0;
        };

        //get queries for input-dropdown events base
        searchContent.prototype.loadData = function () {
            var query = $q.defer();

            $http.get('/querylist/all').success(function (data) {
                var modifiedArray = [];
                data.response.forEach(function (item) {
                    modifiedArray.push({
                        readableName: item.name,
                        id: item.id,
                        created: item.created,
                        body: item.body,
                        description: item.description,
                        role_access: item.role_access
                    });
                });
                query.resolve({
                    queries: modifiedArray,
                    total: this.items.length
                })
                this.items = modifiedArray;
                this.total = this.items.length;
            }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                }.bind(this))

            return query.promise;
        };

        //get queries for input-dropdown fact base
        searchContent.prototype.getFactBaseLists = function () {
            var query = $q.defer();

            $http.get('/bof/all').success(function (data) {
                var modifiedArray = [];
                data.forEach(function (item) {
                    modifiedArray.push({
                        readableName: item.name,
                        body: item.id,
                        type: item.type
                    });
                });
                query.resolve({
                    queries: modifiedArray,
                    total: this.items.length
                })
                this.items = modifiedArray;
                this.total = this.items.length;
            }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                }.bind(this))

            return query.promise;
        };

        return searchContent;
    }


    function getSortedFactory($http, Notification, $q) {
        var sortedContent = function (qb, sort_data) {
            this.query_body = qb;
            this.items = [];
            this.plot_duration = 10000;
            this.period = 500;
            this.data_type = sort_data;
        };


        sortedContent.prototype.getSorted = function () {
            if (this.message === '') {
                this.message = 'Загрузка...';
            }
            this.busy = true;

            var sortRequest = {
                    id: "uniq_ID",
                    query: {
                        query: this.query_body,
                        type: 'event_id',
                        results: '25'
                    },
                    update_period: this.period,
                    plot_duration: this.plot_duration
                },
                deferred = $q.defer(),
                modifyData = [];

            if (this.data_type) {
                sortRequest.query.type = this.data_type
            }
            $http.post('/preventive', angular.toJson(sortRequest)).success(function (data) {
                //if answer has no data
                var dataMarker = 0;
                data.forEach(function (item) {
                    if (item[1].total !== 0) {
                        dataMarker = item[1].total;
                    }
                })

                if (data.length === 0 || dataMarker === 0) {
                    this.message = "Нет необходимых данных на сервере";
                    this.has_error = true;
                    this.busy = false;
                    Notification.error(this.message);
                    return;
                }

                var seriesArr = [],
                    newData = [],
                    categories = [],
                    arr = [];

                data.forEach(function (item) {
                    arr.push(item[1].facet);
                    seriesArr.push({
                        timestamp: parseInt(item[0] * 1000)
                    });
                });

                arr.forEach(function (item, index) {
                    var rawData = [],
                        cat = [];
                    for (var key in item) {
                        rawData.push(item[key]);
                        cat.push(key);
                    }
                    categories = cat;
                    newData.push(rawData);
                });

                var formated = [];

                function rebuildArray() {
                    var newArray = [];
                    for (var i = 0; i < newData.length; i++) {
                        newArray.push({
                            x: +seriesArr[i].timestamp,
                            y: +newData[i][0]
                        });
                        newData[i].shift();
                    }

                    formated.push({
                        data: newArray
                    });
                    if (newData[newData.length - 1].length !== 0) {
                        rebuildArray();
                    }
                }

                rebuildArray();

                formated.forEach(function (item, index) {
                    item.name = categories[index];
                    item.total = item.data[item.data.length - 1].y;
                    item.last_timestamp = item.data[item.data.length - 1].x;
                });

                function reflow() {
                    var c = this;
                    setTimeout(function () {
                        c.reflow();
                    }, 0);
                }

                formated.forEach(function (item, i) {
                    var delta = [];
                    var chart = {};

                    for (var j = 0; j < item.data.length; j++) {
                        var nextIndex = j + 1;
                        if (j < (item.data.length - 1)) {
                            delta.push({
                                x: item.data[j].x,
                                y: item.data[nextIndex].y - item.data[j].y
                            });
                        }

                    }

                    item.data = delta;

                    chart.options = {
                        chart: {
                            backgroundColor: null,
                            borderWidth: 0,
                            type: 'column',
                            margin: [2, 0, 2, 0],
                            width: 120,
                            height: 20,
                            style: {
                                overflow: 'visible'
                            },
                            skipClone: true,
                            load: reflow,
                            addSeries: reflow
                        },
                        tooltip: {
                            positioner: function () {
                                return {x: 150, y: -20};
                            },
                            backgroundColor: 'rgba(255,255,255,0.8)'
                        },
                        navigation: {
                            buttonOptions: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        }
                    };
                    chart.title = {
                        text: ''
                    };
                    chart.xAxis = {
                        type: 'datetime',
                        endOnTick: false,
                        startOnTick: false,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        tickPositions: [0]
                    };
                    chart.yAxis = {
                        min: 0,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: []
                    };
                    chart.plotOptions = {
                        series: {
                            animation: false,
                            lineWidth: 1,
                            shadow: false,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            marker: {
                                radius: 1,
                                states: {
                                    hover: {
                                        radius: 2
                                    }
                                }
                            },
                            fillOpacity: 0.25
                        },
                        column: {
                            negativeColor: '#910000',
                            borderColor: 'silver'
                        }
                    };
                    chart.credits = {
                        enabled: false
                    };
                    chart.series = [{
                        name: item.name,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                }
                            }
                        },
                        pointPadding: 0.96,
                        groupPadding: 0.98,
                        color: '#63A8EB',
                        data: item.data
                    }];
                    modifyData.push({
                        message_type: item.name,
                        count: item.total,
                        last_message_data: item.last_timestamp,
                        chart: chart
                    });
                });

                deferred.resolve(modifyData);
                this.has_error = false;
                this.busy = false;
            }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                    this.busy = false;
                    this.has_error = true;
                }.bind(this))

            return deferred.promise;
        };

        return sortedContent;
    }

})();
