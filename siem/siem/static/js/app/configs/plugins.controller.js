(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('PluginsController', PluginsCtrl);

    PluginsCtrl.$inject = ['$scope', '$timeout', 'pluginsData_preloaded'];

    function PluginsCtrl($scope, $timeout, pluginsData_preloaded) {
        // т.к. прелоад данных загружает все данные которые есть в таблице, то просто передаем их в
        // smart table
        $scope.plugs = pluginsData_preloaded;
        $scope.filterString = undefined;


        var checked = false;
        $scope.toggleAll = function () {
            checked = !checked;
            if (checked) {
                $scope.$broadcast('table.selectAllRows');
            } else {
                $scope.$broadcast('table.deselectAllRows');
            }
        };

        $scope.filterPlugins = function () {
            $scope.$broadcast('table.setFilter', $scope.filterString)
        };
        
        $scope.setState = function(state){
            $scope.$broadcast('config.table.getSelectedRows', state)
        };

        $scope.$on('config.table.resolveSelectedRows', function (e, data) {
            var ids = [];
            data.selectedRows.forEach(function (item) {
                ids.push(item.id);
            });

            $scope.plugs.service.setStateByIds(ids, data.newStatus).then(function (response) {
                if(response === 'ok') {
                    $scope.$broadcast('table.update', data)
                }
            });
        });

        var search = angular.element(document.querySelector('.search-input'));

        search.keypress(function (e) {
            if(e.keyCode === 13 && e.key === 'Enter'){
                $scope.filterPlugins();
            }
        });

        $scope.$on('$viewContentLoaded', function () {
            var defaultSort = [
                {colId: 'active', sort: 'desc'}
            ]
            $timeout(function () {
                $scope.$broadcast('config.table.setDefaultSort', defaultSort)
            },0);

        });
    }

})();