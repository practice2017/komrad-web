(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .filter('accessName', getAccessName)
        .filter('accessMapFilter', accessMapFilter);

    getAccessName.inject = ['accessLvls', '$filter'];
    accessMapFilter.inject = ['$filter'];

    function getAccessName(accessLvls, $filter){
        return function(value) {
            var selected = $filter('filter')(accessLvls, {id: value});
            if (selected)
                return selected[0].name;
            return '';
        }
    }

    function accessMapFilter($filter){
        return function(array, expression){
            //an example
            if (expression.name) {
                var filtered = $filter('filter')(array, expression);
                return filtered;
            }
            var filtered = $filter('filter')(
                array,
                {access_map: expression},
                function(actual, expected) {
                    if (+expected === 3)
                        return +actual >= 1;
                    return +actual == +expected;
                }
            );
           return filtered
        }
    }

})();
