(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('RolesController', roleModalCtrl);

    roleModalCtrl.$inject = ['$scope', '$uibModalInstance', '$filter', 'accessLvls',
        'appModulesHierarchy', 'statement', 'controlService'];


    function roleModalCtrl($scope, $uibModalInstance, $filter, accessLvls,
                           appModulesHierarchy, statement, controlService) {
        var old_access_map = undefined;
        $scope.statement = statement;
        $scope.access_levels = accessLvls;

        // default values
        $scope.roleName = '';
        $scope.error = {
            rolename: false
        };
        //license remover
        appModulesHierarchy.forEach(function (item) {
            if (item.id === 'CONFIGS') {
                item.submodules.forEach(function (subItem) {
                    if (subItem.id === 'PAGE_LICENSE') {
                        item.submodules.splice(subItem, 1)
                    }
                });
            }
        });

        $scope.ahierarchy = appModulesHierarchy;
        $scope.isUpdate = function () {
            if (statement)
                return true;
            return false;
        }();
        $scope.isRole = function () {
            if (statement && statement.username)
                return false;
            return true;
        }();

        // set up access map from modules hierarchy
        // {MODULE_id: default value}
        $scope.access_map = function () {
            // setup default map
            var access_map = {};
            // available levels for access modules
            var access_map_avls = {};
            for (var mod_id = 0; mod_id < appModulesHierarchy.length; ++mod_id) {
                for (var submod_id = 0; submod_id < appModulesHierarchy[mod_id].submodules.length; ++submod_id) {
                    var submod = appModulesHierarchy[mod_id].submodules[submod_id];
                    access_map[submod.id] = 0;
                    access_map_avls[submod.id] = submod.lvls;
                }
            }

            // setup access_map from statement
            if (statement) {
                if ($scope.isRole)
                    $scope.roleName = statement.name;
                //for i in statement.access_map
                for (var k in access_map) {
                    // put statement rules on default access map
                    if (statement.access_map[k]) {
                        // check max access_map lvl
                        if (statement.access_map[k] > access_map_avls[k])
                            access_map[k] = access_map_avls[k];
                        else
                            access_map[k] = statement.access_map[k];
                    }
                }
            }
            old_access_map = angular.copy(access_map);
            return access_map;
        }();


        $scope.get_submodule_lvls = function (lvl) {
            if (lvl === 2) {
                return accessLvls;
            }
            return accessLvls.slice(0, 2);
        }

        $scope.ok = function () {
            if (!$scope.roleName && $scope.isRole) {
                $scope.error.rolename = true;
                return;
            }

            if (statement) {
                if ($scope.isRole)
                    statement.name = $scope.roleName;
                statement.access_map = $scope.access_map;
                controlService.update(statement)
                    .success(function (data) {
                        $uibModalInstance.close();
                    });
                return;
            }

            controlService.add({name: $scope.roleName, access_map: $scope.access_map})
                .success(function (data) {
                    $uibModalInstance.close();
                });


            //$uibModalInstance.close();

        };


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.selected = function (value) {
        };
    }

})();