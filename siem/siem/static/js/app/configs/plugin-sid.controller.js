(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('PluginSidController', PluginSidCtrl);

    PluginSidCtrl.$inject = ['$scope', 'sid_data', 'plugin_id', 'Pagination'];

    function PluginSidCtrl($scope, sid_data, plugin_id, Pagination) {
        // т.к. прелоад данных загружает все данные которые есть в таблице, то просто передаем их в
        // smart table
        $scope.plugin_id = plugin_id;
        $scope.loadedData = sid_data;
        $scope.displayedData = [];
        $scope.loadedData.data.response.forEach(function (item) {
            $scope.displayedData.push(item);
        });

        //pagination

        $scope.pager = {};
        $scope.setPage = setPage;

        initController();
        
        function initController() {
            // initialize to page 1
            $scope.setPage(1);
        }

        function setPage(page) {
            // get pager object from service
            $scope.pager = Pagination.GetPager($scope.displayedData.length, page, 10);
            if (page < 1 || page > $scope.pager.totalPages) {
                return;
            }

            // get current page of items
            $scope.items = $scope.displayedData.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
        }
    }

})();