(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .directive('pluginsTable', pluginsTable);

    // widgetConfig directive duration and update period
    function pluginsTable() {
        var template = '<table-ui config="self.tableUiConfig" service-name="self.serviceName"></table-ui>';
        return {

            // шаблон задан как ссылка или выражение

            template: template,

            // применение директивы ограничивается
            restrict: 'E',
            scope: {
                preloadedData: '=',
                serviceName: '='
            },
            // контроллер для директивы

            controller: plaginsTableCtrl,
            controllerAs: 'self',
            bindToController: true, // because the scope is isolated
            // прописана основная функциональность директивы

            link: function postLink(scope, element, attrs) {

            }

        };

    }

    plaginsTableCtrl.$inject = ['$scope', '$element', '$attrs', '$q', '$state', 'pluginStatuses'];

    function plaginsTableCtrl($scope, $element, $attrs, $q, $state, pluginStatuses) {
        var self = this;

        //ag-grid test -----------------------------------------------------------------------------
        self.tableLoading = true;

        var columnDefs = [
            {
                headerName: "",
                field: "select",
                width: 10,
                checkboxSelection: true,
                suppressSorting: true,
                suppressMenu: true,
                suppressMovable: true
            },
            {headerName: "ID плагина", field: "id", width: 30, suppressMenu: true},
            {
                headerName: "Статус",
                field: "active",
                width: 30,
                cellRenderer: statusIconRenderer,
                suppressMenu: true
            },
            {
                headerName: "Имя плагина",
                field: "name",
                width: 80,
                suppressMenu: true
            },
            {headerName: "Описание плагина", field: "description", width: 200, suppressMenu: true}
        ];

        var localeText = {
            page: 'Страница',
            to: 'до',
            of: 'из',
            last: 'Последняя',
            first: 'Первая',
            next: '<i class="fa fa-chevron-right"></i>',
            previous: '<i class="fa fa-chevron-left"></i>'
        }

        self.gridOptions = {
            columnDefs: columnDefs,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            suppressRowClickSelection: true,
            enableColResize: true,
            //virtualPaging: true,
            rowData: null,
            enableServerSideSorting: true,
            enableServerSideFilter: true,
            scrollbarWidth: 20,
            rowHeight: 37,
            rowSelection: "multiple",
            rowModelType: 'pagination',
            paginationPageSize: 20,
            localeText: localeText,
            icons: {
                sortAscending: '<i class="fa fa-sort-amount-asc"/>',
                sortDescending: '<i class="fa fa-sort-amount-desc"/>',
                checkboxChecked: '<i class="fa fa-check-square" style="color:#63A8EB; font-size: 20px"></i>',
                checkboxUnchecked: '<i class="fa fa-square-o" style="color:#E7EBEC; font-size: 22px"></i>'
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center" ' +
            'style="border: 1px solid #ddd"><img src="static/img/335.gif" ' +
            'style="width: 16px; height: 16px">  Загрузка...</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center" ' +
            'style="border: 1px solid #ddd">По данному запросу ничего не найдено</span>',
            onCellClicked: cellClick
        };

        var defered = $q.defer();
        defered.resolve(self.preloadedData);

        self.tableUiConfig = {
            gridOptions: self.gridOptions,
            preloadData: defered.promise,
            pagination: true,
            paginationSize: 5,
            quantityOfRecords: true,
            quantityOfRecordsOptions: ['20', '50', '100']
        }


        //status icon formatter
        function statusIconRenderer(params) {
            var status = '<span class="' + pluginStatuses[params.value].props + '" title="' +
                pluginStatuses[params.value].name + '">' +
                pluginStatuses[params.value].name + '</span>';
            return status;
        }

        function cellClick(params) {
            var rows = []
            if (params.colDef.field === 'active') {
                self.gridOptions.api.showLoadingOverlay();
                params.data.active = +(!params.data.active)
                rows.push(params.data);
                var resultData = {
                    selectedRows: rows,
                    newStatus: params.data.active
                }
                $scope.$emit('config.table.resolveSelectedRows', resultData);
                return;
            }
            if (params.colDef.field === 'name' ||
                params.colDef.field === 'id') {
                var url = $state.href('plugin_sid', {pid: params.data.id});
                window.open(url, '_blank');
            }
        }

        $scope.$on('config.table.getSelectedRows', function (e, data) {
            self.gridOptions.api.showLoadingOverlay();
            var selectedRows = self.gridOptions.api.getSelectedRows(),
                resultData = {
                    selectedRows: selectedRows,
                    newStatus: data
                }

            $scope.$emit('config.table.resolveSelectedRows', resultData);
        });


        $scope.$on('config.table.setDefaultSort', function (e, data) {
            self.gridOptions.api.setSortModel(data)
        });
        // ag grid test end ------------------------------------------------------------------------

    }

})();