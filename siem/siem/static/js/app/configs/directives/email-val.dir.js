(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .directive('uniqEmail', emailVal);

    emailVal.$inject = ['$filter', 'UsersModel'];

    // widgetConfig directive duration and update period
    function emailVal($filter, UsersModel) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(email) {
                    var userlist = UsersModel().items;
                    var filtered = $filter('filter')(userlist, {email: email}, true);
                    if (filtered.length >= 1) {
                        ctrl.$setValidity('duplicate', false);
                        return undefined;
                    } else {
                        ctrl.$setValidity('duplicate', true);
                        return email;
                    }
                });
            }
        };

    }

})();


