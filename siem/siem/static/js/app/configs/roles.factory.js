/**
 * requester for roles
 * @return {[type]} [description]
 */
(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .factory('RolesModel', roleFactory);

    roleFactory.$inject = ['$http', '$filter', 'Notification'];

    function roleFactory($http, $filter, Notification) {

        var roles = undefined;
        var roleFactory = function () {
            this.items = [];
            this.diplayData = [];
            this.total = 0;
            this.loaded = false;
            this.busy = false;
        };

        function settAssetControleRole(data) {
            data.response.forEach(function (item) {
                if (item.id === 1) {
                    for (var key in item.access_map) {
                        if (key === "PAGE_RESOURCES_CONTROL") {
                            item.access_map[key] = 2;
                        }
                    }
                }
            })
        }

        roleFactory.prototype.get = function () {

            if (this.busy) return;
            this.busy = true;

            $http.get('/config/roles/all').success(function (data) {
                settAssetControleRole(data)
                this.items = data.response;
                this.diplayData = data.response;
                this.total = this.items.length;
                this.loaded = true;
                this.busy = false;
            }.bind(this))
                .error(function (data) {
                    Notification.error("Ошибка получения данных с сервера.");
                    this.busy = false;
                }.bind(this))
        };

        roleFactory.prototype.getRoleById = function (id) {
            if (!this.loaded) {
                return;
            }
            var role = $filter('getByField')(this.items, id, 'id');
            return role;
        }


        roleFactory.prototype.add = function (role_data) {
            return $http.post('/config/role', angular.toJson(role_data))
                .success(function (data, status) {
                    this.get();
                }.bind(this)).error(function () {
                    Notification.error('Ошибка записи данных');
                })
        };

        roleFactory.prototype.update = function (role_data) {
            return $http.put('/config/role', angular.toJson(role_data))
                .success(function (data, status) {
                    this.get();
                }.bind(this))
                .error(function () {
                    Notification.error('Ошибка записи данных');
                })
        };

        roleFactory.prototype.remove = function (row) {
            return $http.delete('/config/role/' + row.id)
                .success(function (data, status) {
                    this.get();
                }.bind(this)).error(function () {
                })
        };

        roleFactory.prototype.setAccessMap = function (role_data) {
            return this.update(role_data);
        };

        roleFactory.prototype.export = function (fmt) {
            fmt = fmt || 'csv';
            return $http.get('/config/role/export/' + fmt, {responseType: 'arraybuffer'})
                .success(function (data) {
                    var file = new Blob([data], {type: 'data:attachment/csv; charset=utf-8'});
                    saveAs(file, 'roles.csv');
                });
        };


        function getRoles() {
            if (!roles)
                roles = new roleFactory();
            return roles;
        }

        return getRoles;
    }

})();