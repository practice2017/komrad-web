(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('RotationsController', RotationsCtrl)
        .controller('RotationModalController', RotationModalCrl)

    RotationsCtrl.$inject = ['$scope', 'RotationsContent', '$sce', '$uibModal', '$timeout'];
    RotationModalCrl.$inject = ['$scope', '$uibModalInstance'];

    function RotationsCtrl($scope, RotationsContent, $sce, $uibModal, $timeout) {

        $scope.status = {
            current: 'current',
            readonly: 'readonly',
            archive: 'archive',
            restored: 'restored',
            restoredArchive: 'restored_archive',
            inAction: 'in_action'
        }

        $scope.calculateStatus = {
            empty: '',
            calculating: 'calculating',
            match: 'match',
            doNotMatch: 'do not match'
        }

        $scope.rData = new RotationsContent();
        $scope.someData = [];
        $scope.displayedData = [];
        $scope.rData.loadAll().then(function (data) {
            data.forEach(function (item) {
                $scope.displayedData.push(item);
            })
        });
        $scope.rData.loadConfig();

        $scope.iconDesc = "r_id";
        $scope.reverse = false;

        $scope.canArchieve = true;
        $scope.canRestore = true;
        $scope.canCalcCh = true;
        $scope.canSave = true;
        $scope.canDelete = true;

        $scope.dataSelector = function (disp, stored) {
            disp.forEach(function (item) {
                stored.forEach(function (subItem) {
                    if (item.id === subItem.id) {
                        if (item.ch) {
                            subItem.ch = item.ch;
                        }
                    }
                })
            })
        }

        $scope.toggleAll = function (to_state) {
            $scope.cbAll = to_state;
            for (var i = 0; i < $scope.rData.data.length; i++) {
                $scope.rData.data[i].ch = to_state;
            }
            $scope.setupBlocks();
        }

        $scope.whenToggled = function () {
            $scope.setupBlocks();
        }

        //revers and avers sort
        $scope.setOrderAndReload = function (name) {
            if ($scope.iconDesc === name) {
                $scope.reverse = !$scope.reverse;
            }
            if ($scope.iconDesc !== name) {
                $scope.reverse = false;
            }
            $scope.iconDesc = name;
        }

        $scope.getLabel = function (status) {

            if (status === $scope.status.current) {
                return $sce.trustAsHtml('<span class="label label-default"><i class="fa fa-download"></i> Текущий </span>')
            }
            if (status === $scope.status.readonly) {
                return $sce.trustAsHtml('<span class="label label-success"><i class="fa fa-search-plus "></i> Только для чтения </span>')
            }
            if (status === $scope.status.archive) {
                return $sce.trustAsHtml('<span class="label label-warning"><i class="fa fa-inbox "></i> Архив </span>')
            }
            if (status === $scope.status.restored) {
                return $sce.trustAsHtml('<span class="label label-primary"><i class="fa fa-arrow-circle-o-up "></i> Сохранен для чтения </span>')
            }
            if (status === $scope.status.restoredArchive) {
                return $sce.trustAsHtml('<span class="label label-info"><i class="fa fa-archive "></i> Сохраненный архив </span>')
            }
            if (status === $scope.status.inAction) {
                return $sce.trustAsHtml('<span class="label label-default"><i class="fa fa-clock-o "></i> В обработке </span>')
            }
        }

        function statusChanger(dataArray, newStatusData) {

            dataArray.forEach(function (item) {
                newStatusData.id.forEach(function (subitem) {
                    if (item.id === subitem) {
                        if (item.status === $scope.status.readonly &&
                            newStatusData.status === 'archieve') {
                            item.status = $scope.status.archive
                        }
                        if (item.status === $scope.status.readonly &&
                            newStatusData.status === 'save') {
                            item.status = $scope.status.restored
                        }
                        if (item.status === $scope.status.archive &&
                            newStatusData.status === 'restore') {
                            item.status = $scope.status.restored
                        }
                        if (item.status === $scope.status.archive &&
                            newStatusData.status === 'save') {
                            item.status = $scope.status.restoredArchive
                        }
                        if (item.status === $scope.status.restoredArchive &&
                            newStatusData.status === 'restore') {
                            item.status = $scope.status.restored
                        }
                        if (item.status === $scope.status.restored &&
                            newStatusData.status === 'archieve') {
                            item.status = $scope.status.restoredArchive
                        }
                    }
                })
            })
        }

        $scope.getChecksummLabel = function (status) {

            if (status === $scope.calculateStatus.empty) {
                return $sce.trustAsHtml('')
            }
            if (status === $scope.calculateStatus.calculating) {
                return $sce.trustAsHtml('<i class="fa fa-clock-o" style="color:#616161"></i>')
            }
            if (status === $scope.calculateStatus.match) {
                return $sce.trustAsHtml('<i class="fa fa-check" style="color:#46a546"></i>')
            }
            if (status === $scope.calculateStatus.doNotMatch) {
                return $sce.trustAsHtml('<i class="fa fa-exclamation-circle" style="color:#e61610"></i>')
            }
        }

        $scope.setComment = function (id, new_data) {
            $scope.rData.setComment({id: id, comment: new_data});
        }

        $scope.setupBlocks = function () {
            var choised_states = [];
            for (var i = 0; i < $scope.displayedData.length; i++) {
                if ($scope.displayedData[i].ch)
                    choised_states.push($scope.displayedData[i].status)
            }
            $scope.canArchieve = true;
            $scope.canRestore = true;
            $scope.canCalcCh = true;
            $scope.canSave = true;
            $scope.canDelete = true;

            if (choised_states.indexOf('current') > -1) {
                $scope.canArchieve = false;
                $scope.canRestore = false;
                $scope.canCalcCh = false;
                $scope.canSave = false;
                $scope.canDelete = false;
            }
            if (choised_states.indexOf('readonly') > -1) {
                $scope.canRestore = false;
                $scope.canCalcCh = true;
                $scope.canDelete = false;
            }
            if (choised_states.indexOf('archive') > -1) {
                $scope.canArchieve = false;
                $scope.canCalcCh = false;
            }
            if (choised_states.indexOf('restored') > -1) {
                $scope.canRestore = false;
                $scope.canCalcCh = true;
                $scope.canSave = false;
            }
            if (choised_states.indexOf('restored_archive') > -1) {
                $scope.canArchieve = false;
                $scope.canCalcCh = false;
                $scope.canSave = false;
            }
        }

        $scope.save = function () {
            $scope.dataSelector($scope.displayedData, $scope.rData.data)
            $scope.rData.save().then(function (data) {
                statusChanger($scope.displayedData, data);
                $scope.setupBlocks();
            });
        }

        $scope.restore = function () {
            $scope.dataSelector($scope.displayedData, $scope.rData.data)
            $scope.rData.restore().then(function (data) {
                statusChanger($scope.displayedData, data);
                $scope.setupBlocks();
            });
        }

        $scope.remove = function () {
            $scope.dataSelector($scope.displayedData, $scope.rData.data)
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'removeDialog.html',
                controller: 'RotationModalController',
                size: 'sm',
                windowClass: "app-modal-window",
                scope: $scope
            });

            modalInstance.rendered.then(function () {

            }, function () {
            });

            modalInstance.result.then(function (data) {
                $scope.rData.rm().then(function (result) {
                    $scope.displayedData.forEach(function (item) {
                        result.id.forEach(function (subitem) {
                            if (item.id === subitem) {
                                var start = $scope.displayedData.indexOf(item);
                                $scope.displayedData.splice(start, 1);
                            }
                        })
                    });
                });
            }, function () {
            });
        }

        $scope.refreshRow = function () {
            $scope.rData.loadAll().then(function (data) {
                $scope.displayedData.forEach(function (item) {
                    data.forEach(function (subItem) {
                        if (item.id === subItem.id) {
                            if (subItem.inspect_result === $scope.calculateStatus.match) {
                                item.inspect_result = subItem.inspect_result
                            }
                            if (subItem.inspect_result === $scope.calculateStatus.doNotMatch) {
                                item.inspect_result = subItem.inspect_result
                            }
                            if (subItem.action === 'inspect') {
                                $scope.$broadcast('CS.calculating')
                                item.inspect_result = $scope.calculateStatus.calculating;
                            }
                        }
                    });
                });
            });
        }


        $scope.$on('CS.calculating', function (e) {
            $timeout(function () {
                $scope.refreshRow();
            }, 30000)
        })

        $scope.calculate = function () {
            $scope.dataSelector($scope.displayedData, $scope.rData.data)
            $scope.rData.calc().then(function (data) {
                $scope.displayedData.forEach(function (item) {
                    data.id.forEach(function (subItem) {
                        if (item.id === subItem) {
                            if (item.inspect_result !== $scope.calculateStatus.match &&
                                item.inspect_result !== $scope.calculateStatus.doNotMatch) {
                                item.inspect_result = $scope.calculateStatus.calculating;
                            }
                        }
                    });
                });
                $scope.refreshRow();
            });
        }

        $scope.archieve = function () {
            $scope.dataSelector($scope.displayedData, $scope.rData.data)
            $scope.rData.archieve().then(function (data) {
                statusChanger($scope.displayedData, data);
                $scope.setupBlocks();
            });
        }

        $scope.saveTTLArchieve = function (data) {
            $scope.rData.saveConfig({ttl_archive: data});
        }

        $scope.saveTTLReadOnly = function (data) {
            $scope.rData.saveConfig({ttl_readonly: data});
        }

        $scope.$on('$viewContentLoaded', function () {

        });

    }

    function RotationModalCrl($scope, $uibModalInstance) {

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.remove = function () {
            $uibModalInstance.close();
        }
    }

})();