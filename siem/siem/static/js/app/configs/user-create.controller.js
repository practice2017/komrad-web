(function () {
    'use strict';

    angular
        .module('KomradeApp.configs')
        .controller('createUserCntroller', createUserCtrl);


    createUserCtrl.$inject = ['$scope', '$uibModalInstance', '$filter', 'groupsObj',
        '$q', 'RolesModel', 'user_data', 'control_service'];

    function createUserCtrl($scope, $uibModalInstance, $filter, groupsObj, $q, RolesModel,
                            base_user_data, control_service) {

        $scope.roles = RolesModel().items;
        /************************* dropdown functions *************************/
        $scope.user_data = function(){
            if (!base_user_data){
                return {
                    role: 1,
                    groups: [1]
                };
            }
            return base_user_data;
        }();

        $scope.groups = groupsObj.items;

        $scope.myModel = 1;

        $scope.selectizeConfig = {
            plugins: ['remove_button'],
            valueField: 'id',
            labelField: 'name',
            delimiter: '|',
            placeholder: 'Pick something',
            onInitialize: function(selectize){
            // receives the selectize object as an argument
            },
            // maxItems: 1
        };

        $scope.getGroupName = function(id){
            var gr =groupsObj.getById(id);
            if (!gr){
                return ;
            }
            return gr.name;
        };

        $scope.filterObjectList = function (input_name) {
            var filter = $q.defer();
            var normalisedInput = input_name.toLowerCase();
            var filteredArray = $scope.groups.filter(function (group) {
                var matchName = group.readableName.toLowerCase().indexOf(normalisedInput) === 0;
                return matchName;
            });
            filter.resolve(filteredArray);
            return filter.promise;
        };

        $scope.addGroup = function(group){
            $scope.user_data.groups.push(group.id);
            setTimeout(function() {
                var input = angular.element(document.querySelector(
                    ".input-dropdown input"));
                input.val('').trigger('change');
            }, 10);

        }

        $scope.removeGroup = function(group){
        }

        /**********************************************************************/

        $scope.myImage='';
        $scope.myCroppedImage='';
        var handleFileSelect=function(evt) {
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage = evt.target.result;
                });
            };
            $scope.browsed = true;
            reader.readAsDataURL(file);
        };

        $scope.$on('runScropper', function(event, data) {
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
        });

        function dataURItoBlob(dataURI) {
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {
                type: mimeString
            });
        }

        $scope.ok = function (user_data) {
            if(!$scope.isUpdate){
                if (!this.form.$valid){
                    this.form.username.$dirty = true;
                    this.form.password.$dirty = true;
                    this.form.passwordConfirm.$dirty = true;
                    return;
                }
            }
            else{
                if (!this.form.$valid){
                    this.form.username.$dirty = true;
                    return;
                }
                if (!this.passform.$valid && user_data.updatePass){
                    this.passform.password.$dirty = true;
                    this.passform.passwordConfirm.$dirty = true;
                    return;
                }
            }
            if ($scope.myCroppedImage){
                  var fd = new FormData();
                  var imgBlob = dataURItoBlob($scope.myCroppedImage);
                  fd.append('file', imgBlob, 'i.png');
                  user_data.img_fd = fd;
            }
            if (angular.equals(base_user_data, user_data)){
                $uibModalInstance.dismiss('cancel');
            }
            if (!base_user_data) {
                control_service
                    .add(user_data)
                    .success(function () {
                        $uibModalInstance.close(user_data);
                    })
            }
            else {
                control_service
                    .update(user_data)
                    .success(function () {
                        $uibModalInstance.close(user_data);
                    })
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }


})();