function getter1(node) {
    var sid = node.parent().parent().find(".sid");
    sid.css('visibility', 'hidden');
    var data = {
        limit: 10,
        prefix: node.val()
    };

    $.get("/autocomplete_pid?limit=" + data.limit +
        "&prefix=" + data.prefix, function (data) {
        var innode = node.parent().find(".pid");
        innode.empty();
        for (var key in data) {
            innode.append("<li class='list-group-item input_1' data-value='" +
                key + "' data-pid='" + data[key] + "'>" +
                key + "</li>");
        }
        innode.css('visibility', 'visible');
        var lists = innode.children('li');
        lists.each(function (i, list) {
            list.style.cursor = 'pointer';
            list.style.overflowX = 'hidden';
            list.addEventListener('click', function (event) {
                node.val(list.dataset.value);
                node.attr('data-inppid', list.dataset.pid);
                node.attr('data-value', list.dataset.value);
                innode.css('visibility', 'hidden');
                node.trigger('change');
            })

        });
    });
}


function reset(node) {
    console.log(node);
    if (node[0].value == undefined || node[0].value == '') {
        node.removeAttr('data-inppid');
        node.removeAttr('data-inpsid');
        node.removeAttr('data-value');
        node.trigger('change');
    }
}

function getter2(node) {
    var pid = node.parent().parent().find(".pid");
    pid.css('visibility', 'hidden');
    var fnode = node.parent().parent().find('[name$=_input_1]');
    var data = {
        "limit": 10,
        "prefix": node.val(),
        "pid": fnode.val()
    };
    $.get("/autocomplete_sid?limit=" + data.limit +
        "&prefix=" + data.prefix + "&pid=" + data.pid, function (data) {
        var innode = node.parent().find(".sid");
        innode.empty();
        for (var key in data) {
            innode.append("<li class='list-group-item input_2' data-value='" +
                key + "' data-sid='" + data[key] + "'>" +
                key + "</li>");
        }
        innode.css('visibility', 'visible');
        var lists = innode.children('li');
        lists.each(function (i, list) {
            list.style.cursor = 'pointer';
            list.style.overflowX = 'hidden';
            list.addEventListener('click', function (event) {
                node.val(list.dataset.value);
                node.attr('data-inpsid', list.dataset.sid);
                node.attr('data-value', list.dataset.value);
                innode.css('visibility', 'hidden');
                node.trigger('change');
            })
        });

    });
}

$(document).click(function (event) {
    if ($(event.target).closest("ul").length) return;
    $('ul.pid').css('visibility', 'hidden');
    $('ul.sid').css('visibility', 'hidden');
    event.stopPropagation();
});


$.fn.queryBuilder.define('mapping2', function (options) {

    this.on('getRuleInput.filter', function (h, rule, name) {

        var filter = rule.filter;
        if (filter.data_type === 'sourceType') {
            function eventTypeQBInput() {
                var element_html = '<div class="input-group dropdown"><input class="form-control" type="text" name="' + name + '_input_1" onfocus="getter1($(this));" oninput="getter1($(this));" onblur="reset($(this))" data-toggle="dropdown" style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;"><ul class="list-group dropdown-menu pid" style="display: block; visibility: hidden"></ul></div>\n\
                                    <div class="input-group dropdown"><input class="form-control" type="text" name="' + name + '_input_2" onfocus="getter2($(this));" oninput="getter2($(this));" onblur="reset($(this))" data-toggle="dropdown" style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;"><ul class="list-group dropdown-menu sid" style="display: block; visibility: hidden"></ul></div>';

                return element_html;
            }

            function eventTypeQBGetter(rule) {
                var firstInp = rule.$el.find('[name$=_input_1]'),
                    secondInp = rule.$el.find('[name$=_input_2]');

                return [{
                    numPid: firstInp.attr('data-inppid'),
                    numSid: secondInp.attr('data-inpsid')
                }, {
                    strPid: firstInp.attr('data-value'),
                    strSid: secondInp.attr('data-value')
                }];
            }

            function eventTypeQBSetter(rule, value) {
                var firstInp = rule.$el.find('.rule-value-container [name$=_input_1]'),
                    secondInp = rule.$el.find('.rule-value-container [name$=_input_2]'),
                    firstId = rule.$el.find('.pid'),
                    secondId = rule.$el.find('.sid');
                if (rule.operator.nb_inputs > 0) {
                    firstInp.val(value[1].strPid);//.trigger('change');
                    secondInp.val(value[1].strSid);//.trigger('change');
                    firstId.append("<li class='input_1' value='" +
                        value[1].strPid + "' data-pid='" + value[0].numPid.pid + "'></li>");
                    if(value[0].numSid){
                    secondId.append("<li class='input_2' value='" +
                        value[1].strSid + "' data-sid='" + value[0].numSid.sid + "'></li>")
                    }else{
                        secondId.append("<li class='input_2' value='' data-sid=''></li>")
                    }
                }
            }

            function validation(rule) {
                console.log(rule);
                var emptyErr = 'неверное заполнение';
                if (rule[0].numPid !== undefined) {
                    return true;
                } else {
                    return emptyErr;
                }

            }

            filter.validation = {
                callback: validation
            };
            filter.valueGetter = eventTypeQBGetter;
            filter.valueSetter = eventTypeQBSetter;

            h.value = eventTypeQBInput();
        }
    });
    var node = $(this);

    node.click(function (event) {
        if ($(event.target).closest("ul").length) return;
        $('ul.pid').css('visibility', 'hidden');
        $('ul.sid').css('visibility', 'hidden');
        event.stopPropagation();
    });

    $.fn.queryBuilder.extend({
        getRulesResolved: function () {
            var it = $(this);
            node.querybuild = it[0];
            var resultJson = node.querybuild.getRules();
            var newJson = {};

            function recurseFix(resultJson) {
                // группа
                if (typeof resultJson.condition !== "undefined") {
                    var njs = {};
                    njs.condition = resultJson.condition;
                    njs.rules = [];
                    resultJson.rules.forEach(function (object) {
                        njs.rules.push(recurseFix(object));
                    });
                    return njs;
                } else {    // правило
                    if (resultJson.field === "event_type") {   // подмена
                        if (typeof resultJson.value[0].numPid !== "undefined" && typeof resultJson.value[0].numSid !== "undefined") {
                            newJson = {
                                condition: "AND",
                                rules: [{
                                    id: "plugin_id",
                                    field: "plugin_id",
                                    type: resultJson.type,
                                    input: resultJson.input,
                                    operator: resultJson.operator,
                                    value: resultJson.value[0].numPid
                                }, {
                                    id: "plugin_sid",
                                    field: "plugin_sid",
                                    type: resultJson.type,
                                    input: resultJson.input,
                                    operator: resultJson.operator,
                                    value: resultJson.value[0].numSid
                                }]
                            }
                        } else if (typeof resultJson.value[0].numPid !== "undefined") {
                            newJson = {
                                condition: "AND",
                                rules: [{
                                    id: "plugin_id",
                                    field: "plugin_id",
                                    type: resultJson.type,
                                    input: resultJson.input,
                                    operator: resultJson.operator,
                                    value: resultJson.value[0].numPid
                                }]
                            }
                        }
                    }
                    return newJson;
                }
            }

            var njs = recurseFix(resultJson);
            return njs;
        }
    });


}, {
    font: 'glyphicons',
    color: 'default'
});

