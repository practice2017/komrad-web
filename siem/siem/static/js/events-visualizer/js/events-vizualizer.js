var exist = 0;
var s;
var nodes = new Set();
var nodes_src = new Set();
var nid = {};
var max_width = 15;

var src_field = 'username';
var dst_field = 'ip_src';

$(function () {
    $.fn.eventsVisualizer = function (data, data_arg) {
        var evis = this[0]['id'];

        var colors = [
            '#42b382',
            '#E9585B',
            '#3f51b5',
            '#e53935',
            '#63A8EB',
            '#90c1f1',
            '#ffc107',
            '#ff5722',
            '#424242',
            '#37474f'
        ];

        /*
         * ОБРАБОТКА МЕТОДОВ
         */
        if (typeof (data) === "string") {
            if (data === "kill") {
                nodes = new Set();
                nid = {};

                s.kill();
                return;
            }

            if (data === "time-range") {
                var from = data_arg['from'];
                var to = data_arg['to'];

                var visible_nodes = new Set();

                for (var edge_i in s.graph.edges()) {
                    var edge = s.graph.edges()[edge_i];
                    var new_size = 0;
                    for (var i in edge['timestamps']) {
                        var ts = edge['timestamps'][i];
                        if (from - 1 <= ts && ts <= to + 1) {
                            new_size += 1;
                        }
                    }

                    //var old_label = edge['label'];
                    //edge['label'] = updateLabel(old_label, new_size);
                    edge['size'] = (new_size > max_width) ? max_width : new_size;

                    /*
                     * Рёбра с шириной 0 - надо удалять руками 
                     * Аналогично для вершин без ребер
                     */
                    if (new_size === 0) {
                        edge.hidden = true;
                    } else {
                        edge.hidden = false;
                        visible_nodes.add(edge['source']);
                        visible_nodes.add(edge['target']);
                    }
                }

                /*
                 * Скрываем вершины без рёбер
                 */
                var nds = s.graph.nodes();
                for (var node_i in nds) {
                    var node = nds[node_i];
                    if (visible_nodes.has(node['id'])) {
                        node.hidden = false;
                    } else {
                        node.hidden = true;
                    }
                }

                s.refresh();
                return;
            }
        }

        /*
         * Данные сохраняются для того, чтобы иметь возможность 
         * при изменении time-range редактировать грани
         */

        exist = 1;
        var i,
                img,
                N = 10,
                E = 50,
                g = {
                    nodes: [],
                    edges: []
                };


        // Подсчитать уникальные узлы
        //console.log(data);
        src_field = data['src_field'];
        dst_field = data['dst_field'];
        data = data['data'];
        for (var node in data) {

            var src = data[node][src_field];
            var dst = data[node][dst_field];
            if (typeof (src) !== "undefined") {
                nodes.add(src);
                nodes_src.add(src);
            }

            if (typeof (dst) !== "undefined")
                nodes.add(dst);
        }

        // вершины
        var i = 0;
        nodes.forEach(function (value) {
            var label = dst_field;
            if (nodes_src.has(value))
                label = src_field;
            nid[value] = i;
            g.nodes.push({
                id: 'n' + i,
                label: label + ': ' + value,
                type: (value === 'siem') ? 'image' : 'def',
                url: '/static/js/events-visualizer/SIEM.PNG',
                x: Math.random(),
                y: Math.random(),
                size: (value === 'siem') ? 25 : 10,
                color: colors[Math.floor(Math.random() * colors.length)]
            });
            i += 1;
        });

        // грани
        i = 0;
        for (var node in data) {
            if (typeof(data[node][src_field]) === "undefined" || typeof(data[node][dst_field]) === "undefined")
                continue;            
            
            var label = data[node]['plugin_id'] + ':' + data[node]['plugin_sid'] + ' - 1 шт.';
            var uniq_id = data[node]['plugin_id'] + ':' + data[node]['plugin_sid'] + ':' + data[node][src_field] + data[node][dst_field];

            /*
             * Тут нужен поиск дубликатов, ибо много параллельных линий для 
             * одного типа событий очень тормозит браузер. 
             * 
             * Предлагается обозначать количество событий этого типа 
             * шириной линии и указанием в метке.
             */
            var found = false;
            for (var edge in g.edges) {
                var e_label = g.edges[edge]['uniq_id'];
                if (e_label === uniq_id) {
                    found = true;
                    // уточнение шириной линии ...
                    g.edges[edge]['count_events'] += 1;
                    g.edges[edge]['timestamps'].push(parseInt(data[node]['event_id']));
                    // ... и лэйблом 
                    //var old_label = g.edges[edge]['label'];
                    var new_size = g.edges[edge]['count_events'];
                    //g.edges[edge]['label'] = updateLabel(old_label, new_size);
                    // ограничим ширину линий, чтобы не уходили на весь экран
                    g.edges[edge]['size'] = (new_size > max_width) ? max_width : new_size;
                    break;
                }
            }
            if (found === true)
                continue;

            // Добавление граней
            g.edges.push({
                id: 'e' + i,
                //label: label,
                uniq_id: uniq_id,
                source: 'n' + nid[data[node][src_field]],
                target: 'n' + nid[data[node][dst_field]],
                type: 'arrow',
                count: i,
                size: 1,
                count_events: 1,
                // массив меток времени используется для обновления time-range
                timestamps: [parseInt(data[node]['event_id']), ]
            });
            i += 1;
        }
        sigma.canvas.nodes.image.cache(
                '/static/js/events-visualizer/SIEM.PNG',
                function () {
                    s = new sigma({
                        graph: g,
                        renderer: {
                            // IMPORTANT:
                            // This works only with the canvas renderer, so the
                            // renderer type set as "canvas" is necessary here.
                            container: document.getElementById(evis)/*document.getElementById('graph-container')*/,
                            type: 'canvas'
                        },
                        settings: {
                            minNodeSize: 0,
                            maxNodeSize: 0,
                            minEdgeSize: 0,
                            maxEdgeSize: 0,
                            minArrowSize: 7,
                            enableEdgeHovering: false,
                            edgeHoverSizeRatio: 2,
                            edgeHoverColor: "#000",
                            edgeLabelSize: 'proportional'
                        }
                    });

                    s.startForceAtlas2(
                            {
                                worker: true,
                                barnesHutOptimize: false,
                                iterationsPerRender: 20,
                                startingIterations: 5000,
                                strongGravityMode: true,
                                slowDown: 10
                            });
                    setTimeout(function () {
                        s.stopForceAtlas2();
                        // Initialize the dragNodes plugin:
                        sigma.plugins.dragNodes(s, s.renderers[0]);
                    }, 1000);
                }
        );
    };


    sigma.utils.pkg('sigma.canvas.nodes');
    sigma.canvas.nodes.image = (function () {
        var _cache = {},
                _loading = {},
                _callbacks = {};

        // Return the renderer itself:
        var renderer = function (node, context, settings) {
            var args = arguments,
                    prefix = settings('prefix') || '',
                    size = node[prefix + 'size'],
                    color = node.color || settings('defaultNodeColor'),
                    url = node.url;

            if (_cache[url]) {
                context.save();

                // Draw the clipping disc:
                /*context.beginPath();
                 context.arc(
                 node[prefix + 'x'],
                 node[prefix + 'y'],
                 node[prefix + 'size'],
                 0,
                 Math.PI * 2,
                 true
                 );
                 context.closePath();
                 context.clip();*/

                // Draw the image
                context.drawImage(
                        _cache[url],
                        node[prefix + 'x'] - size,
                        node[prefix + 'y'] - size,
                        2 * size,
                        2 * size
                        );

                // Quit the "clipping mode":
                context.restore();

                // Draw the border:
                context.beginPath();
                context.arc(
                        node[prefix + 'x'],
                        node[prefix + 'y'],
                        node[prefix + 'size'],
                        0,
                        Math.PI * 2,
                        true
                        );
                context.lineWidth = size / 5;
                context.strokeStyle = node.color || settings('defaultNodeColor');
                context.stroke();
            } else {
                sigma.canvas.nodes.image.cache(url);
                sigma.canvas.nodes.def.apply(
                        sigma.canvas.nodes,
                        args
                        );
            }
        };

        // Let's add a public method to cache images, to make it possible to
        // preload images before the initial rendering:
        renderer.cache = function (url, callback) {
            if (callback)
                _callbacks[url] = callback;

            if (_loading[url])
                return;

            var img = new Image();

            img.onload = function () {
                _loading[url] = false;
                _cache[url] = img;

                if (_callbacks[url]) {
                    _callbacks[url].call(this, img);
                    delete _callbacks[url];
                }
            };

            _loading[url] = true;
            img.src = url;
        };

        return renderer;
    })();


});

function updateLabel(old_label, new_size) {
    var delimeter_pos = old_label.indexOf(' - ');
    var new_label = old_label.substr(0, delimeter_pos);
    new_label += ' - ' + new_size + ' шт.';
    return new_label;
}

