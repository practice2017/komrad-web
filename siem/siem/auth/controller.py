# -*- coding: utf-8 -*-

from flask import Blueprint, \
    render_template, \
    redirect, url_for, flash, request, jsonify
from flask_login import login_user, logout_user, login_required, \
    current_user

from forms import LoginForm
from siem.models import User

from . import authentification


@authentification.route("/", methods=["GET", "POST"])
@authentification.route("/login", methods=["GET", "POST"])
def login():
    return_code = 200
    if current_user.is_authenticated:
        return redirect(url_for("event_listener.index"))

    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        user = User.get_first_by_username(form.username.data)
        if user.active:
            login_user(user)
            user.updateLoginTime()
            flash("Logged in successfully.", "success")
            return redirect(url_for("event_listener.index"))

    return render_template("login.html", form=form), return_code


@authentification.route("/logout", methods=["POST", "GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for("login.login"))


@authentification.route("/about_me", methods=["GET"])
@login_required
def get_current_user_data():
    return jsonify(current_user.serialize), 200
