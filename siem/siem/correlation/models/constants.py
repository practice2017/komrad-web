# -*- coding: utf-8 -*-

from flask_babel import lazy_gettext as _

INC_STATUSES = {
    '0': _('New'),
    '1': _('Opened'),
    '2': _('Closed')
}

INC_COLORS = {
    '0': '#ff7174',
    '1': '#ffbc75',
    '2': '#5bcc9b'
}