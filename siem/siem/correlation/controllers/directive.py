# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, redirect, url_for, flash, \
    jsonify, request, stream_with_context, send_file, current_app
from flask import json

from siem.wrappers import access_required
from siem.models.DirStore import DirStore
from siem.models.Reactions import Reactions

import uuid
import requests as curl

from siem.correlation import correlation
from ..models.directive import *
import siem.route_access as ra

def treeWalker(data, parent_name, select_guid=None):
    children = []
    for i, d in enumerate(data):
        if d['breadcrumbs'] == parent_name:
            element = {
                "data": d,
                "text": d['ru_name'],
                "name": d['name'],
                "state": {
                    "opened": True
                },
                "icon": 'fa fa-play',
                "is_folder": d['is_folder']
            }

            if d['uuid'] == select_guid:
                element['state']['selected'] = True

            if d['status'] == 1:
                element['icon'] = 'fa fa-pause'

            if d['is_folder'] == 1:
                element['type'] = 'folder'
                element['icon'] = 'fa fa-folder-open-o'
                el_ch = treeWalker(data, element['name'], select_guid)
                element['children'] = el_ch

            children.append(element)

    return children

@correlation.route("/dirconstructor", methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def ajax_dircons():
    return render_template("correlation/dir_constructor.html")


@correlation.route('/correl/dir/<int:_id>', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def get_directive(_id=None):
    """route for statement (node or leaf) operations:
    GET - get statement record

    Args:
        _id (int, optional): primary key for get and put requests

    Returns:
        TYPE: response and error code
    """
    directive = DirStore.get_by_id(_id)
    return json.dumps(directive.as_dict()), 200


@correlation.route('/correl/uuid', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def generate_uuid():
    return str(uuid.uuid4())


@correlation.route('/correl/jstree', methods=["GET"])
@correlation.route('/correl/jstree/<string:guid>', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def buildTreeStructure(guid=None):
    tree = DirStore.get_all_as_dict()
    tree_structure = treeWalker(tree, '', guid)
    return json.dumps(tree_structure), 200


@correlation.route('/correl/get_directive_by_guid/<string:guid>',
                   methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def getDirectiveByGUID(guid):
    dir = DirStore.get_by_UUID(guid)
    dir = dir.as_dict()
    return json.dumps(dir), 200


@correlation.route('/correl/add_folder', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def addFolderToTree():
    data = request.get_json()
    
    if DirStore.get_by_name("folder-" + data['name']) > 0:
        return "folder already exist", 400
        
    bread = ''
    if data['parent'] != '':
        dir = DirStore.get_by_UUID(data['parent'])
        dir = dir.as_dict()

        if dir['is_folder'] != 1:
            return "parent isn't a directory", 400

        bread = dir['name']

    guid = generate_uuid()
    folder = {
        'breadcrumbs': bread,
        'name': 'folder-' + data['name'],
        'ru_name': data['name'],
        'uuid': guid,
        'data': ''
    }

    DirStore.createDirStore(
        folder['breadcrumbs'], folder['name'], folder['ru_name'],
        folder['uuid'], folder['data'], is_folder=1)
    return guid, 200


@correlation.route('/correl/add_directive', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def addDirectiveToTree():
    data = request.get_json()

    if DirStore.get_by_name("directive-" + data['name']) > 0:
        return "directive already exist", 400

    bread = ''
    if data['parent'] != '':
        dir = DirStore.get_by_UUID(data['parent'])
        dir = dir.as_dict()
        if dir['is_folder'] != 1:
            return "parent is not folder", 400 

        bread = dir['name']
    guid = generate_uuid()

    directive = {
        'breadcrumbs': bread,
        'name': 'directive-' + data['name'],
        'ru_name': data['name'],
        'uuid': guid,
        'data': ''
    }

    DirStore.createDirStore(
        directive['breadcrumbs'], directive['name'],
        directive['ru_name'], directive['uuid'], directive['data'],
        is_folder=0)

    # add reaction
    Reactions.create_reaction(guid, 0, 0, 0, 0, '', '')
    return guid, 200


@correlation.route('/correl/check_directive_for_changes', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR, 1)
def checkDirectiveForChanges():
    data = request.get_json()

    if ('prev_sel_guid' in data and
            len(data['prev_sel_guid']) > 0 and
            data['pass'] == False):

        dir = DirStore.get_by_UUID(data['prev_sel_guid'])
        dir = dir.as_dict()

        new_dir = json.loads(data['directive'])
        old_dir = json.loads(dir['data'])
        new_reaction = json.loads(data['reaction'])
        old_reaction = json.loads(get_reaction(data['prev_sel_guid'])[0])

        if new_dir == old_dir and new_reaction == old_reaction:
            return "eq", 200
        else:
            return "un_eq", 400
    else:
        return "eq", 200


@correlation.route('/correl/dir', methods=["POST", "DELETE", "PUT"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def update_directive():
    """route for statement (node or leaf) operations:
    PUT - update statement by id
    POST - create new statement
    DELETE - delete statement

    Returns:
        TYPE: response and error code
    """
    data = request.get_json()

    if request.method == 'PUT':
        DirStore.updateRecord(
            data['id'], data['breadcrumbs'], data['name'],
            data['ru_name'], data['uuid'], data['data'], data['status'], 0)
    if request.method == 'POST':
        DirStore.createDirStore(
            data['breadcrumbs'], data['name'],
            data['ru_name'], data['uuid'], data['data'], 0, 0)
    if request.method == 'DELETE':
        DirStore.removeDirStore(data['id'])

    return "ok", 200


@correlation.route('/correl/reaction/<string:_id>', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def get_reaction(_id=None):
    reaction = Reactions.get_by_id_as_dict(_id)
    return json.dumps(reaction), 200


@correlation.route('/correl/reaction', methods=["POST", "DELETE", "PUT"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def update_reaction():
    """route for statement (node or leaf) operations:
    PUT - update statement by id
    POST - create new statement
    DELETE - delete statement

    Returns:
        TYPE: response and error code
    """
    if request.method == 'PUT':
        data = request.get_json()
        Reactions.update_reaction(
            data['id'], data['status'],
            data['mail_status'], data['scripts_status'], data['ext_status'],
            data['scripts'], data['ext_services'], data['groups_assign'])
    if request.method == 'POST':
        data = request.get_json()
        Reactions.create_reaction(
            data['id'], data['status'],
            data['mail_status'], data['scripts_status'], data['ext_status'],
            data['scripts'], data['ext_services'])
    if request.method == 'DELETE':
        data = request.get_json()
        Reactions.remove_reaction(data['id'])

    return jsonify({"status": "ok"}), 200


@correlation.route('/correl/tree', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def get_directive_tree():
    """route for getting directives tree structure

    Returns:
        TYPE: jsonified tree on rendered template
    """
    tree = DirStore.get_all_as_dict()
    return json.dumps(tree), 200


@correlation.route('/correl/save', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def save_directive():
    data = request.get_json()
    # dir = {data['uuid']: json.loads(data['data'])}

    # update on correlation engine only if directive is active
    if data['status'] == 0:
        curl.delete(current_app.config['CORRELATOR_URL'],
                    data=json.dumps({'uuid': data['uuid']}))
        resolved_dir = {
            data['uuid']: json.loads(
                curl.get("{0}correl/by_id?id={1}".format(
                    current_app.config['FACTS_BASE_URL'],
                    data['uuid'])).content)
        }

        curl.post(
            current_app.config['CORRELATOR_URL'],
            data=json.dumps({
                'uuid': data['uuid'],
                "directive": resolved_dir.items()[0] #dir.items()[0]}))
                })
            )
    return "Saved", 200


@correlation.route('/correl/remove', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def remove_directive():
    data = request.get_json()
    curl.delete(current_app.config['CORRELATOR_URL'],
                data=json.dumps({'uuid': data['uuid']}))
    return "Removed", 200


@correlation.route('/correl/pause', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def pause_directive():
    data = request.get_json()
    curl.delete(current_app.config['CORRELATOR_URL'],
                data=json.dumps({'uuid': data['uuid']}))
    DirStore.updateDirStoreStatus(data['uuid'], 1)

    return "Paused", 200


@correlation.route('/correl/resume', methods=["POST"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def resume_directive():
    data = request.get_json()

    resolved_dir = {
        data['uuid']: json.loads(
            curl.get("{0}correl/by_id?id={1}".format(
                current_app.config['FACTS_BASE_URL'], data['uuid'])).content)
    }

    curl.post(
        current_app.config['CORRELATOR_URL'],
        data=json.dumps({
            'uuid': data['uuid'],
            "directive": resolved_dir.items()[0]})
        )#dir.items()[0]}))

    DirStore.updateDirStoreStatus(data['uuid'], 0)

    return json.dumps(resolved_dir), 200


@correlation.route('/correl/status/<string:_id>', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def get_directive_status(_id=''):
    dir = DirStore.get_by_UUID(_id)
    dir = dir.as_dict()

    status = dir['status']

    if status == 0:
        return "Run", 200
    elif status == 1:
        return "Pause", 200
    else:
        return "Removed", 200


@correlation.route('/correl/alldirectives', methods=["GET"])
@access_required(ra.PAGE_DIR_CONSTRUCTOR)
def get_alldirectives():
    """route for getting directives tree structure

    Returns:
        TYPE: jsonified tree on rendered template
    """
    tree = DirStore.get_all_as_dict()
    return json.dumps(tree), 200



