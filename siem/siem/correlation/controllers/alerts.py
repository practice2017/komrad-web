# -*- coding: utf-8 -*-

from flask import render_template, url_for, jsonify, request, Response, \
    stream_with_context
from flask import app, current_app, make_response, abort

from ..models.alert_requester import *
from siem.wrappers import access_required
from siem.models import Group
from siem.models import EventFields
from siem.classes import makeReport

from operator import itemgetter

import datetime

import pdfkit

from siem.correlation import correlation
import siem.route_access as ra


@correlation.route('/correl/alert_page', methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def ajax_alert_page():
    return render_template("correlation/alert.html")


@correlation.route('/correl/alerts/all', methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def ajax_get_alerts_list():
    return render_template("correlation/alert_list.html")


@correlation.route("/incidents/page", methods=["POST"])
@access_required(ra.PAGE_INCIDENTS, act_type=1)
def get_incidents_by_request():
    try:
        incidents = AlertRequester.getByPage(current_user.id,
                                             request.get_json())
    except:
        abort(503)
    return jsonify(incidents), 200


@correlation.route('/correl/alerts/stat', methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def json_get_total_alerts():
    total = AlertRequester.getTotal(current_user.id)
    return jsonify({'total': total}), 200


@correlation.route('/correl/alerts/drilldown',
                   methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def json_get_alert_drilldown():
    dd = AlertRequester.getDrilldown(current_user.id)
    return jsonify({'response': dd}), 200


@correlation.route('/correl/alert', methods=["POST"])
@access_required(ra.PAGE_INCIDENTS)
def update_alert():
    data = request.get_json()
    AlertRequester.updateAlert(data)

    return jsonify({'status': ok}), 200


@correlation.route('/correl/alert/<string:uuid>', methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def alert_data(uuid):
    alert_data = AlertRequester.getAlertByUUID(current_user.id, uuid, True,
                                               True)

    return jsonify(alert_data)


@correlation.route("/incidents/set_states", methods=["POST"])
@access_required(ra.PAGE_INCIDENTS)
def mass_change_state():
    data = request.get_json()
    inc_list = data.get('ids')
    new_status = data.get('state')
    if inc_list is None or new_status is None:
        raise AssertionError("bad request")

    for i in inc_list:
        AlertRequester.updateAlert({
            'id': i,
            's': new_status
        })

    return jsonify({'status': 'ok'}), 200

@correlation.route("/incidents/delete", methods=["POST"])
@access_required(ra.PAGE_INCIDENTS)
def mass_delete_alert():
    data = request.get_json()
    inc_list = data.get('ids', None)
    if inc_list is None:
        raise AssertionError("bad request")

    for i in inc_list:
        AlertRequester.deleteAlert({
            'id': i
        })

    return jsonify({'status': 'ok'}), 200


@correlation.route("/incidents/export", methods=["GET"])
@access_required(ra.PAGE_INCIDENTS)
def make_inc_csv():
    fmt = request.args.get('fmt', None)
    if fmt not in ['csv']:
        return abort(400)

    inc_id = request.args.get('id', None)
    if not inc_id:
        return abort(400)

    fields = EventFields.get_all()

    alert_data = AlertRequester.getAlertByUUID(current_user.id, inc_id, True,
                                               False)

    def generate(fields, events):

        fields = sorted(fields, key=itemgetter('id'))

        def getValue(_dict, field):
            value = _dict.get(field['name'], '')
            if not value:
                return ' '
            if field['type'] == 'datetime':
                value = datetime.datetime.fromtimestamp(
                    int(value)).strftime('%Y-%m-%d %H:%M:%S')
            return str(value).replace('\n', '')

        spr = app.config['CSV_SEPARATOR']
        yield spr.join([x['label'] for x in fields]) + '\n'
        for lvl in events:
            for event_data in lvl['events']:
                values = [getValue(event_data, f) for f in fields]
                yield spr.join(values) + '\n'

    # for i in generate(fields, alert_data['events']):
    #     print i

    return Response(stream_with_context(
        generate(fields, alert_data['events'])),
        mimetype='text/csv')
