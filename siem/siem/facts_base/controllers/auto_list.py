# -*- coding: utf-8 -*-

from flask import jsonify, request
from siem.wrappers import access_required
from siem.facts_base import facts_base

from siem.facts_base.models.BOFRequester import BofRequester
import siem.route_access as ra


@facts_base.route('/alist/<string:_id>', methods=["GET"])
@access_required(ra.PAGE_FACTS_BASE)
def get_auto_list(_id):
    res = BofRequester.getAutoList(_id)
    return jsonify(res), 200


@facts_base.route('/alist', methods=["POST"])
@access_required(ra.PAGE_FACTS_BASE)
def create_auto_list():
    data = request.get_json()
    res = BofRequester.createAutoList(data)
    if res == 400:
        return jsonify({u'response': "wrong request"}), 400
    return jsonify(res), 200


@facts_base.route('/alist', methods=["PUT"])
@access_required(ra.PAGE_FACTS_BASE)
def update_auto_list():
    data = request.get_json()
    res = BofRequester.updateAutoList(data)
    if res == 400:
        return jsonify({u'response': "wrong request"}), 400
    return jsonify(res), 200


@facts_base.route('/alist/<string:_id>', methods=["DELETE"])
@access_required(ra.PAGE_FACTS_BASE)
def delete_auto_list(_id):
    res = BofRequester.removeAutoList(_id)
    return jsonify(res), 200

