# -*- coding: utf-8 -*-
import datetime
import time
from functools import wraps
from wsgiref.handlers import format_date_time
from flask import make_response, current_app, Request, abort
from flask import g, request, redirect, url_for
from flask_login import current_user
from models import ACCESS_LVLS
from werkzeug.exceptions import Forbidden

import sys

def cache(expires=None, round_to_minute=False):
    """
    Add Flask cache response headers based on expires in seconds.

    If expires is None, caching will be disabled.
    Otherwise, caching headers are set to expire in now + expires seconds
    If round_to_minute is True, then it will always expire at the start of a minute (seconds = 0)

    Example usage:

    @app.route('/map')
    @cache(expires=60)
    def index():
    return render_template('index.html')

    """
    def cache_decorator(view):
        @wraps(view)
        def cache_func(*args, **kwargs):
            now = datetime.datetime.now()

            response = make_response(view(*args, **kwargs))
            response.headers['Last-Modified'] = format_date_time(
                time.mktime(now.timetuple()))

            if expires is None:
                response.headers['Cache-Control'] = 'no-store, no-cache, '\
                    'must-revalidate, post-check=0, pre-check=0, max-age=0'
                response.headers['Expires'] = '-1'
            else:
                expires_time = now + datetime.timedelta(seconds=expires)

                if round_to_minute:
                    expires_time = expires_time.replace(
                        second=0, microsecond=0)

                response.headers['Cache-Control'] = 'public'
                response.headers['Expires'] = format_date_time(
                    time.mktime(expires_time.timetuple()))

            return response
        return cache_func
    return cache_decorator


def access_required(access_set, act_type=None):
    """Враппер для проверки уровня досутпа пользователя
    за основу взят запрос
    GET - чтение (1)
    POST, PUT, DELETE - запись (2)
    если POST подразумевает чтение то следует изменить act_type на 1
    
    :param access_set: название модуля 
    :param act_type: минимальный уровень доступа
    :return: decorated function
    """
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
                return current_app.login_manager.unauthorized()
            # из-за doublewrap приходится проверять ее наличие в функции
            if 'act_type' not in locals():
                if request.method in ('POST', 'PUT', 'DELETE'):
                    act_type = 2
                else:
                    act_type = 1
            check_res = current_user.checkAccess(act_type, access_set)
            if not check_res:
                abort(403)
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper

