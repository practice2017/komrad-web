# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel

from flask_login import current_user

from siem.utils import get_current_time
from scapy.all import *
import json


class Query(siemModel):
    """Acrive record class for queries table

    Attributes:
        body (column object): query body
        created (column object): cteated datetime
        description (TYPE): some string with desc
        id (column column object): Primary key
        name (column object): query name
        role_access (column object): required lvl for data access
    """
    __tablename__ = 'queries'

    id = Column('id', db.Integer, primary_key=True)
    name = Column('name', db.String(127), nullable=False, unique=True)
    description = Column('description', db.String(255))
    body = Column('body', db.Unicode(1023), nullable=False)
    created = Column('created', db.DateTime(timezone=True), nullable=False)
    role_access = Column('role_access', db.Integer, nullable=False)

    def __init__(self, name, body, description=u'empty'):
        self.name = name
        self.body = json.dumps(body).encode('utf8')
        self.description = description
        self.created = get_current_time()
        self.role_access = current_user.role

    @classmethod
    def get_by_id(self, query_id):
        return self.query.filter_by(id=query_id).first_or_404()

    def is_unique(self):
        if self.query.filter_by(name=self.name).first():
            return False
        return True

    @staticmethod
    def get_by_user_id():
        return None

    @staticmethod
    def get_all_queries():
        return Query.query.all()

    @staticmethod
    def get_all_as_dict():
        res = []
        for f in Query.query.all():
            res.append(f.as_dict())
        return res

    def __repr__(self):
        return '<Query: %r>' % (self.name)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'body': json.loads(self.body),
            'created': self.created
        }


