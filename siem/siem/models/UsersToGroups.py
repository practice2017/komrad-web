# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel


class UsersToGroups(siemModel):
    """Summary

    Attributes:
        group_id (TYPE): Description
        groups (TYPE): Description
        user_id (TYPE): Description
        users (TYPE): Description
    """
    __tablename__ = 'user_group'

    user_id = Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    group_id = Column(db.Integer, db.ForeignKey('groups.id'), primary_key=True)

    user = db.relationship("User", back_populates="groups")
    group = db.relationship("Group", back_populates="users")

    def __repr__(self):
        return '<user@group: %d@%d>' % (self.user_id, self.group_id)
