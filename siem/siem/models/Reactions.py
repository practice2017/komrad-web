# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel
from Groups import Group
from Users import User
from DirStore import DirStore


class Reactions(siemModel):

    __tablename__ = 'reactions'
    id = Column(db.String(55), primary_key=True)
    status = Column('status', db.Integer)
    mail_status = Column('mail_status', db.Integer)
    scripts_status = Column('scripts_status', db.Integer)
    ext_status = Column('ext_status', db.Integer)
    scripts = Column('scripts', db.Unicode(10000))
    ext_services = Column('ext_services', db.Unicode(10000))
    groups_assign = Column('groups_assign', db.Unicode(81920))

    @staticmethod
    def get_by_id(directive_id):
        return Reactions.query.filter_by(id=directive_id).first_or_404()

    @staticmethod
    def get_by_id_as_dict(directive_id):
        return Reactions.query.filter_by(id=directive_id).first_or_404().as_dict()

    @staticmethod
    def create_reaction(directive_id, status=0, mail_status=0,
                        scripts_status=0, ext_status=0, scripts="",
                        ext_services="", groups_assign='1'):
        statement = Reactions()
        statement.id = directive_id
        statement.status = status
        statement.mail_status = mail_status
        statement.scripts_status = scripts_status
        statement.ext_status = ext_status
        statement.scripts = scripts
        statement.ext_services = ext_services
        statement.groups_assign = groups_assign
        db.session.add(statement)
        db.session.commit()
        return

    @staticmethod
    def update_reaction(directive_id, status=0, mail_status=0, 
                        scripts_status=0, ext_status=0, scripts="",
                        ext_services="", groups_assign='1'):
        statement = Reactions.get_by_id(directive_id)
        statement.id = directive_id
        statement.status = status
        statement.mail_status = mail_status
        statement.scripts_status = scripts_status
        statement.ext_status = ext_status
        statement.scripts = scripts
        statement.ext_services = ext_services
        statement.groups_assign = groups_assign

        db.session.commit()
        return 'done'

    @staticmethod
    def remove_reaction(id):
        statement = DirStore.get_by_id(id)
        db.session.delete(statement)
        db.session.commit()
        return

