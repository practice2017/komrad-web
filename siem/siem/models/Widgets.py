# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel

# -*- coding: utf-8 -*-
from flask import current_app
import requests
import uuid


class Widgets(siemModel):

    __tablename__ = 'widgets'

    id = Column(db.Unicode(60), primary_key=True, nullable=False)
    user_id = Column(db.Integer)
    update_period = Column(db.Integer)
    plot_duration = Column(db.Integer)
    description = Column(db.UnicodeText)
    data_type = Column(db.Unicode(45))
    widgetType = Column(db.Unicode(45))
    show_delta = Column(db.Integer)
    results_count = Column(db.Integer)
    query_body = Column(db.UnicodeText)
    graph_type = Column(db.Unicode(45))
    name = Column(db.UnicodeText)
    row = Column(db.Integer)
    col = Column(db.Integer)
    sizeX = Column(db.Integer)
    sizeY = Column(db.Integer)
    sorted_by_value = Column(db.Integer)

    facet_id = Column(db.Integer, db.ForeignKey('facets.id'))
    facet = db.relationship('Facets')

    def __init__(self):
        self.id = str(uuid.uuid1())
        self.row = 0
        self.col = 0
        self.sizeX = 1
        self.sizeY = 1
        self.sorted_by_value = 0

    @classmethod
    def get_by_id(self, _id):
        return self.query.filter_by(id=_id).first()

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'settings': {
                'update_period': self.update_period,
                'plot_duration': self.plot_duration - self.update_period,
                'description': self.description,
                'widgetType': self.widgetType,
                'show_delta': self.show_delta,
                'query_body': self.query_body,
                'graph_type': self.graph_type,
                'name': self.name,
                'uuid': self.id,
                'sort': self.sorted_by_value,
                'facet': self.getFacet()
            },
            'row': self.row,
            'col': self.col,
            'sizeX': self.sizeX,
            'sizeY': self.sizeY
        }

    def getFacet(self):
        if not self.facet:
            return None
        return self.facet.serialize_chain

    def save(self):
        db.session.add(self)
        db.session.commit()
        self.sendSignal_add()

    def sendSignal_add(self):
        json_to_req = {'id': self.id}
        req = requests.post(
            current_app.config['WIDGET_STAT_URL'] + 'widget/add',
            json=json_to_req)
        return req.status_code

    def sendSignal_rm(self):
        requests.post(current_app.config['WIDGET_STAT_URL'] + 'widget/delete',
                      json={'id': self.id})

    def setFromDict(self, _dict):
        for key, value in _dict.iteritems():
            setattr(self, key, value)
        return

    @staticmethod
    def getByUser(_id):
        return Widgets.query.filter_by(user_id=_id).all()

    def update(self, with_signals=True):
        """rewrite uuid of record, delete daemon and add new
        """
        if with_signals:
            self.sendSignal_rm()
            self.id = str(uuid.uuid1())

        db.session.commit()

        if with_signals:
            self.sendSignal_add()

        # self.setFromDict()

    @staticmethod
    def remove(id):
        """removing users

        Args:
            id (int): primary key for record search

        Raises:
            ValueError: Block admin record remove
        """
        statement = Widgets.get_by_id(id)
        statement.sendSignal_rm()
        db.session.delete(statement)
        return db.session.commit()


