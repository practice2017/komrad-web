# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel

from werkzeug import generate_password_hash, check_password_hash
from flask_login import UserMixin
from siem.extensions import login_manager
from Groups import Group
from siem.utils import get_current_time
from UsersToGroups import UsersToGroups
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from Accessability import Accessability

class User(siemModel, UserMixin):
    """Users table - active record

    Attributes:
        create_time (datetime): record create time
        description (unicode): Description of record
        first_name (unicode): name
        id (int): primary key
        last_login (datetime): last sign up time
        last_name (unicode): second name
        password (unicode): password hash code
        role (int): Description
        roles (object): relationshit to roles table
        username (unicode): as it is
        widgets_template (unicode): JSON for widgets page
    """
    __tablename__ = 'users'

    id = Column(db.Integer, primary_key=True)
    username = Column(db.String(50), nullable=False, unique=True)
    _password = Column('password', db.String(255), nullable=False)
    email = Column(db.Unicode(255), nullable=False, unique=True)
    description = Column(db.Unicode(200))
    first_name = Column(db.Unicode(50))
    last_name = Column(db.Unicode(50))
    widgets_template = Column(db.Unicode(4096), default=u'{}')
    last_login = Column(db.DateTime)
    create_time = Column(db.DateTime, nullable=False)
    role = Column(db.Integer, db.ForeignKey('roles.id'))
    active = Column(db.Integer)
    image = Column(db.Integer, db.ForeignKey('file_storage.id'))
    phone = Column(db.Unicode(127))
    rank = Column(db.Unicode(255))

    groups = db.relationship("UsersToGroups", back_populates="user",
                             cascade="all, delete-orphan")
    roles = db.relationship('Role')
    images = db.relationship('FileStorage', uselist=False)

    def _get_access_map(self):
        if self.id == 1:
            a_map = Accessability.getDefaultAccess()
            for k in a_map.keys():
                a_map[k] = 3
            return a_map
        # if not self.roles:
        #     return {}
        # role_access_map = self.roles.access_map
        try:
            my_access_map = Accessability.query.filter_by(
                type='user', type_id=self.id).one().getAccessMap()
        except NoResultFound:
            my_access_map = {}
        # role_access_map.update(my_access_map)
        return my_access_map

    access_map = property(_get_access_map)

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.active = 1

    def _get_password(self):
        return self._password

    def _set_password(self, password):
        self._password = generate_password_hash(password)

    # Hide password encryption by exposing password field only.
    password = db.synonym('_password',
                          descriptor=property(_get_password,
                                              _set_password))

    def check_password(self, password):
        if self.password is None:
            return False
        return check_password_hash(self.password, password)

    def getFullAccessMap(self):
        """
    
        :return: словарь с доступом роли + доступом пользователя
        """
        with_role = self.roles.access_map
        with_role.update(self.access_map)
        return with_role

    def checkAccess(self, _access_lvl, _needed_domain):
        if self.id == 1: # it is admin, he cans everything
            return True

        with_role = self.getFullAccessMap()
        for rule in with_role.keys():
            if rule in _needed_domain and with_role[rule] >= _access_lvl:
                return True
        return False

    def updateLoginTime(self):
        """Update last login time
        """
        self.last_login = get_current_time()
        return db.session.commit()

    @classmethod
    def get_by_id(self, user_id):
        return self.query.filter_by(id=user_id).first()

    @login_manager.user_loader
    def load_user(id):
        u = User.query.get(int(id))
        return u

    @staticmethod
    def get_first_by_username(name):
        return User.query.filter_by(username=name).first()

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        result = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'description': self.description,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'last_login': self.last_login,
            'create_time': self.create_time,
            'role': self.role,
            'rank': self.rank,
            'active': self.active,
            'groups': [x.group_id for x in self.groups],
            'access_map': self.access_map
        }
        if self.images:
            result['image'] = self.images.path
        return result


    @property
    def serialize2(self):
        """Return object data in easily serializeable format"""
        result = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'description': self.description,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'last_login': self.last_login,
            'create_time': self.create_time,
            'role': self.role,
            'rank': self.rank,
            'active': self.active,
            'groups': [x.group_id for x in self.groups],
        }
        if self.images:
            result['image'] = self.images.path
        return result

    @property
    def serialize_strict(self):
        """Return object data in easily serializeable format"""
        result = {
            'id': self.id,
            'username': self.username,
            'email': self.email
        }
        if self.images:
            result['image'] = self.images.path
        return result

    @staticmethod
    def add(username, email, password, role_id, desc='', first_name='',
                last_name='', image_id=None, groups=[Group.getDefaultId()]):
        """add user to database

        Args:
            username (unicode): uniq user name
            password (unicode): password
            role_id (int): role id in roles
            desc (str, optional): description
            first_name (str, optional): name
            last_name (str, optional): second name

        """
        statement = User(username, password)

        statement.email = email
        statement.role = role_id
        statement.description = desc
        statement.first_name = first_name
        statement.last_name = last_name
        statement.create_time = get_current_time()

        if image_id:
            statement.image = image_id

        a = UsersToGroups()
        a.group = Group(username, 1)
        statement.groups.append(a)

        # def_a = UsersToGroups()
        # def_a.group_id = Group.getDefaultId()
        # statement.groups.append(def_a)

        if not Group.getDefaultId() in groups:
            groups.append(Group.getDefaultId())

        for i in groups:
            assoc = UsersToGroups()
            assoc.group_id = i
            statement.groups.append(assoc)

        db.session.add(statement)
        db.session.commit()
        return statement

    @staticmethod
    def update(id, username, email, role_id, desc,
            first_name, last_name, image_id=None, groups=None):
        """update record fields, without password

        Args:
            id (unicode): primary key for record search
            username (unicode): uniq user name
            role_id (int): role id in roles
            desc (unicode): new description
            first_name (unicode): name
            last_name (unicode): second name

        Raises:
            ValueError: Block admin record update
        """
        if id == 1:
            statement = User.get_by_id(id)
            statement.email = email
            statement.description = desc
            statement.first_name = first_name
            statement.last_name = last_name
        else:
            statement = User.get_by_id(id)
            statement.username = username
            statement.email = email
            statement.role = role_id
            statement.description = desc
            statement.first_name = first_name
            statement.last_name = last_name

        if image_id:
            statement.image = image_id

        if groups:
            old_groups = UsersToGroups.query.filter_by(user_id=id).all()
            for of in old_groups:
                db.session.delete(of)
            for i in groups:
                assoc = UsersToGroups()
                assoc.group_id = i
                statement.groups.append(assoc)
            if not Group.getDefaultId() in groups:
                groups.append(Group.getDefaultId())

        try:
            db.session.commit()
        except IntegrityError:
            return 'not uniq name'
        return 'done'

    @staticmethod
    def setAccessMap(id, new_access_map):
        if id == 1:
            pass
        statement = User.get_by_id(id)
        access_map = Accessability.query.filter_by(
            type='user', type_id=id).first()
        if access_map:

            access_map.setAccessMap(new_access_map)
            db.session.commit()
            return
        access_map = Accessability()
        access_map.type = 'user'
        access_map.type_id = statement.id
        access_map.setAccessMap(new_access_map)
        db.session.add(access_map)
        db.session.commit()

    @staticmethod
    def updatePassword(id, password):
        """Update record password

        Args:
            id (int): primary key for record search
            password (TYPE): new password
        """
        statement = User.get_by_id(id)
        statement.password = password
        return db.session.commit()


    @staticmethod
    def updateImage(id, img_id):
        """Update record password

        Args:
            id (int): primary key for record search
            password (TYPE): new password
        """
        statement = User.get_by_id(id)
        statement, img_id = img_id
        return db.session.commit()

    @staticmethod
    def remove(id):
        """removing users

        Args:
            id (int): primary key for record search

        Raises:
            ValueError: Block admin record remove
        """
        if id == 1:
            raise ValueError('Immutable record')
        statement = User.get_by_id(id)
        db.session.delete(statement)
        db.session.delete(
            Group.query.filter_by(name=statement.username).first())
        return db.session.commit()

    @staticmethod
    def assasinate(id):
        """lock user by id (remove simulation)

        Args:
            id (int): primary key for record search

        Raises:
            ValueError: Block admin record remove
        """
        if id == 1:
            raise ValueError('Immutable record')
        statement = User.get_by_id(id)
        statement.active = 0
        return db.session.commit()

    @staticmethod
    def ressurect(id):
        """unlock user by id (remove simulation)

        Args:
            id (int): primary key for record search

        Raises:
            ValueError: Block admin record remove
        """
        if id == 1:
            raise ValueError('Unremovable record')
        statement = User.get_by_id(id)
        statement.active = 1
        return db.session.commit()
