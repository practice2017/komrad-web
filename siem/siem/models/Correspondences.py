# -*- coding: utf-8 -*-

from sqlalchemy import Column

from abstractions import siemModel
from scapy.all import *
from siem.extensions import db


class Correspondences(siemModel):
    """Active model for table correspondences

    Attributes:
        comment (unicode): Comment for record
        description (unicode): Description for record
        id (int): primary key
        implemented (bool): is the ref implemented? 
        name (unicode): Description
        ref (unicode): view name
        selected (bool): is the ref selected? 
        type (unicode): leaf or root or node
    """
    __tablename__ = 'correspondences'

    id = Column('id', db.Integer, primary_key=True)
    ref = Column('ref', db.Unicode(64), unique=True, nullable=False)
    name = Column('name', db.Unicode(256), nullable=False)
    type = Column('type', db.Unicode(16), nullable=False)
    description = Column('description', db.Unicode(4096))
    selected = Column('selected', db.Integer(), nullable=False)
    implemented = Column('implemented', db.Integer(), nullable=False)
    comment = Column('comment', db.Unicode(8192))

    @classmethod
    def get_by_id(self, query_id):
        return self.query.filter_by(id=query_id).first_or_404()

    @staticmethod
    def get_all():
        return Correspondences.query.all()

    @classmethod
    def get_all_as_dict(self):
        res = []
        for f in Correspondences.query.all():
            res.append(f.as_dict())
        return res

    def _get_oprs(self):
        return json.loads(self._operators)

    def __repr__(self):
        return '<Query: %r>' % (self.selected)
