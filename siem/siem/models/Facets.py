# -*- coding: utf-8 -*-

from sqlalchemy import Column
from siem.extensions import db
from abstractions import siemModel

CHAIN_LIMIT = 2


class Facets(siemModel):

    __tablename__ = 'facets'

    id = Column(db.Integer, primary_key=True, nullable=False)
    child_id = Column(db.Integer, db.ForeignKey('facets.id'))
    type_id = Column(db.Integer, db.ForeignKey('event_fields.id'),
                     nullable=False)
    count = Column(db.Integer)

    child = db.relationship('Facets', remote_side=[id],
                            lazy='joined', join_depth=4)
    child_1 = db.relationship('Facets', lazy='joined', join_depth=4)
    type = db.relationship('EventFields')

    def __init__(self, type_id, count, child_id=None):
        self.type_id = type_id
        self.count = count
        if child_id:
            self.child_id = child_id

    @classmethod
    def get_by_id(self, _id):
        return self.query.filter_by(id=_id).first()

    @property
    def serialize_chain(self):
        """Return object data in easily serializeable format"""
        _curr = self
        result = []
        for i in xrange(CHAIN_LIMIT):
            if not _curr:
                break
            try:
                result.append(_curr.serialize)
            except AttributeError:
                return result
            _curr = _curr.child
        return result

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            "id": self.type.id,
            "c": self.count,
            "locale": self.type.name_ru,
            "name": self.type.name,
            "type": self.type.type
        }

    @staticmethod
    def addChain(chain):
        """
        take dict with next structure:
        {
            0: {id:1,c:5},
            1: {id:3,c:5},
            2: {id:134,c:500},
            ...
        }
        return id of chain head
        """
        child_id = None
        head = None       
        for lvl in reversed(chain):
            if 'id' not in lvl:
                lvl['id'] = 11
            f = Facets(lvl['id'], lvl['c'], child_id)
            db.session.add(f)
            db.session.commit()
            child_id = f.id
            head = f
        return head

    @staticmethod
    def deleteChain(head_id):
        """
        take dict with next structure:
        {
            0: {id:1,c:5},
            1: {id:3,c:5},
            2: {id:134,c:500},
            ...
        }
        return id of chain head
        """
        _curr = Facets.get_by_id(head_id)
        while _curr:
            if not _curr:
                break
            db.session.delete(_curr)
            _curr = _curr.child
        db.session.commit()

    def __repr__(self):
        return '<Facet ID: %r>' % (self.id)

