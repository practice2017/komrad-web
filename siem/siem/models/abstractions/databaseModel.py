# -*- coding: utf-8 -*-
"""
Reimplement db.Model with some useful mwthods
"""

from siem.extensions import db


class siemModel(db.Model):
    __abstract__ = True

    def as_dict(self):
        res = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        return res
