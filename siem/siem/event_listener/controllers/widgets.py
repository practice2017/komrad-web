# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""

from flask import render_template, url_for, jsonify, request, current_app, \
    make_response, abort
from flask_login import current_user
from siem.wrappers import access_required
from siem.event_listener import event_listener
from ..models.widget_data import WidgetData
from siem.models import Widgets, Facets
from siem.extensions import db

import datetime
import time
import pdfkit
import json

import siem.route_access as ra

@event_listener.route("/widget/<string:widget_uuid>/<float:start_time>",
                      methods=["GET"])
@event_listener.route("/widget/<string:widget_uuid>/<int:start_time>",
                      methods=["GET"])
@access_required(ra.PAGE_WIDGETS)
def widgets_controller_getter(_type='line', widget_uuid=None, start_time=None,
                              delta='no_delta'):
    """ GET - getting events from server by uuid widget
    POST - create new widget with UUID
    DELETE - delete new widget with UUID

    :param _type: type of chart (pie or someone else)
    :param widget_uuid: uuid of widget
    :param start_time: time filter for events
    :param delta: if == 'delta' return delta value between value and next to
    :return: 200 - All good, 204 - no data for send
    """
    wd = WidgetData(widget_uuid)
    result = wd.getWidgetData(start_time)
    return jsonify({'result': result}), 200


@event_listener.route("/widget/report/<string:widget_uuid>", methods=["GET"])
@access_required(ra.PAGE_WIDGETS)
def make_widget_report(widget_uuid):

    # search widget in user data:

    try:
        widget = Widgets.get_by_id(widget_uuid)
        wd = WidgetData(widget_uuid)
        result = wd.getWidgetData(0)
    except ValueError:
        raise
    js_files = [
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for('static',
            filename='bower_components/jquery/dist/jquery.min.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for('static',
            filename='bower_components/highcharts/highcharts.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for('static',
            filename='bower_components/highcharts/highcharts-3d.js'),
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for('static',
            filename='bower_components/highcharts/highcharts-more.js')
    ]
    rendered_template = render_template(
        "event_listener/reports/widget_report.html", js=js_files,
        json_data=json.dumps(result), data=result, widget=widget.serialize['settings'],
        time=int(time.time()),
        float_to_dt=datetime.datetime.utcfromtimestamp)
    options = {
        'encoding': "utf8",
        '--enable-javascript': '',
        "debug-javascript": '',
        '--javascript-delay': 2000
    }

    css = [
        current_app.config['PROJECT_ROOT'] + '/siem' + url_for('static',
            filename='bower_components/bootstrap/dist/css/bootstrap.min.css')
    ]

    config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf.sh')

    pdf = pdfkit.from_string(rendered_template, False, options=options,
        css=css, configuration=config)

    response = make_response(pdf)
    response.headers['Content-Disposition'] = "attachment; \
        filename='widget%s.pdf" % widget_uuid
    response.headers['Content-Type'] = "application/pdf; charset=utf-8"

    return response


@event_listener.route("/widget/<string:widget_uuid>", methods=["GET"])
@access_required(ra.PAGE_WIDGETS)
def get_widget(widget_uuid):
    """Get widget bu uuid

    :return: json response
    """
    widget = Widgets.get_by_id(widget_uuid)
    res = widget.serialize
    res['settings']['query_body'] = json.loads(res['settings']['query_body'])
    return jsonify(res), 200


@event_listener.route("/widget/<string:widget_uuid>", methods=["PUT"])
@access_required(ra.PAGE_WIDGETS)
def update_widgets_list(widget_uuid):
    """Update widget bu uuid

    :return: json response
    """
    widget = Widgets.get_by_id(widget_uuid)

    if not widget:
        return abort(404)

    if widget.user_id != current_user.id:
        return jsonify({'uuid': widget.id}), 200
    with_signals = False

    new_query_body = json.dumps(request.get_json().get('query_body', None))


    new_query_body = request.get_json().get('query_body', None)
    if type(new_query_body) is dict:
        new_query_body = json.dumps(new_query_body)

    new_update_period = request.get_json().get('update_period',
                                               widget.update_period)
    new_plot_duration = request.get_json().get('plot_duration',
                                               widget.plot_duration)

    new_facet = request.get_json().get('facet', None)
    if new_facet:
        # Facets.deleteChain(widget.facet_id)
        widget.facet = Facets.addChain(new_facet)
        with_signals = True

    if (new_query_body != widget.query_body or
            new_update_period != widget.update_period or
            new_plot_duration != widget.plot_duration):
        widget.query_body = new_query_body
        widget.update_period = new_update_period
        widget.plot_duration = new_plot_duration
        with_signals = True

    widget.show_delta = request.get_json().get('show_delta',
                                               widget.show_delta)
    widget.name = request.get_json().get('name', widget.name)
    widget.widgetType = request.get_json().get('widgetType',
                                               widget.widgetType)
    widget.description = request.get_json().get('description',
                                                widget.description)
    widget.graph_type = request.get_json().get('graph_type', widget.graph_type)
    widget.sorted_by_value = request.get_json().get('sort',
        widget.sorted_by_value)

    if widget.plot_duration < widget.update_period + widget.plot_duration:
        widget.plot_duration = widget.update_period + widget.plot_duration

    widget.update(with_signals)
    return jsonify({'uuid': widget.id}), 200


@event_listener.route("/widget/add", methods=["POST"])
@access_required(ra.PAGE_WIDGETS)
def create_widgets_list():
    """Update widget bu uuid

    :return: json response
    """
    widget = Widgets()
    new_chain = request.get_json().get('facet', None)
    if new_chain:
        widget.facet = Facets.addChain(new_chain)
    else:
        widget.facet = None

    widget.user_id = current_user.id
    widget.query_body = request.get_json().get('query_body', None)
    if type(widget.query_body) is dict:
        widget.query_body = json.dumps(widget.query_body)

    widget.update_period = request.get_json().get('update_period', None)
    widget.plot_duration = request.get_json().get('plot_duration', None)
    widget.show_delta = request.get_json().get('show_delta', 0)

    if widget.plot_duration < widget.update_period + widget.plot_duration:
        widget.plot_duration = widget.update_period + widget.plot_duration

    widget.name = request.get_json().get('name', 'NoName')
    widget.widgetType = request.get_json().get('widgetType', None)
    widget.description = request.get_json().get('description', '')
    widget.graph_type = request.get_json().get('graph_type', None)
    widget.sorted_by_value = request.get_json().get('sort', 0)
    widget.save()
    return jsonify({'uuid': widget.id}), 200


@event_listener.route("/widget/<string:widget_uuid>", methods=["DELETE"])
@access_required(ra.PAGE_WIDGETS)
def delete_widget(widget_uuid):
    """remove widget bu uuid

    :return: json response body and code
    """
    Widgets.remove(widget_uuid)
    return jsonify({'result': 'Ok'}), 200


@event_listener.route("/user/widgets", methods=["GET"])
@access_required(ra.PAGE_WIDGETS)
def get_widgets_list():
    """Widget page saver and getter
    On POST request bode must content field widgets!!!

    :return: json response body and code
    """
    user_widgets = Widgets.getByUser(current_user.id)
    serialized = [i.serialize for i in user_widgets]
    return jsonify({'response': serialized}), 200


@event_listener.route("/user/widgets", methods=["POST"])
@access_required(ra.PAGE_WIDGETS)
def widget_page_controller():
    """Widget page saver and getter
    On POST request bode must content field widgets!!!

    :return: json response body and code
    """
    widgets = request.get_json().get('widgets', None)
    if widgets is None:
        return abort(400)

    for widget in widgets:
        statement = Widgets.get_by_id(widget['settings']['uuid'])
        statement.col = widget.get('col', 0)
        statement.row = widget.get('row', 0)
        statement.sizeX = widget['sizeX']
        statement.sizeY = widget['sizeY']
        db.session.commit()
    return jsonify({'success': True}), 200
