# -*- coding: utf-8 -*-
"""Blueprint and controllers of event listener module

Attributes:
    ACCESS_MODULE (str): module name for access wrapper
    event_listener (TYPE): Blueprint for this module
"""

from flask import render_template, jsonify, request, current_app
from siem.models import EventFields, Query
from siem.wrappers import access_required
from siem.event_listener import event_listener
from ..models.LuceneRequester import LuceneRequester
import json
import requests
import siem.route_access as ra

@event_listener.route("/fullrequest", methods=["POST"])
@access_required(ra.PAGE_SEARCH, act_type=1)
def get_events_by_qb():
    """
    Getter for events data:
        All request parameters contains in body.
    
    Returns:
        json: event records
    
    Deleted Parameters:
        query_body (json): dict with query data
        page (int): request page
        sort_by (str): sorting field
        sort_type (str): sort order
        from (int): from value
        to (int): to valuer
    """
    query_body = request.get_json().get('query_body', None)
    page = request.get_json().get('page', 1)
    sort_by = request.get_json().get('sort_by', None)
    sort_type = request.get_json().get('sort_reverse', None)
    time_from = request.get_json().get('from', 0)
    time_to = request.get_json().get('to', 0)
    result = LuceneRequester.lucene_request(
        query_body, page, sort_by=sort_by, sort_reverse=sort_type,
        time_from=time_from, time_to=time_to)
    if not result:
        return jsonify({'response': 'Connection to backend is wrong'}), 500
    try:
        data = jsonify(json.loads(result))
    except ValueError:
        data = {}
    except:
        raise
    return data


@event_listener.route("/range_slider", methods=["POST"])
@access_required(ra.PAGE_SEARCH, act_type=1)
def get_range_slider():
    """get grouped events by time
    
    Returns:
        json: event records
    """
    data = request.get_json()
    r = requests.post(current_app.config['WIDGET_STAT_URL'] +
                      'widget/range_slider', json=data)
    return jsonify(json.loads(r.content)), 200


@event_listener.route("/preventive", methods=["POST"])
@access_required(ra.PAGE_SEARCH, act_type=1)
def get_sort_data():
    data = request.get_json()
    r = requests.post(current_app.config['WIDGET_STAT_URL'] +
                      'widget/preventive', json=data)
    return jsonify(json.loads(r.content)), 200


@event_listener.route("/autocomplete_pid", methods=["GET"])
@access_required(ra.PAGE_SEARCH + ra.PAGE_WIDGETS)
def get_autocompleted_pid():
    limit = request.args.get('limit')
    prefix = request.args.get('prefix')

    r = requests.get(
        current_app.config['FACTS_BASE_URL'] +
        'autocomplete_pid?limit={0}&prefix={1}'.format(limit, prefix))
    return jsonify(json.loads(r.content))


@event_listener.route("/autocomplete_sid", methods=["GET"])
@access_required(ra.PAGE_SEARCH + ra.PAGE_WIDGETS)
def get_autocompleted_sid():
    limit = request.args.get('limit')
    prefix = request.args.get('prefix')
    pid = request.args.get('pid')

    r = requests.get(current_app.config['FACTS_BASE_URL'] +
                     'autocomplete_sid?limit={0}&prefix={1}&pid={2}'.format(
                         limit, prefix, pid))
    return jsonify(json.loads(r.content))



