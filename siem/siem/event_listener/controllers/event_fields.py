# -*- coding: utf-8 -*-
from flask import jsonify
from siem.wrappers import access_required
from siem.models import EventFields
from siem.event_listener import event_listener
import siem.route_access as ra


@event_listener.route('/fields/all')
@event_listener.route('/fields/all/<int:faceted>')
@access_required(ra.ACCESS_FIELDS)
def get_event_fields(faceted=None):
    """Return event fields data as json """
    if faceted:
        return jsonify(data=EventFields.getFaceted())
    return jsonify(data=EventFields.get_all())


@event_listener.route('/fields/tree')
@access_required(ra.ACCESS_FIELDS)
def get_event_fields_tree():
    """Return event fields data as json """
    data=EventFields.get_trees()
    for_send = []
    for i in data:
        if i['children']:
		    for_send.append(i)
    return jsonify(for_send)

