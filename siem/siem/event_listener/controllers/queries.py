# -*- coding: utf-8 -*-
"""
"""

from flask import render_template, jsonify, request, abort
from siem.wrappers import access_required
from siem.models import EventFields
from ..models.LuceneRequester import LuceneRequester
from ..models.rubicon_event import RubiconPCAP
from siem.event_listener import event_listener
from siem.models import Query
from siem.extensions import db

import json
import siem.route_access as ra


@event_listener.route("/querylist/all", methods=["GET"])
@access_required(ra.ACCESS_QUERIES)
def json_query_list():
    """Getter for queries

    Returns:
        json: all record in queries table
    """
    return jsonify({'response': Query.get_all_as_dict()})


@event_listener.route('/query/body/<int:query_id>', methods=["GET"])
@event_listener.route('/query/body', methods=["GET"])
@access_required(ra.ACCESS_QUERIES)
def query_controller_getter(query_id=None):
    """get all queries

    Args:
        query_id (int, optional): query id

    Returns:
        json: all queries from db
    """
    if query_id:
        return jsonify(Query.get_by_id(query_id).serialize), 200

    return jsonify(response=[i.as_dict() for i in Query.get_all_queries()]), \
        200


@event_listener.route('/query/body/<int:query_id>', methods=["DELETE"])
@event_listener.route('/query/body', methods=["POST"])
@access_required(ra.ACCESS_QUERIES)
def query_controller_updater(query_id=None):
    """controller for queries table in database

    Args for POST:
        query_name (str): query new name
        query_body (str): query body
        query_desc (str): description for query
        query_id (int): id of query

    Args for DELETE:
        query_id (int, for DELETE): Description

    Returns:
        json: response and error code
    """
    if request.method == 'POST':
        query_name = request.get_json().get('query_name', None)
        query_body = request.get_json().get('query_body', None)
        query_desc = request.get_json().get('query_description', None)

        if query_name is None or query_body is None:
            return abort(400)

        query_id = request.get_json().get('query_id', None)

        if not query_id:
            new_query = Query(query_name, query_body, query_desc)

            if not new_query.is_unique():
                return json.dumps(
                    {'success': False, 'error': 'Name is not unique'}), 400

            db.session.add(new_query)
            db.session.commit()
            return json.dumps({'success': True}), 200

        else:
            loaded_query = Query.get_by_id(query_id)
            loaded_query.name = query_name
            loaded_query.body = json.dumps(query_body)
            loaded_query.description = query_desc

            try:
                db.session.commit()
            except:
                return json.dumps(
                    {'success': False}), 400

            return json.dumps({'success': True}), 200
        return json.dumps({'success': False}), 500

    if request.method == 'DELETE':
        query = Query.get_by_id(query_id)
        db.session.delete(query)
        db.session.commit()
        return json.dumps({'success': True}), 200

    return jsonify({'response': ''}), 302
