# -*- coding: utf-8 -*-

class NoEventsReceived(Exception):
    """Raise when no exceptions received from backend server"""