# -*- coding: utf-8 -*-

from siem.wrappers import access_required

from flask_login import current_user
from flask_socketio import emit, send
from siem.extensions import sio
from flask_socketio import join_room, leave_room
import json


ROOM_NAME_PREFIX = "rmu"
import siem.route_access as ra


@sio.on('join_events')
@access_required(ra.PAGE_WIDGETS)
def join_events(data):
    room_name = ROOM_NAME_PREFIX + str(current_user.id)
    join_room(room_name)
    send('welcome to the events_notify room', room=room_name)


@sio.on('leave_events')
@access_required(ra.PAGE_WIDGETS)
def leave_correl(data):
    room_name = ROOM_NAME_PREFIX + str(current_user.id)
    leave_room(room_name)
    send('Goodbye', room=room_name)



