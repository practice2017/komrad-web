# -*- coding: utf-8 -*-

import os
import ConfigParser

config = ConfigParser.RawConfigParser()


class BaseConfig(object):

    # sql database file path
    try:
        config.read('/etc/mysql/.my.cnf')
        SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s/' \
            'komrad_system?charset=utf8' % (config.get('client', 'user'),
                config.get('client', 'password'),
                config.get('client', 'host'))
    except:
        raise AssertionError("Please set mysql congif in file /etc/mysql/.my.cnf")

    SIO_REDIS_HOST = "127.0.0.1"
    SIO_REDIS_PORT = 6379
    SIO_CHANNEL = "flask-socketio"
    REDIS_URL = "redis://127.0.0.1:6379/4"

    # SQLALCHEMY_POOL_SIZE = 10
    BOOTSTRAP_SERVE_LOCAL = True

    # role restrictions
    none_access = 0
    read = 1
    write = 2

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 3

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "secret"

    # Secret key for signing cookies
    SECRET_KEY = "secret"

    PROJECT = "siem_web_app"

    # Get app root path, also can use flask.root_path.
    # ../../config.py
    PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

    DEBUG = False
    TESTING = False

    ADMINS = ['zpldmity@gmail.com']

    # LOG_FOLDER = os.path.join(INSTANCE_FOLDER_PATH, 'logs')

    # BOOTSTRAP_SERVE_LOCAL = True
    JSON_SORT_KEYS = True
    USER_ENABLE_EMAIL = False
    USER_ENABLE_CONFIRM_EMAIL = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # FRAMEWORK_URL = 'http://192.168.5.241:10001/'
    FRAMEWORK_URL = 'http://localhost:10001/'

    WIDGET_STAT_URL = 'http://127.0.0.1:49002/'
    LUCENE_POST_URL = 'http://127.0.0.1:49012/'
    LICENSE_URL = 'http://127.0.0.1:49102/'
    CORRELATOR_URL = 'http://127.0.0.1:19001/'

    ALERTS_STAT_URL = 'http://127.0.0.1:49444/'
    FACTS_BASE_URL = 'http://127.0.0.1:49020/'

    ROTATIONS_URL = 'http://127.0.0.1:20001/'
    NAGVIS_URL = 'http://127.0.0.1:81/'

    EVENTS_ON_PAGE = 100

    BABEL_DEFAULT_LOCALE = 'ru'

    DEFAULT_ROOM_PREFIX = 'rmu'

    LANGUAGES = {
        'en': 'English',
        'ru': 'Русский'
    }

    PCAP_FILES_PATH = '/tmp/rubiconPCAP/'

    UPLOAD_FOLDER = '/var/www/komrad_files/'
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
    CSV_SEPARATOR = '||'

    # uploading file size < than 4 Gb
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024

    # session delay in minutes
    SESSION_TIMEOUT = 150


class DefaultConfig(BaseConfig):
    SQLALCHEMY_POOL_SIZE = 20
    SESSION_TIMEOUT = 6400


class DebugConfig(BaseConfig):
    SQLALCHEMY_POOL_SIZE = 200
    DEBUG = True
    TESTING = True
    SESSION_TIMEOUT = 3


class TestServer5002(BaseConfig):
    DEBUG = True
    TESTING = True

    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://komrad:Seclab1234@localhost/komrad_system?charset=utf8'
    # PROFILE = True


class DebugConfig241(DebugConfig):
    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://komrad:Seclab1234@192.168.5.241/komrad_system?charset=utf8'
    WIDGET_STAT_URL = 'http://192.168.5.241:49002/'
    LUCENE_POST_URL = 'http://192.168.5.241:49012/'
    ALERTS_STAT_URL = 'http://192.168.5.241:49444/'
    FACTS_BASE_URL = 'http://192.168.5.241:49020/'
    LICENSE_URL = 'http://192.168.5.241:49102/'
    CORRELATOR_URL = 'http://192.168.5.241:19001/'
    ROTATIONS_URL = 'http://192.168.5.241:20001/'
    NAGVIS_URL = 'http://192.168.5.93:81/'


    SIO_REDIS_HOST = "192.168.5.241"
    REDIS_URL = "redis://192.168.5.241:6379/4"

    # SQLALCHEMY_ECHO = True

class TestConfig241(DebugConfig241):
    TESTING = True


