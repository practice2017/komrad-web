# -*- coding: utf-8 -*-
import unittest
import os
import sys
from coverage import coverage
cov = coverage(branch=True, include=['siem/*'], concurrency='eventlet')
cov.start()

from test.auth.test_auth import *
from test.configuration.test_user import *

if __name__ == "__main__":
    try:
        unittest.main()
    except:
        pass
    cov.stop()
    cov.save()
    print("\n\nCoverage Report:\n")
    cov.report()
    cov.html_report()
    cov.erase()