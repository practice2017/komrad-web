from json import dumps

from test.siem_unittest import *


class TestAssignersCtrl(AdminTestCase):

    def test_get_groups(self):
        # get
        res, status_code = self.client.get('/groups/all').get_json()

        self.assertEqual(status_code, 200)
