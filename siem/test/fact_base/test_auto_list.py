from json import dumps

from test.siem_unittest import *


class TestAutoListCtrl(AdminTestCase):

    def test_create(self):
        payload = {
            'values': ['a', 'b', 'c'],
            'name': 'ololo'
        }
        # create
        res, status_code = self.client.post('/alist', json=dumps(payload))
        self.assertEqual(status_code, 200)
        alist_id = res.get('id')
        # get
        res, status_code = self.client.get('/alist/{}'.format(alist_id)).get_json()
        self.assertEqual(status_code, 200)

    def test_update(self):
        payload = {
            'values': ['a', 'b', 'c'],
            'name': 'ololo'
        }
        # create
        res, status_code = self.client.post('/alist', json=dumps(payload))
        self.assertEqual(status_code, 200)
        alist_id = res.get('id')
        payload = {
            'values': ['d', 'e', 'f'],
            'name': 'ololo',
            'request': 'id=1',
            'sql': None, #idk about sql
            'id': alist_id,
            'timer_select': None #idk about sql
        }

        # update
        res, status_code = self.client.put('/alist/{}'.format(alist_id)).get_json()
        self.assertEqual(status_code, 200)

    def test_remove(self):
        payload = {
            'values': ['a', 'b', 'c'],
            'name': 'ololo'
        }
        # create
        res, status_code = self.client.post('/alist', json=dumps(payload))
        self.assertEqual(status_code, 200)
        alist_id = res.get('id')

        # remove
        res, status_code = self.client.delete('/alist/{}'.format(alist_id)).get_json()
        self.assertEqual(status_code, 200)

        # get
        res, status_code = self.client.get('/alist/{}'.format(alist_id)).get_json()
        self.assertEqual(status_code, 404)
