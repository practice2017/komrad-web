# -*- coding: utf-8 -*-
import unittest
import sys
from siem.config import TestConfig241
from siem.app import create_app
import json
import coverage
import logging
from flask_socketio import SocketIO
from flask_classes.test_client import JSONTestClient

import random, string

def randomword(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))

class NoAuthTestCase(unittest.TestCase):

    def create_app(self):
        app = create_app(TestConfig241, False, True)
        app.test_client_class = JSONTestClient
        return app

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.app = self.create_app()
        self.socketio = SocketIO(self.app)
        self.client = self.app.test_client()
        self._ctx = self.app.test_request_context()
        self._ctx.push()

    def tearDown(self):
        if self._ctx is not None:
            self._ctx.pop()


class AdminTestCase(unittest.TestCase):

    def create_app(self):
        app = create_app(TestConfig241, False, True)
        app.test_client_class = JSONTestClient
        return app

    def setUp(self):
        self.app = self.create_app()
        self.socketio = SocketIO(self.app)
        self.client = self.app.test_client()
        self._ctx = self.app.test_request_context()
        self._ctx.push()
        self.client.post('/login', data=dict(
            username='admin',
            password='admin'
        ), follow_redirects=True)

    def tearDown(self):
        self.client.post('/logout', follow_redirects=True)

